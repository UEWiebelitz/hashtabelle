# Optimized 3-Way-Cuckoo-Hashtable
## Master thesis project by Uriel Elias Wiebelitz

This project is an implementation of the 3-Way-Cuckoo-Hashtable by (Pagh, Rodler, 2004).
As use case some methods of sequence analysis in bioinformatics are implemented.


It is based on extensions of the classical Cuckoo-Hashing by more than two hashfunctions (d≥2)
and bins/pages that may contain more than one element.

This implementation includes serveral optimizations such as
- Prefetching
- Parallelization by distribution in sub hashtables with one thread each
- Parallelization of reading operations on the hashtable
- Overlapping bins/pages
- Multi-level hashtable

## Steps to build To build the hashtable

Install Conda

Install the provided conda environment

    conda env create -f conda.yml

Activate environment

    conda activate cuckoo_ht

Run cmake

    CXX=clang++ cmake ./
    
Run make

    make -j9


### Compile flags for Optimizations

Some optimizations can be enabled by setting compile flags.
To enable for instance prefetching define

`PREFETCH` in combination with at least one of the following
- `PREFETCH_FIRST_PAGE` (might be combined with `PREFETCH_FIRST_PAGE_NEXT_PAGE`, `PREFETCH_FIRST_PAGE_NO_CACHE` or `PREFETCH_NEXT_CACHE_LINE`)
- `PREFETCH_ALL_PAGE`



## Execution
The binary src will be created in the directory src.

To get a list of the commands type
    
    src/src --help

To build the hashtable for a genome sequence pass it a file with extension .fa.
Ỳou always have to specify the size (upper limit) of the hashtable in numbers of elements.
The actions to perform can be given as a list without an argument prefix

    ./src/src -f Escherichia_coli_str_k_12_substr_mg1655.ASM584v2.dna.toplevel.fa -c 4600000 insert

An call for operating on a parallelized hashtable based on 9 sub hashtabeles with overlapping bins and 6 bits per counter could be

    ./src/src -f Escherichia_coli_str_k_12_substr_mg1655.ASM584v2.dna.toplevel.fa -c 5000000 count lookup mappability -k 25 --wL 500 --wpp 13 --ht blocked --ht linear -b 9 --hfCount 3 --counter 6 --kmer_count 6000000 -t 10
