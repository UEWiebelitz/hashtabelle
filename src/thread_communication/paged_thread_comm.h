#pragma once

#include <type_traits>
#include <vector>

#include <condition_variable>
#include <mutex>
#ifdef TAKE_TIME
#include <chrono>
#include <iostream>
#include "benchmark/tools.h"
#endif

#include "circular_buffer.h"
#include "compile_hints/inline_enforce.h"
#include "benchmark/taketime.h"

template<class T>
struct Page {
	enum class State{READABLE, WRITEABLE};
	State state = State::WRITEABLE;
	T *storage = nullptr;
	size_t currPos = 0;
	size_t availableElem = 0;
	size_t size = 0;

	Page(size_t pageSize)
		: size(pageSize)
	{
        storage = static_cast<T*>(malloc(sizeof(T) * pageSize));
	}

    Page(Page &&other)
    {
		std::swap(state, other.state);
		std::swap(storage, other.storage);
		std::swap(currPos, other.currPos);
		std::swap(availableElem, other.availableElem);
		std::swap(size, other.size);
	}

	~Page() {
		if (storage) {
            free(storage);
		}
	}

	FORCE_INLINE void push(const T &elem) {
#ifdef DEBUG_COMPILE
		assert(state == State::WRITEABLE);
		assert(availableElem <= size);
#endif
		storage[availableElem] = elem;
		++availableElem;
	}

	FORCE_INLINE const T& pop() {
		return storage[currPos++];
	}

	FORCE_INLINE bool isFull() {
		return availableElem == size;
	}

	FORCE_INLINE bool pageRead() {
		return currPos == availableElem;
	}
};

template<class T>
class PagedThreadComm {
protected:
	std::vector<Page<T>*> pages;
	std::mutex mut;

	CircularBuffer<Page<T>*> writeable;
	CircularBuffer<Page<T>*> readable;

	Page<T> *currWritePage = nullptr;
	Page<T> *currReadPage = nullptr;

	std::condition_variable condVar_read;
	std::condition_variable condVar_write;

	bool eof = false;

#ifdef TAKE_TIME
	std::chrono::nanoseconds  m_writeLockDuration;
	std::chrono::nanoseconds  m_readLockDuration;
#endif

	FORCE_INLINE bool isOne(typename Page<T>::State state) const {
		for (auto &p : pages) {
			if (p->state == state) {
				return true;
			}
		}
		return false;
	}

public:
	PagedThreadComm(std::size_t pageCount, std::size_t pageSize)
		: writeable(pageCount)
		, readable(pageCount)
	{
		pages.reserve(pageCount);
		for (size_t i=0; i<pageCount; ++i) {
			pages.push_back(new Page<T>(pageSize));
			writeable.push(pages.back());
		}
	}

	~PagedThreadComm() {
		for (auto p : pages) {
			delete p;
		}
		pages.clear();
	}

	PagedThreadComm(PagedThreadComm &&other) = delete;
	PagedThreadComm(const PagedThreadComm &other) = delete;

	FORCE_INLINE void push(const T &elem) {
		if (!currWritePage) {
#ifdef TAKE_TIME
			auto start = std::chrono::steady_clock::now();
#endif
			std::unique_lock<std::mutex> lock(mut);
			while (!writeable.elemCount()) {
				condVar_write.wait(lock);
			}
#ifdef TAKE_TIME
			m_writeLockDuration += std::chrono::steady_clock::now() - start;
#endif

			currWritePage = writeable.pop();
			currWritePage->availableElem = 0;
		}

		currWritePage->push(elem);
		if (currWritePage->isFull()) {
			{
				std::unique_lock<std::mutex> lock(mut);
				readable.push(currWritePage);
				currWritePage->state = Page<T>::State::READABLE;
			}
			currWritePage = nullptr;
			condVar_read.notify_one();
		}
	}

    /**
     * @brief waitForRead blocks until a page is readable or the end of input was signaled by the producer
     * @return true if elements are available
     */
    FORCE_INLINE bool waitForRead() {
        if (!currReadPage) {
#ifdef TAKE_TIME
            auto start = std::chrono::steady_clock::now();
#endif
            std::unique_lock<std::mutex> lock(mut);
            while (!readable.elemCount() && !eof) {
                condVar_read.wait(lock);
            }
#ifdef TAKE_TIME
            m_readLockDuration += std::chrono::steady_clock::now() - start;
#endif

            if (!readable.elemCount() && eof) {
                return false;
            }

            currReadPage = readable.pop();
            currReadPage->currPos = 0;
        }

        return !currReadPage->pageRead();
    }

	FORCE_INLINE T pop() {
		T elem = currReadPage->pop();
		if (currReadPage->pageRead()) {
			{
				std::unique_lock<std::mutex> lock(mut);
				currReadPage->state = Page<T>::State::WRITEABLE;
				writeable.push(currReadPage);
			}
			currReadPage = nullptr;
			condVar_write.notify_one();
		}
		return elem;
	}

	/**
	 * @brief isEOF indicates that there will not be any more input.
	 * @attention this funcion should always be called by the consumer!
	 * @return true if all readable pages are completely read and eof flag is set.
	 */
	FORCE_INLINE bool isEOF() {
		if (currReadPage) {
			return false;
		}
		std::unique_lock<std::mutex> lock(mut);
		if (readable.elemCount()) {
			return false;
		}
		return eof;
	}

	/**
	 * @brief setEOF indicates that there will not be any more input.
	 * @attention this funcion should always be called by the producer!
	 */
	FORCE_INLINE void setEOF() {
        std::unique_lock<std::mutex> lock(mut);
        eof = true;

        if (!currWritePage) {
            while (!writeable.elemCount()) {
                condVar_write.wait(lock);
            }
#ifdef TAKE_TIME
            m_writeLockDuration += std::chrono::steady_clock::now() - start;
#endif

            currWritePage = writeable.pop();
            currWritePage->availableElem = 0;
        }

        currWritePage->state = Page<T>::State::READABLE;
        readable.push(currWritePage);
        currWritePage = nullptr;
        condVar_read.notify_one();

#ifdef TAKE_TIME
	std::cerr << "write lock duration: " << printDots(m_writeLockDuration.count()) << std::endl;
	std::cerr << "read lock duration: " << printDots(m_readLockDuration.count()) << std::endl;
#endif
	}
};
