#pragma once

#include <type_traits>
#include <cstring>
#include <cstdlib>
#include <stdexcept>

#include "compile_hints/inline_enforce.h"
#include "compile_hints/likely_hint.h"

class CircCounter {
	size_t val = 0;
	size_t limit;

public:
	CircCounter(size_t limit)
		: limit(limit)
	{}

    FORCE_INLINE operator size_t() const {
		return val;
	}

    FORCE_INLINE const CircCounter& operator++() {
        ++val;
        if (unlikely(val == limit)) {
            val = 0;
        }
		return *this;
	}

    FORCE_INLINE CircCounter operator++(int) {
		CircCounter old = *this;
		operator++();
		return old;
	}

    FORCE_INLINE CircCounter operator+(size_t addVal) {
        CircCounter other = *this;
        other.val = (other.val + addVal) % other.limit;
        return other;
    }

    FORCE_INLINE size_t getLimit() const {
        return limit;
    }
};

template<class T>
class CircularBuffer {
	CircCounter front;
	CircCounter back;
	size_t m_elemCount = 0;

#ifdef DEBUG_COMPILE
    size_t m_size = 0;
#endif

    T *arr = nullptr;

public:
	CircularBuffer(std::size_t size)
		: front(size)
		, back(size)
    #ifdef DEBUG_COMPILE
        , m_size(size)
    #endif
	{
		const size_t byteCount = size * sizeof (T);
		arr = reinterpret_cast<T*>(malloc(byteCount));
		memset(arr, 0, byteCount);
    }

    CircularBuffer(CircularBuffer &&other)
        : front(0)
        , back(0)
    {
        std::swap(front, other.front);
        std::swap(back, other.back);
        std::swap(m_elemCount, other.m_elemCount);
#ifdef DEBUG_COMPILE
        std::swap(m_size, other.m_size);
#endif
        std::swap(arr, other.arr);
    }

//    CircularBuffer(const CircularBuffer &other)
//        : front(other.front)
//        , back(other.back)
//        , m_elemCount(other.m_elemCount)
//    {
//        const size_t byteCount = other.size() * sizeof (T);
//        arr = reinterpret_cast<T*>(malloc(byteCount));
//        memset(arr, 0, byteCount);

//        for(size_t i=0; i<other.elemCount(); ++i) {
//            arr[front +i] = other.arr[front + i];
//        }
//    }

	~CircularBuffer() {
        if (arr) {
            free(arr);
        }
	}

	inline void push(const T &val) {
		arr[front] = val;
		++front;
		++m_elemCount;

#ifdef DEBUG_COMPILE
        if (m_elemCount > m_size) {
            throw std::runtime_error("size of circular buffer exceeded!");
        }
#endif
	}

	inline T pop() {
		--m_elemCount;
		return arr[back++];
	}

	inline size_t elemCount() const {
		return m_elemCount;
	}

    inline size_t size() const {
        return front.getLimit();
    }
};
