#pragma once

#include <vector>
#include <numeric>

namespace Random {

template<class RandomGen>
class  UnequallyDist {
    const std::vector<size_t> probs;
    size_t sum;
    RandomGen &randGen;

public:
    UnequallyDist(const std::vector<size_t> &probs, RandomGen &randGen)
        : probs(probs)
        , sum(std::accumulate(probs.begin(), probs.end(), size_t(0)))
        , randGen(randGen)
    {

    }

    inline size_t operator()() const {
        auto randNum = randGen() % sum;

        for (size_t i=0; i<probs.size(); ++i) {
            if (randNum < probs[i]) {
                return i;
            }
            randNum -= probs[i];
        }

        return  randNum;
    }

    const std::vector<size_t> &getProbs() const {
        return probs;
    }
};

}
