#pragma once

#include <type_traits>
#include <string>
#include <iostream>
#include <numeric>
#include <iomanip>
#include <map>
#include <unordered_map>

template<class iter1, class iter2>
double avg(const iter1 &freq, const iter2 &values) {
	assert(values.size() == freq.size());

	std::size_t sum = 0;
	std::size_t freqSum = 0;

	for (std::size_t i=0; i<values.size(); ++i) {
		sum += values[i]*freq[i];
		freqSum += freq[i];
	}

	return double(sum) / freqSum;
}

template<class iter1>
double avg(const iter1 &freq, std::size_t valueOffset = 0) {
	std::size_t sum = 0;
	std::size_t freqSum = 0;

	for (std::size_t i=0; i<freq.size(); ++i) {
		sum += (i+valueOffset)*freq[i];
		freqSum += freq[i];
	}

	return double(sum) / freqSum;
}

template<class mapT1, class mapT2>
double avg(const std::unordered_map<mapT1, mapT2> &hist, std::size_t valueOffset = 0) {
	mapT1 sum = 0;
	mapT2 freqSum = 0;

	for (auto elem : hist) {
		sum += (elem.first+valueOffset) *elem.second;
		freqSum += elem.second;
	}

	return double(sum) / freqSum;
}

template<class List>
void printHistogram(std::ostream &stream, const std::string &name, const List &hist, std::size_t valueOffset = 0, bool printNulls = true) {
//	stream << std::endl;
	stream << "#BEGIN histogram: " << name << std::endl;

	size_t sum = std::accumulate(hist.begin(), hist.end(), 0);

	for(std::size_t i=0; i<hist.size(); ++i) {
		if (printNulls || hist[i] != 0) {
			stream << i + valueOffset << ": " << hist[i] << " (" <<  std::fixed << std::setprecision(4) << double (hist[i] * 100) / double(sum) << "%)" << std::endl;
		}
	}
	stream << "#END histogram: " << name << std::endl;

	stream << name << " avg: " << avg(hist, valueOffset) << std::endl;
}

template<class mapT1, class mapT2>
void printHistogram(std::ostream &stream, const std::string &name, const std::/*unordered_*/map<mapT1, mapT2> &hist, std::size_t valueOffset = 0) {
//	stream << std::endl;
	stream << "#BEGIN histogram: " << name << std::endl;

	mapT2 sum = 0;
	for (auto elem : hist) {
		sum += elem.second;
	}

	for (auto it : hist) {
			stream << it.first + valueOffset << ": " << it.second << " (" <<  std::fixed << std::setprecision(4) << double (it.second * 100) / double(sum) << "%)" << std::endl;
	}

	stream << "#END histogram: " << name << std::endl;

	stream << name << " avg: " << avg(hist, valueOffset) << std::endl;
}

std::string printDots(std::size_t val);
