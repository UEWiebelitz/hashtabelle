#pragma once

#include "dna_seq/dna_alph.h"
#include "dna_seq/canonized_kmer.h"
#include "runtime/hashtables/replacing.h"
#include "dna_seq/kmer_reader.h"

#include "runtime/hashtables/blocked_ht.h"
#include "runtime/hashtables/hashtable_multilevel.h"
#include "runtime/hashtables/delimited_linear_probing.h"
#include "runtime/page_layouts/ht_page_container.h"

#include "benchmark/hashtable_actions_kmer_vector.h"

#include "benchmark/infoprinter.h"

#include "runtime/page_layouts/packed_null.h"
#include "runtime/hashfunctions/fingerprint.h"
#include "runtime/page_layouts/lin_probing_container.h"
#include "runtime/hashtables/prefetching_ht.h"
#include "runtime/hashtables/actor_ht.h"

#include "template/bitshifting/wordcontainer.h"

#include <tuple>

template<class KmerIterator>
auto getKmerIterators(const std::vector<std::string> &filenames, size_t buffSize, size_t k) {
	std::vector<std::shared_ptr<KmerIterator>> iterators;
	for (const auto &file : filenames) {
		iterators.push_back(std::shared_ptr<KmerIterator>(new KmerIterator(file, buffSize, k)));
	}
	return iterators;
}

template<class HT, class KmerIterator>
void performActions(const CommandLine_Config &config, HT &ht) {
	InfoPrinter::printStaticData(ht);

	for (size_t actionIdx = 0; actionIdx < config.actions.size(); ++actionIdx) {
		const auto &act = config.actions[actionIdx];
		std::string actString;

		for (auto it : actionsMap) {
			if (it.second == act) {
				actString = it.first;
			}
		}

		std::cout << std::endl << "start action: " << actString << std::endl;

		OnFailure onFailure = STOP;
		if (config.actOnFailure.size() <= actionIdx) {
			std::cerr << "no action on failure given - using STOP" << std::endl;
		}
		TakeTime timer;

		//each action gets new iterators of files -> we will start from begin each time
		auto kmerIterators = getKmerIterators<KmerIterator>(config.files, config.fileBufferSize, config.k);
		if (act == INSERT) {
            auto stats = /*HashtableActions::*/insert<HT, KmerIterator> (ht, kmerIterators, onFailure);
			InfoPrinter::printInsertStats(ht);
			std::cout << std::endl;

			InfoPrinter::printActionStats(stats);
		}
		else if (act == LOOKUP) {
                auto stats = /*HashtableActions::*/lookup<HT, KmerIterator> (ht, kmerIterators, onFailure, config.parallelChunkCount, config.parallelChunkSize, config.threadCount);
				InfoPrinter::printActionStats(stats);
		}
		else if (act == COUNT) {
            auto stats = /*HashtableActions::*/count<HT, KmerIterator> (ht, kmerIterators, onFailure, config.counterBits);
			InfoPrinter::printCountStats(ht);
			std::cout << std::endl;
			InfoPrinter::printActionStats(stats);
		}
        else if(act == MAPPABILITY_DIRECT) {
            auto stats = /*HashtableActions::*/mappabilityLookup_direct<HT, KmerIterator> (ht, kmerIterators, config.expectedKmerCount);
//            InfoPrinter::printCountStats(ht);
            std::cout << std::endl;
            InfoPrinter::printActionStats(stats);
        }
        else if(act == MAPPABILITY) {
            auto stats = /*HashtableActions::*/mappabilityLookup<HT, KmerIterator> (
                        ht, kmerIterators, config.threadCount, config.parallelChunkCount, config.parallelChunkSize, config.expectedKmerCount);
//            InfoPrinter::printCountStats(ht);
            std::cout << std::endl;
            InfoPrinter::printActionStats(stats);
        }
		else {
            std::cerr << "action currently not implemented: " << act << std::endl;
		}

		std::cout << "time complete action: " << printDots(timer.getElapsedTime()) << "ms" << std::endl;
		std::cout << "finished action: " << actString << std::endl;
	}
}

#ifdef PREFETCH
template<class HT, class KmerIterator
         , typename std::enable_if<!is_instantiation_of<Runtime::Hashtable::Blocked, HT>::value, int>::type =0>
void performActions_prefetched(const CommandLine_Config &config, HT &ht_nonPref) {
    using Prefetching_HT = Runtime::Hashtable::PrefetchAhead<HT>;

	Prefetching_HT ht(ht_nonPref, config.prefetchAhead);
	InfoPrinter::printStaticData(ht);

	for (size_t actionIdx = 0; actionIdx < config.actions.size(); ++actionIdx) {
		const auto &act = config.actions[actionIdx];
		std::string actString;

		for (auto it : actionsMap) {
			if (it.second == act) {
				actString = it.first;
			}
		}

		std::cout << std::endl << "start action: " << actString << std::endl;

		OnFailure onFailure = STOP;
		if (config.actOnFailure.size() <= actionIdx) {
			std::cerr << "no action on failure given - using STOP" << std::endl;
		}
		TakeTime timer;

		//each action gets new iterators of files -> we will start from begin each time
		auto kmerIterators = getKmerIterators<KmerIterator>(config.files, config.fileBufferSize, config.k);
		if (act == INSERT) {
            auto stats = /*HashtableActions::*/insert(ht, kmerIterators, onFailure);
			InfoPrinter::printInsertStats(ht);
			std::cout << std::endl;

			InfoPrinter::printActionStats(stats);
		}
		else if (act == LOOKUP) {
                auto stats = /*HashtableActions::*/lookup<Prefetching_HT, KmerIterator> (ht, kmerIterators, onFailure, config.parallelChunkCount, config.parallelChunkSize, config.threadCount);
				InfoPrinter::printActionStats(stats);
		}
		else if (act == COUNT) {
            auto stats = /*HashtableActions::*/count<Prefetching_HT, KmerIterator> (ht, kmerIterators, onFailure, config.counterBits);
            InfoPrinter::printCountStats(ht);
            std::cout << std::endl;
            InfoPrinter::printActionStats(stats);
		}
        else if(act == MAPPABILITY_DIRECT) {
            auto stats = /*HashtableActions::*/mappabilityLookup_direct<Prefetching_HT, KmerIterator> (ht, kmerIterators, config.expectedKmerCount);
//            InfoPrinter::printCountStats(ht);
            std::cout << std::endl;
            InfoPrinter::printActionStats(stats);
        }
        else if(act == MAPPABILITY) {
            auto stats = /*HashtableActions::*/mappabilityLookup<Prefetching_HT, KmerIterator> (
                        ht, kmerIterators, config.threadCount, config.parallelChunkCount, config.parallelChunkSize, config.expectedKmerCount);
//            InfoPrinter::printCountStats(ht);
            std::cout << std::endl;
            InfoPrinter::printActionStats(stats);
        }
		else {
			std::cerr << "action currently not supported: " << act << std::endl;
		}

		std::cout << "time complete action: " << printDots(timer.getElapsedTime()) << "ms" << std::endl;
		std::cout << "finished action: " << actString << std::endl;
	}
}
#endif

template<class HT, class KmerIterator
         , typename std::enable_if<!is_instantiation_of<Runtime::Hashtable::Blocked, HT>::value, int>::type = 0>
void actOnHT(const CommandLine_Config &config, HT &ht) {
#ifdef PREFETCH
    if (config.prefetchAhead) {
        performActions_prefetched<HT, KmerIterator>(config, ht);
        return;
    }
#endif

    performActions<HT, KmerIterator>(config, ht);
}

template<class HT, class KmerIterator
         , typename std::enable_if<is_instantiation_of<Runtime::Hashtable::Blocked, HT>::value, int>::type = 0>
void actOnHT(const CommandLine_Config &config, HT &ht) {
    performActions<HT, KmerIterator>(config, ht);
}


void actOnBlockedPagedHT(const CommandLine_Config &config) {
    typedef Runtime::CanonizedKmer<DNA_Alph> KMer;
    typedef Runtime::CanonizedKmer<DNA_Alph> KeyT;
    typedef size_t ValueT;

    using HF = typename Runtime::HF_Fingerprint<KeyT>;
    using SubKey = typename HF::KeyStoreType;
    using SubHF = typename Runtime::HF_Fingerprint<WordContainer<SubKey>>;

    typedef typename Runtime::HT_PageContainer<Runtime::WordPacking_access> DataContainer;
    using RuntimeHT_Replacing = Actor_HT<Runtime::Hashtable::Replacing<size_t, ValueT, SubHF, DataContainer>>;

    auto *htCfg = dynamic_cast<const HTConfig_Blocked*>(config.htConfig);
    if (!htCfg) {
        throw std::bad_cast();
    }

    HF hf({std::random_device()() | 1,  2*config.k, KMer(config.k)});

    std::vector<DataContainer*> subDataContainers;
    std::vector<RuntimeHT_Replacing*> subHts;

    size_t subHtCount = htCfg->subHTCount();

    //ensure that capacity <= sum(subCapacities) <= capacity + subHtCount
    size_t subHtCapacity = (htCfg->capacity() + subHtCount -1) / subHtCount;

    if (htCfg->subHtType() != PAGED) {
        std::cerr << "sub ht type is currently not supported" << std::endl;
        throw std::invalid_argument("sub ht type is currently not supported");
    }

    for (size_t i=0; i<htCfg->subHTCount(); ++i) {
        std::vector<SubHF> hashFunctions;
        for (auto mult : htCfg->hfMultipliers()) {
            hashFunctions.push_back({mult, hf.fpBitCount(subHtCount), SubHF::KeyStoreType()});
        }
        DataContainer *dataContainer = new DataContainer(
                    subHtCapacity, htCfg->wordsPerPage_linProbingRange(), hashFunctions.size(), config.counterBits
                    , [&](size_t hashPositionCnt) {return hashFunctions.front().fpBitCount(hashPositionCnt);});
        subDataContainers.push_back(dataContainer);
#ifdef MANUAL_RAND
        subHts.push_back(new RuntimeHT_Replacing{hashFunctions, *dataContainer, config.replacingDist, htCfg->walkLength(), config.randomSeed});
#else
        subHts.push_back(new RuntimeHT_Replacing{hashFunctions, *dataContainer, htCfg->walkLength(), config.randomSeed});
#endif
    }

    if (config.prefetchAhead > 0) { //set subHTs as prefetching hashtables
#ifdef PREFETCH
        std::vector<Runtime::Hashtable::PrefetchAhead<RuntimeHT_Replacing>*> subHTsPrefetching;
        for (auto subHt : subHts) {
            subHTsPrefetching.push_back(new Runtime::Hashtable::PrefetchAhead<RuntimeHT_Replacing>(*subHt, config.prefetchAhead));
        }
        typedef Runtime::Hashtable::Blocked<typename KMer::WordType, ValueT, Runtime::Hashtable::PrefetchAhead<RuntimeHT_Replacing>, HF> BlockedHT;
        BlockedHT ht(subHTsPrefetching, hf, htCfg->batchCount(), htCfg->batchSize());
        actOnHT<BlockedHT, KmerIterator<KMer, size_t>>(config, ht);
#else
        throw std::invalid_argument("prefetching is not compiled but prefetch ahead was set in configuration");
#endif
    }
    else {
        typedef Runtime::Hashtable::Blocked<typename KMer::WordType, ValueT, RuntimeHT_Replacing, HF> BlockedHT;
        BlockedHT ht(subHts, hf, htCfg->batchCount(), htCfg->batchSize());
        actOnHT<BlockedHT, KmerIterator<KMer, size_t>>(config, ht);
    }

    for(auto ptr : subHts) {
        delete ptr;
    }
    subHts.clear();

    for(auto ptr : subDataContainers) {
        delete ptr;
    }
    subDataContainers.clear();
}


void actonOnBlockedLinHT(const CommandLine_Config &config) {
	typedef Runtime::CanonizedKmer<DNA_Alph> KMer;
	typedef Runtime::CanonizedKmer<DNA_Alph> KeyT;
	typedef size_t ValueT;

	using HF = typename Runtime::HF_Fingerprint<KeyT>;
	using SubKey = typename HF::KeyStoreType;
	using SubHF = typename Runtime::HF_Fingerprint<WordContainer<SubKey>>;

	typedef typename Runtime::LinProbContainer DataContainer;
    using RuntimeHT_Replacing = Actor_HT<Runtime::Hashtable::Replacing<size_t, ValueT, SubHF, DataContainer>>;

	auto *htCfg = dynamic_cast<const HTConfig_Blocked*>(config.htConfig);
	if (!htCfg) {
		throw std::bad_cast();
	}

	HF hf({std::random_device()() | 1,  2*config.k, KMer(config.k)});

	std::vector<DataContainer*> subDataContainers;
	std::vector<RuntimeHT_Replacing*> subHts;

	size_t subHtCount = htCfg->subHTCount();

	//ensure that capacity <= sum(subCapacities) <= capacity + subHtCount
	size_t subHtCapacity = (htCfg->capacity() + subHtCount -1) / subHtCount;

	if (htCfg->subHtType() != LINEAR) {
		std::cerr << "sub ht type is currently not supported" << std::endl;
		throw std::invalid_argument("sub ht type is currently not supported");
	}

	for (size_t i=0; i<htCfg->subHTCount(); ++i) {
		std::vector<SubHF> hashFunctions;
		for (auto mult : htCfg->hfMultipliers()) {
			hashFunctions.push_back({mult, hf.fpBitCount(subHtCount), SubHF::KeyStoreType()});
		}
		DataContainer *dataContainer = new DataContainer(
					subHtCapacity, htCfg->wordsPerPage_linProbingRange(), hashFunctions.size(), config.counterBits
					, [&](size_t hashPositionCnt) {return hashFunctions.front().fpBitCount(hashPositionCnt);});
		subDataContainers.push_back(dataContainer);
#ifdef MANUAL_RAND
        subHts.push_back(new RuntimeHT_Replacing{hashFunctions, *dataContainer, config.replacingDist, htCfg->walkLength(), config.randomSeed});
#else
		subHts.push_back(new RuntimeHT_Replacing{hashFunctions, *dataContainer, htCfg->walkLength(), config.randomSeed});
#endif
	}

    if (config.prefetchAhead > 0) { //set subHTs as prefetching hashtables
#ifdef PREFETCH
        std::vector<Runtime::Hashtable::PrefetchAhead<RuntimeHT_Replacing>*> subHTsPrefetching;
        for (auto subHt : subHts) {
            subHTsPrefetching.push_back(new Runtime::Hashtable::PrefetchAhead<RuntimeHT_Replacing>(*subHt, config.prefetchAhead));
        }
        typedef Runtime::Hashtable::Blocked<typename KMer::WordType, ValueT, Runtime::Hashtable::PrefetchAhead<RuntimeHT_Replacing>, HF> BlockedHT;
        BlockedHT ht(subHTsPrefetching, hf, htCfg->batchCount(), htCfg->batchSize());
        actOnHT<BlockedHT, KmerIterator<KMer, size_t>>(config, ht);
#else
        throw std::invalid_argument("prefetching is not compiled but prefetch ahead was set in configuration");
#endif
    }
    else {
        typedef Runtime::Hashtable::Blocked<typename KMer::WordType, ValueT, RuntimeHT_Replacing, HF> BlockedHT;
        BlockedHT ht(subHts, hf, htCfg->batchCount(), htCfg->batchSize());
        actOnHT<BlockedHT, KmerIterator<KMer, size_t>>(config, ht);
    }

    for(auto ptr : subHts) {
        delete ptr;
    }
    subHts.clear();

    for(auto ptr : subDataContainers) {
        delete ptr;
    }
    subDataContainers.clear();
}

void actOnBlockedHT(const CommandLine_Config &config) {
    auto *htCfg = dynamic_cast<const HTConfig_Blocked*>(config.htConfig);
    if (!htCfg) {
        throw std::bad_cast();
    }

    if (htCfg->subHtType() == LINEAR) {
        actonOnBlockedLinHT(config);
    }
    else if (htCfg->subHtType() == PAGED) {
        actOnBlockedPagedHT(config);
    }
    else {
        std::cerr << "sub ht type is currently not supported" << std::endl;
        throw std::invalid_argument("sub ht type is currently not supported");
    }

}


template<class ...SubHTs>
void actonOnMultilevelHT(const CommandLine_Config &config, SubHTs&...hts) {
	typedef Runtime::CanonizedKmer<DNA_Alph> KMer;
	typedef Runtime::CanonizedKmer<DNA_Alph> KeyT;
	typedef size_t ValueT;
    using MultiLevelHT = Actor_HT<HashtableMultilevel<typename KeyT::WordType, ValueT, SubHTs...>>;

	MultiLevelHT ht(hts...);

	actOnHT<MultiLevelHT, KmerIterator<KMer, size_t>>(config, ht);
}

void actonOnMultilevelHTLinear_Prefetching(const CommandLine_Config &config) {
    typedef Runtime::CanonizedKmer<DNA_Alph> KMer;
    typedef size_t ValueT;
    typedef Runtime::HF_Fingerprint<KMer> FpHf;

    using DataContainer = typename Runtime::LinProbContainer;
    using RuntimeHT_Replacing = Runtime::Hashtable::Replacing<typename KMer::WordType, ValueT, FpHf, DataContainer>;

    auto *htCfg = dynamic_cast<const HTConfig_Multilevel*>(config.htConfig);
    auto *ht1Cfg = dynamic_cast<const HTConfig*>(htCfg->subHtConfigs()[0]);
    auto *ht2Cfg = dynamic_cast<const HTConfig*>(htCfg->subHtConfigs()[1]);

    std::vector<FpHf> hash1Functions;
    for (auto mult : ht1Cfg->hfMultipliers()) {
        hash1Functions.push_back({mult, 2*config.k, KMer(config.k)});
    }

//	if(hash1Functions.size() > 1) {
//		std::cerr << "currently the linear probing hash table cannot handle multiple hash functions" << std::endl;
//	}
    DataContainer dataContainer1(ht1Cfg->capacity(), ht1Cfg->wordsPerPage_linProbingRange(), hash1Functions.size(), config.counterBits
                                , [&](size_t hashPositionCnt) {return hash1Functions.front().fpBitCount(hashPositionCnt);});
#ifdef MANUAL_RAND
   RuntimeHT_Replacing ht1(hash1Functions, dataContainer1, {1}, ht1Cfg->walkLength(), config.randomSeed);
#else
    RuntimeHT_Replacing ht1(hash1Functions, dataContainer1, ht1Cfg->walkLength(), config.randomSeed);
#endif

    std::vector<FpHf> hash2Functions;
    for (auto mult : ht2Cfg->hfMultipliers()) {
        hash2Functions.push_back({mult, 2*config.k, KMer(config.k)});
    }

//	std::size_t elemCount, size_t wordsPerPage, size_t hfCnt, size_t valBitCnt, const std::function<std::size_t(std::size_t /*hashPositionsCnt*/)> &bitCount

    DataContainer dataContainer2(ht2Cfg->capacity(), ht2Cfg->wordsPerPage_linProbingRange(), hash2Functions.size(), config.counterBits
                                , [&](size_t hashPositionCnt) {return hash2Functions.front().fpBitCount(hashPositionCnt);});
#ifdef MANUAL_RAND
    RuntimeHT_Replacing ht2(hash2Functions, dataContainer2, config.replacingDist, ht2Cfg->walkLength(), config.randomSeed);
#else
    RuntimeHT_Replacing ht2(hash2Functions, dataContainer2, ht2Cfg->walkLength(), config.randomSeed);
#endif

    actonOnMultilevelHT(config, ht1, ht2);
}

void actonOnMultilevelHTLinear(const CommandLine_Config &config) {
	typedef Runtime::CanonizedKmer<DNA_Alph> KMer;
	typedef size_t ValueT;
	typedef Runtime::HF_Fingerprint<KMer> FpHf;

	using DataContainer = typename Runtime::LinProbContainer;
	typedef Runtime::Hashtable::Replacing<typename KMer::WordType, ValueT, FpHf, DataContainer> RuntimeHT_Replacing;

	auto *htCfg = dynamic_cast<const HTConfig_Multilevel*>(config.htConfig);
	auto *ht1Cfg = dynamic_cast<const HTConfig*>(htCfg->subHtConfigs()[0]);
	auto *ht2Cfg = dynamic_cast<const HTConfig*>(htCfg->subHtConfigs()[1]);

	std::vector<FpHf> hash1Functions;
	for (auto mult : ht1Cfg->hfMultipliers()) {
		hash1Functions.push_back({mult, 2*config.k, KMer(config.k)});
	}

//	if(hash1Functions.size() > 1) {
//		std::cerr << "currently the linear probing hash table cannot handle multiple hash functions" << std::endl;
//	}
	DataContainer dataContainer1(ht1Cfg->capacity(), ht1Cfg->wordsPerPage_linProbingRange(), hash1Functions.size(), config.counterBits
								, [&](size_t hashPositionCnt) {return hash1Functions.front().fpBitCount(hashPositionCnt);});
#ifdef MANUAL_RAND
    RuntimeHT_Replacing ht1(hash1Functions, dataContainer1, {1}, ht1Cfg->walkLength(), config.randomSeed);
#else
	RuntimeHT_Replacing ht1(hash1Functions, dataContainer1, ht1Cfg->walkLength(), config.randomSeed);
#endif

	std::vector<FpHf> hash2Functions;
	for (auto mult : ht2Cfg->hfMultipliers()) {
		hash2Functions.push_back({mult, 2*config.k, KMer(config.k)});
	}

//	std::size_t elemCount, size_t wordsPerPage, size_t hfCnt, size_t valBitCnt, const std::function<std::size_t(std::size_t /*hashPositionsCnt*/)> &bitCount

	DataContainer dataContainer2(ht2Cfg->capacity(), ht2Cfg->wordsPerPage_linProbingRange(), hash2Functions.size(), config.counterBits
                                , [&](size_t hashPositionCnt) {return hash2Functions.front().fpBitCount(hashPositionCnt);});
#ifdef MANUAL_RAND
    RuntimeHT_Replacing ht2(hash2Functions, dataContainer2, config.replacingDist, ht2Cfg->walkLength(), config.randomSeed);
#else
	RuntimeHT_Replacing ht2(hash2Functions, dataContainer2, ht2Cfg->walkLength(), config.randomSeed);
#endif

	actonOnMultilevelHT(config, ht1, ht2);
}

void actonOnMultilevelHTPagedReplacing(const CommandLine_Config &config) {
	typedef Runtime::CanonizedKmer<DNA_Alph> KMer;
	typedef size_t ValueT;
	typedef Runtime::HF_Fingerprint<KMer> FpHf;
	using DataContainer = typename Runtime::HT_PageContainer<Runtime::WordPacking_access>;
	typedef Runtime::Hashtable::Replacing<typename KMer::WordType, ValueT, FpHf, DataContainer> RuntimeHT_Replacing;

	auto *htCfg = dynamic_cast<const HTConfig_Multilevel*>(config.htConfig);
	auto *ht1Cfg = dynamic_cast<const HTConfig*>(htCfg->subHtConfigs()[0]);
	auto *ht2Cfg = dynamic_cast<const HTConfig*>(htCfg->subHtConfigs()[1]);

	std::vector<FpHf> hash1Functions;
	for (auto mult : ht1Cfg->hfMultipliers()) {
		hash1Functions.push_back({mult, 2*config.k, KMer(config.k)});
	}

    if (hash1Functions.size() > 1) {
		std::cerr << "currently only one multiplier for linear probing hash table is supported" << std::endl;
	}
    DataContainer dataContainer(ht1Cfg->capacity(), ht1Cfg->wordsPerPage_linProbingRange(), hash1Functions.size(), config.counterBits
								, [&](size_t hashPositionCnt) {return hash1Functions.front().fpBitCount(hashPositionCnt);});
#ifdef MANUAL_RAND
    RuntimeHT_Replacing ht1(hash1Functions, dataContainer, {1}, ht1Cfg->walkLength(), config.randomSeed);
#else
	RuntimeHT_Replacing ht1(hash1Functions, dataContainer, ht1Cfg->walkLength(), config.randomSeed);
#endif


	std::vector<FpHf> hash2Functions;
	for (auto mult : ht2Cfg->hfMultipliers()) {
		hash2Functions.push_back({mult, 2*config.k, KMer(config.k)});
	}
	DataContainer dataContainer2(ht2Cfg->capacity(), ht2Cfg->wordsPerPage_linProbingRange(), hash2Functions.size(), config.counterBits
								 , [&](size_t hashPositionCnt) {return hash2Functions.front().fpBitCount(hashPositionCnt);});
#ifdef MANUAL_RAND
    RuntimeHT_Replacing ht2(hash2Functions, dataContainer2, config.replacingDist, ht2Cfg->walkLength(), config.randomSeed);
#else
	RuntimeHT_Replacing ht2(hash2Functions, dataContainer2, ht2Cfg->walkLength(), config.randomSeed);
#endif

	actonOnMultilevelHT(config, ht1, ht2);
}

void actonOnHTLinProbReplacing(const CommandLine_Config &config) {
	typedef Runtime::CanonizedKmer<DNA_Alph> KeyT;
	typedef size_t ValueT;
	typedef Runtime::HF_Fingerprint<KeyT> FpHf;
	typedef typename Runtime::LinProbContainer DataContainer;
    using RuntimeHT_Replacing = Actor_HT<Runtime::Hashtable::Replacing<typename KeyT::WordType, ValueT, FpHf, DataContainer>>;

	auto *htCfg = dynamic_cast<const HTConfig*>(config.htConfig);

	std::vector<FpHf> hashFunctions;
	for (auto mult : htCfg->hfMultipliers()) {
		hashFunctions.push_back({mult, 2*config.k, KeyT(config.k)});
	}

	DataContainer dataContainer(htCfg->capacity(), htCfg->wordsPerPage_linProbingRange(), hashFunctions.size(), config.counterBits
								, [&](size_t hashPositionCnt) {return hashFunctions.front().fpBitCount(hashPositionCnt);});
#ifdef MANUAL_RAND
    RuntimeHT_Replacing ht(hashFunctions, dataContainer, config.replacingDist, htCfg->walkLength(), config.randomSeed);
#else
	RuntimeHT_Replacing ht(hashFunctions, dataContainer, htCfg->walkLength(), config.randomSeed);
#endif

	actOnHT<RuntimeHT_Replacing, KmerIterator<KeyT, size_t>>(config, ht);
}

void actOnMultilevel(const CommandLine_Config &commandLineCfg) {
	switch (dynamic_cast<HTConfig *>(commandLineCfg.htConfig)->htType()) {
	case PAGED:
		actonOnMultilevelHTPagedReplacing(commandLineCfg);
		break;
	case LINEAR:
		actonOnMultilevelHTLinear(commandLineCfg);
		break;
	default:
		throw std::invalid_argument("a sub hash table cannot be a multilevel or blocked hashtable");
	}
}

void actonOnReplacingHT(const CommandLine_Config &config) {
	typedef Runtime::CanonizedKmer<DNA_Alph> KeyT;
	typedef size_t ValueT;
	typedef Runtime::HF_Fingerprint<KeyT> FpHf;
	using DataContainer = typename Runtime::HT_PageContainer<Runtime::WordPacking_access>;
    using RuntimeHT_Replacing = Actor_HT<Runtime::Hashtable::Replacing<typename KeyT::WordType, ValueT, FpHf, DataContainer>>;

	auto *htCfg = dynamic_cast<const HTConfig*>(config.htConfig);

	std::vector<FpHf> hashFunctions;
	for (auto mult : htCfg->hfMultipliers()) {
		if (! (mult & 1)) {
			std::cerr << "the hash function multipliers have to be odd!" << std::endl;
			exit(1);
		}
		hashFunctions.push_back({mult, 2*config.k, KeyT(config.k)});
	}
	DataContainer dataContainer(htCfg->capacity(), htCfg->wordsPerPage_linProbingRange(), hashFunctions.size(), config.counterBits
								, [&](size_t hashPositionCnt) {return hashFunctions.front().fpBitCount(hashPositionCnt);});
#ifdef MANUAL_RAND
    RuntimeHT_Replacing ht(hashFunctions, dataContainer, config.replacingDist, htCfg->walkLength(), config.randomSeed);
#else
	RuntimeHT_Replacing ht(hashFunctions, dataContainer, htCfg->walkLength(), config.randomSeed);
#endif

	actOnHT<RuntimeHT_Replacing, KmerIterator<KeyT, size_t>>(config, ht);
}

void actonOnHT(const CommandLine_Config &config) {
	switch (config.htConfig->htType()) {
	case MULTI_LEVEL:
	{
		auto htCfg_Mult = dynamic_cast<HTConfig_Multilevel *>(config.htConfig);
		if (htCfg_Mult->subHtConfigs().size() != 2) {
			std::cerr << "currently multilevel hashtables must have exactly two sub hash tables." << std::endl;
		}

		if (htCfg_Mult->subHtConfigs()[0]->htType() == LINEAR) {
			actonOnMultilevelHTLinear(config);
		}
		else if (htCfg_Mult->subHtConfigs()[0]->htType() == PAGED) {
			actonOnMultilevelHTPagedReplacing(config);
		}
	}
		break;
	case PAGED:
		actonOnReplacingHT(config);
		break;
	case LINEAR:
		actonOnHTLinProbReplacing(config);
		break;
	case BLOCKED:
        actOnBlockedHT(config);
		break;
	}
}
