#pragma once

#include "hashtableactions.h"

#include "runtime/hashtables/blocked_ht.h"

//namespace HashtableActions
//{

template<class HT, class KmerIterator>
static std::vector<LookupStats> lookup(const HT &ht, std::vector<std::shared_ptr<KmerIterator>> &kmerIterators
                                       , OnFailure onFailure, size_t parallelBatchCount, size_t parallelBatchSize, size_t threadCount) {
    std::vector<LookupStats> stats;
    for (auto &kmerIt : kmerIterators) {
        if (parallelBatchSize > 0 && threadCount > 0 && parallelBatchCount > 0) {
            stats.push_back(HashtableActions::lookup_parallel<HT, KmerIterator>(ht, *kmerIt, threadCount, parallelBatchCount, parallelBatchSize));
        }
        else {
            if (parallelBatchSize > 0) {
                throw std::invalid_argument("parallel batch size is set but thread count is not set. Running sequential.");
            }
            if (parallelBatchCount > 0) {
                throw std::invalid_argument("parallel batch size is set but thread count is not set. Running sequential.");
            }
            if (threadCount > 0) {
                throw std::invalid_argument("thread count is set but parallel batch size is not set. Running sequential.");
            }
            stats.push_back(HashtableActions::lookup(ht, *kmerIt, onFailure));
        }
    }

    return stats;
}

template<class HT, class KmerIterator>
static std::vector<InsertStats> count(HT &ht, std::vector<std::shared_ptr<KmerIterator>> &kmerIterators, OnFailure onFailure, size_t counterBits) {
    std::vector<InsertStats> stats;
    for (auto &kmerIt : kmerIterators) {
        stats.push_back(HashtableActions::count(ht, *kmerIt, onFailure, counterBits));
    }

    return stats;
}

template<class HT, class KmerIterator>
static std::vector<InsertStats> insert(HT &ht, std::vector<std::shared_ptr<KmerIterator>> &kmerIterators, OnFailure onFailure) {
    std::vector<InsertStats> stats;
    for (auto &kmerIt : kmerIterators) {
        stats.push_back(HashtableActions::insert(ht, *kmerIt, onFailure));
    }

    return stats;
}

template<class HT, class KmerIterator>
static auto mappabilityLookup(HT &ht, std::vector<std::shared_ptr<KmerIterator>> &kmerIterators, size_t threadCount, size_t pageCount, size_t pageSize, const size_t expectedKmerCount) {
    using RetType = decltype (HashtableActions::mappabilityLookup(ht, *kmerIterators.front(), threadCount, pageCount, pageSize, expectedKmerCount));
    std::vector<RetType> stats;

    for (auto &kmerIt : kmerIterators) {
        stats.push_back(HashtableActions::mappabilityLookup(ht, *kmerIt, threadCount, pageCount, pageSize, expectedKmerCount));
    }

    return stats;
}

template<class HT, class KmerIterator>
static auto mappabilityLookup_direct(HT &ht, std::vector<std::shared_ptr<KmerIterator>> &kmerIterators, const size_t expectedKmerCount) {
    using RetType = decltype (HashtableActions::mappabilityLookup_direct(ht, *kmerIterators.front(), expectedKmerCount));
    std::vector<RetType> stats;

    for (auto &kmerIt : kmerIterators) {
        stats.push_back(HashtableActions::mappabilityLookup_direct(ht, *kmerIt, expectedKmerCount));
    }

    return stats;
}
//}
