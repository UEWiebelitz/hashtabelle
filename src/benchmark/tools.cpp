#include "tools.h"

std::string printDots(std::size_t val) {
    if (val == 0) {
        return "0";
    }

    std::string str;
    bool first = true;

    while (val != 0) {
        if (!first) {
            str = ',' + str;
        }
        else {
            first = false;
        }

        std::string currblock = std::to_string(val % 1000);
        val /= 1000;

        if (val) {
            currblock.insert(0, 3 - currblock.size(), '0');
        }
        str = currblock + str;

    }
    return str;
}
