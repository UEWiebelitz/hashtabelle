#include "actionstatistics.h"

#include "tools.h"

ActionStats ActionStats::operator +(const ActionStats &other) const {
	ActionStats stats = *this;
	stats += other;
	return stats;
}

ActionStats &ActionStats::operator +=(const ActionStats &other) {
	timeElapsed += other.timeElapsed;
	kmerCount += other.kmerCount;
	return *this;
}

std::ostream &ActionStats::printToStream(std::ostream &stream) const {
	stream << "runtime (micro sec.): " << printDots(timeElapsed) << std::endl;
	stream << "k-mers computed: " << printDots(kmerCount) << std::endl;

	return stream;
}

std::string ActionStats::getName() const {
	return "ActionStats";
}

InsertStats InsertStats::operator +(const InsertStats &other) const {
	InsertStats stats = *this;
	stats += other;
	return stats;
}

InsertStats &InsertStats::operator +=(const InsertStats &other) {
	ActionStats::operator+=(other);
	inserted += other.inserted;
	notInserted += other.notInserted;
	existed += other.existed;
	return *this;
}

std::ostream &InsertStats::printToStream(std::ostream &stream) const {
	ActionStats::printToStream(stream);

	stream << "inserted: " << printDots(inserted) << std::endl;
	stream << "not inserted: " << printDots(notInserted) << std::endl;
	stream << "existed: " << printDots(existed) << std::endl;

	return stream;
}

std::string InsertStats::getName() const {
	return "InsertStats";
}

LookupStats LookupStats::operator +(const LookupStats &other) const {
	LookupStats stats = *this;
	stats += other;
	return stats;
}

LookupStats &LookupStats::operator +=(const LookupStats &other) {
	ActionStats::operator+=(other);
	exists += other.exists;
	notExists += other.notExists;
	unsure += other.unsure;
	return *this;
}

std::ostream &LookupStats::printToStream(std::ostream &stream) const {
	ActionStats::printToStream(stream);

	stream << "exists: " << printDots(exists) << std::endl;
	stream << "not exists: " << printDots(notExists) << std::endl;
	stream << "unsure: " << printDots(unsure) << std::endl;

	return stream;
}

std::string LookupStats::getName() const {
	return "LookupStats";
}

MappabilityStats MappabilityStats::operator +(const MappabilityStats &other) const
{
    MappabilityStats stats = *this;
    stats += other;
    return stats;
}

MappabilityStats &MappabilityStats::operator +=(const MappabilityStats &other)
{
    ActionStats::operator+=(other);
    uniqueMappable += other.uniqueMappable;
    return *this;
}

std::ostream &MappabilityStats::printToStream(std::ostream &stream) const
{
    ActionStats::printToStream(stream);
    stream << "uniqueMappable: " << printDots(uniqueMappable) << std::endl;
    return stream;
}

std::string MappabilityStats::getName() const
{
    return "MappabilityStats";
}

std::ostream &operator <<(std::ostream &stream, const ActionStats &stats)
{
	std::cout << "#BEGIN " << stats.getName() << std::endl;
	stats.printToStream(stream);
	std::cout << "#END " << stats.getName() << std::endl;
	return stream;
}
