#ifndef TAKETIME_H
#define TAKETIME_H

#include <string>
#include <chrono>

class TakeTime
{
protected:
	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

public:
	unsigned long long getElapsedTime() const;
};

class LoggingTimer : public TakeTime
{
	friend class TimerManagement;
	std::string name;

public:
	LoggingTimer(const std::string &name);
	~LoggingTimer();
};

#include <unordered_map>

class TimerManagement {
	typedef  std::unordered_map<std::string, unsigned long long> TimesMap;
	static TimesMap times;

public:
	static void addTime(const LoggingTimer &takeTime);
	static unsigned long long getTime(const std::string &name);
};

#endif // TAKETIME_H
