#pragma once

#include <type_traits>
#include <string>
#include <vector>
#include <map>
#include <thread>
#include <atomic>
#include <mutex>

#include "benchmark/taketime.h"
#include "return_indicators.h"
#include "tools.h"
#include "compile_hints/likely_hint.h"

#include "dna_seq/cached_kmeriterator.h"
#include "thread_communication/circular_buffer.h"
#include "actionstatistics.h"
#include "commandline_config.h"

#include "runtime/hashtables/prefetching_ht.h"
#include "runtime/hashtables/blocked_ht.h"
#include "runtime/hashtables/blocked_ht_non_parallel.h"
#include "runtime/hashtables/actor_ht.h"

#include "template/is_instantiation_of.h"

#include "hashtable_actions_kmer_vector.h"
#include "dna_seq/canonized_kmer.h"
#include "dna_seq/dna_alph.h"

struct HashtableActions {

template<class HT, class KmerIterator, typename std::enable_if<!is_instantiation_of<Runtime::Hashtable::Blocked, HT>::value, int>::type = 0>
static auto mappabilityLookup_direct(HT &ht, KmerIterator &kmers, const size_t expectedKmerCount) {
    std::cout << "mappabilityLookup_direct" << std::endl;

    TakeTime timer;

    size_t kmerCount = 0;
    using MappabilityCounter = unsigned char;

    //we do not need to take care of atomicity of the counter because direct access does not cause parallel execution
    std::vector<MappabilityCounter> mappability;
    mappability.resize(expectedKmerCount, 0);
    size_t mappabilityPos = 0;


    while (likely(!kmers.eof())) {
#ifdef DEBUG_COMPILE
            if (mappabilityPos >= expectedKmerCount) {
                throw std::runtime_error("actual kmer count exceeds expectedKmerCount");
            }
#endif

        Runtime::CanonizedKmer<DNA_Alph> kmer(*kmers);

        size_t count = 0;
        auto result = ht.directValue(kmer);
        if (result.res() == LOOKUP_EXISTS) {
            count = std::min(static_cast<size_t>(count) +  result.get() +1, static_cast<size_t>(std::numeric_limits<MappabilityCounter>::max()));
        }
        ++kmerCount;

        for (size_t i=0; i<kmer.getK(); i++) {
            for (size_t j=0; j<kmer.alph.alphSize -1; ++j) {
                ++kmerCount;
                kmer.inc(i);
                auto result = ht.directValue(kmer);
                if (result.res() == LOOKUP_EXISTS) {
                    //the variable count is not accessed in parallel. We do not need to take atomicity into account.
                    count = std::min(static_cast<size_t>(count) +  result.get() +1, static_cast<size_t>(std::numeric_limits<MappabilityCounter>::max()));
                }
            }
            kmer.inc(i); //increment once more to reset to former value
        }

        count = std::min(count, static_cast<size_t>(std::numeric_limits<MappabilityCounter>::max()));
        mappability[mappabilityPos] = static_cast<MappabilityCounter>(count);
        ++mappabilityPos;

        ++kmers;
    }

    MappabilityStats stats; // += sumActor.stats()
    stats.timeElapsed = timer.getElapsedTime();
    stats.kmerCount = kmerCount;

    for (auto count : mappability) {
        if (count == 1) {
            ++stats.uniqueMappable;
        }
    }

    return stats;
}


template<class HT, class MappabilityCounter, class isAtomic = void>
class MappabilityAct;

template<class HT, class MappabilityCounter>
class MappabilityAct<HT, MappabilityCounter, typename std::enable_if<!is_instantiation_of<std::atomic, MappabilityCounter>::value>::type>  {
    MappabilityCounter *counter;

public:
    MappabilityAct(const MappabilityAct &other)
        : counter(other.counter) {}

    MappabilityAct(MappabilityCounter &counter)
        : counter(&counter) {}


    FORCE_INLINE void act(typename HT::HT_ValueResult accSupp) {
        if (accSupp.res() == LOOKUP_EXISTS) {
                *counter = std::min(static_cast<size_t>(*counter) +  accSupp.get() +1, static_cast<size_t>(std::numeric_limits<MappabilityCounter>::max()));
        }
    }
};

template<class HT, class AtomicMappabilityCounter>
class MappabilityAct<HT, AtomicMappabilityCounter, typename std::enable_if<is_instantiation_of<std::atomic, AtomicMappabilityCounter>::value>::type>  {
    AtomicMappabilityCounter *counter;

public:
    MappabilityAct(const MappabilityAct &other)
        : counter(other.counter) {}

    MappabilityAct(AtomicMappabilityCounter &counter)
        : counter(&counter) {}


    FORCE_INLINE void act(typename HT::HT_ValueResult accSupp) {
        if (accSupp.res() == LOOKUP_EXISTS) {
            //for atomic access we have to set the counter differently

            ///this might overflow the counter
            //(*counter) += static_cast<size_t>(accSupp.get()) +1;
            ///and this might not be atomic anymore
            //(*counter) = (static_cast<size_t>(counter->load()) + static_cast<size_t>(accSupp.get()) +1) % std::numeric_limits<MappabilityCounter>::max();

            ///but this should work:
            size_t addVal = accSupp.get();
            ++addVal;

            size_t valBefore = counter->fetch_add(static_cast<size_t>(addVal));
            if (valBefore + addVal > std::numeric_limits<typename AtomicMappabilityCounter::__integral_type>::max()) {
                counter->store(std::numeric_limits<typename AtomicMappabilityCounter::__integral_type>::max());
            }
        }
    }
};

//template<class HT, class MappabilityCounter
//         ,  typename std::enable_if<is_instantiation_of<std::atomic, MappabilityCounter>::value, int>::type = 0>
//class MappabilityAct<HT, MappabilityCounter, 1> {
//     typename is_instantiation_of<std::atomic, MappabilityCounter>::value, int>::type = 0 *counter;

//public:
//    MappabilityAct(const MappabilityAct &other)
//        : counter(other.counter) {}

//    MappabilityAct(MappabilityCounter &counter)
//        : counter(&counter) {}


//    template<typename std::enable_if<!is_instantiation_of<std::atomic, MappabilityCounter>::value, int>::type = 0>
//    FORCE_INLINE void act(typename HT::HT_ValueResult accSupp) {
//        if (accSupp.res() == LOOKUP_EXISTS) {
//                *counter = std::min(static_cast<size_t>(*counter) +  accSupp.get() +1, static_cast<size_t>(std::numeric_limits<MappabilityCounter>::max()));
//        }
//    }

//    template<typename std::enable_if<is_instantiation_of<std::atomic, MappabilityCounter>::value, int>::type = 0>
//    FORCE_INLINE void act(typename HT::HT_ValueResult accSupp) {
//        if (accSupp.res() == LOOKUP_EXISTS) {
//            //for atomic access we have to set the counter differently

//            ///this might overflow the counter
//            //(*counter) += static_cast<size_t>(accSupp.get()) +1;
//            ///and this might not be atomic anymore
//            //(*counter) = (static_cast<size_t>(counter->load()) + static_cast<size_t>(accSupp.get()) +1) % std::numeric_limits<MappabilityCounter>::max();

//            ///but this should work:
//            size_t addVal = accSupp.get();
//            ++addVal;

//            size_t valBefore = counter->fetch_add(static_cast<size_t>(addVal));
//            if (valBefore + addVal > std::numeric_limits<MappabilityCounter>::max()) {
//                counter->store(std::numeric_limits<MappabilityCounter>::max());
//            }
//        }
//    }
//};

/**
 * @brief mappabilityLookup_accessor uses the accessor concept to check the mappability of given kmers.
 */
template<class HT, class KmerIterator>
static auto mappabilityLookup_accessor(HT &ht, KmerIterator &kmers, const size_t expectedKmerCount) {
    std::cout << "mappabilityLookup_accessor" << std::endl;

    TakeTime timer;
    using MappabilityCounter = unsigned char;

    //we set the counter type atomic if the hashtable is of type Blocked, because it causes parallel access
    using AtomicMapabilityCounter = typename std::conditional<
        is_instantiation_of<Runtime::Hashtable::Blocked, HT>::value
        , std::atomic<MappabilityCounter>, MappabilityCounter>::type;

    AtomicMapabilityCounter *mappability = new AtomicMapabilityCounter[expectedKmerCount];
    for (size_t i=0; i<expectedKmerCount; ++i) {
        mappability[i] = 0;
    }



    struct InsertInfo {
            decltype (*kmers) kmer;
            MappabilityAct<HT, AtomicMapabilityCounter> act;
    };

    size_t kmerCount = 0;
    size_t mappabilityPos = 0;

    {
        auto valueAcc = ht.template getValueAccessor<MappabilityAct<HT, AtomicMapabilityCounter>>();

        while (likely(!kmers.eof())) {
#ifdef DEBUG_COMPILE
            if (mappabilityPos >= expectedKmerCount) {
                throw std::runtime_error("actual kmer count exceeds expectedKmerCount");
            }
#endif

            MappabilityAct<HT, AtomicMapabilityCounter> act(mappability[mappabilityPos]);
            Runtime::CanonizedKmer<DNA_Alph> kmer(*kmers);
            valueAcc.value(kmer, act);

            for (size_t i=0; i<kmer.getK(); i++) {
                for (size_t j=0; j<kmer.alph.alphSize -1; ++j) {
                    kmer.inc(i);
                    valueAcc.value(kmer, act);
                }
                kmer.inc(i); //increment once more to reset to former value
            }

            ++kmerCount;
            ++mappabilityPos;
            ++kmers;
        }
    }

    MappabilityStats stats; // += sumActor.stats()
    stats.timeElapsed = timer.getElapsedTime();
    stats.kmerCount = kmerCount;

    for (size_t i=0; i<expectedKmerCount; ++i) {
        if (mappability[i] == 1) {
            ++stats.uniqueMappable;
        }
    }
    delete[] mappability;

    return stats;
}

/**
 * @brief mappabilityLookup_parallel produces threads for parralel reading access to the hashtable.
 */
template<class HT, class KmerIterator, typename std::enable_if<!is_instantiation_of<Runtime::Hashtable::Blocked, HT>::value, int>::type = 0>
static auto mappabilityLookup_parallel(HT &ht, KmerIterator &kmers
                                       , const size_t threadCount, const size_t pageCount, const size_t pageSize, const size_t expectedKmerCount) {
    std::cout << "mappabilityLookup_parallel" << std::endl;

    TakeTime timer;

    using MappabilityCounter = unsigned char;
    using AtomicMapabilityCounter = std::atomic<MappabilityCounter>;

    AtomicMapabilityCounter *mappability = new AtomicMapabilityCounter[expectedKmerCount];
    for(size_t i=0; i<expectedKmerCount; ++i) {
        mappability[i] = 0;
    }

    class MappabilityAct {
        AtomicMapabilityCounter *counter;

    public:
        MappabilityAct(const MappabilityAct &other)
            : counter(other.counter) {}

        MappabilityAct(AtomicMapabilityCounter &counter)
            : counter(&counter) {}

        void act(typename HT::HT_ValueResult accSupp) {
            if (accSupp.res() == LOOKUP_EXISTS) {
                size_t addVal = accSupp.get();
                ++addVal;
                size_t valBefore = counter->fetch_add(static_cast<size_t>(addVal));
                if (valBefore + addVal > std::numeric_limits<MappabilityCounter>::max()) {
                        counter->store(std::numeric_limits<MappabilityCounter>::max());
                }
            }
        }
    };

    //the kmer is always exchanged together with the corresponding actor
    struct InsertInfo {
            decltype (*kmers) kmer;
            MappabilityAct act;
    };

    //each thread needs its own PagedThreadComm for exchange of kmers and actors.
    std::vector<PagedThreadComm<InsertInfo>*> pagedComm(threadCount, nullptr);
    std::vector<std::thread> threads;
    threads.reserve(threadCount);

    for(size_t i=0; i<threadCount; ++i) {
        pagedComm[i] = new PagedThreadComm<InsertInfo>(pageCount, pageSize);
    }

    auto mappabilityThread = [&ht, &mappability](PagedThreadComm<InsertInfo>*comm) {
        auto valueAcc = ht.template getValueAccessor<MappabilityAct>();

        while(comm->waitForRead()) {
            InsertInfo insInfo = comm->pop();
            Runtime::CanonizedKmer<DNA_Alph> kmer(insInfo.kmer);
            valueAcc.value(kmer, insInfo.act);

            for (size_t i=0; i<kmer.getK(); i++) {
                for (size_t j=0; j<kmer.alph.alphSize -1; ++j) {
                    kmer.inc(i);
                    valueAcc.value(kmer, insInfo.act);
                }
                kmer.inc(i); //increment once more to reset to former value
            }
        }
    };

    size_t kmerCount = 0;
    size_t mappabilityPos = 0;

    size_t currPagedComm = 0;
    size_t elemsInPage = 0;

    for (size_t i=0; i<threadCount; ++i) {
        threads.push_back(std::thread(mappabilityThread, pagedComm[i]));
    }

    while (likely(!kmers.eof())) {
#ifdef DEBUG_COMPILE
            if (mappabilityPos >= expectedKmerCount) {
                throw std::runtime_error("actual kmer count exceeds expectedKmerCount");
            }
#endif

        pagedComm[currPagedComm]->push({*kmers, MappabilityAct(mappability[mappabilityPos])});
        ++kmerCount;

        ++mappabilityPos;
        ++kmers;
        ++elemsInPage;
        if (elemsInPage == pageSize) {
            elemsInPage = 0;
            currPagedComm = (currPagedComm+1) % threadCount;
        }
    }

    for (auto *pagedC : pagedComm) {
        pagedC->setEOF();
    }

    for (auto &thread : threads) {
        thread.join();
    }

    for (auto *pagedC : pagedComm) {
        delete pagedC;
    }

    MappabilityStats stats; // += sumActor.stats()
    stats.timeElapsed = timer.getElapsedTime();
    stats.kmerCount = kmerCount;

    for (size_t i=0; i<expectedKmerCount; ++i) {
        if (mappability[i] == 1) {
            ++stats.uniqueMappable;
        }
    }
    delete[] mappability;

    return stats;
}

template<class HT, class KmerIterator, typename std::enable_if<is_instantiation_of<Runtime::Hashtable::Blocked, HT>::value, int>::type = 0>
static auto mappabilityLookup_parallel(HT &ht, KmerIterator &kmers, size_t threadCount, size_t pageCount, size_t pageSize, const size_t expectedKmerCount) {
    //we disable the internal parallelization of the blocked hashtable
    Runtime::Hashtable::BlockedHT_nonParallel<typename HT::KeyType, typename HT::ValueType, typename HT::SubHT, typename HT::HFType>nonParallelBlocked(ht);
    return HashtableActions::mappabilityLookup_parallel(nonParallelBlocked, kmers, threadCount, pageCount, pageSize, expectedKmerCount);
}

template<class HT, class KmerIterator, typename std::enable_if<is_instantiation_of<Runtime::Hashtable::Blocked, HT>::value, int>::type = 0>
static auto mappabilityLookup_direct(HT &ht, KmerIterator &kmers, const size_t expectedKmerCount) {
    //we disable the internal parallelization of the blocked hashtable
    Runtime::Hashtable::BlockedHT_nonParallel<typename HT::KeyType, typename HT::ValueType, typename HT::SubHT, typename HT::HFType> nonParallelBlocked(ht);
    return HashtableActions::mappabilityLookup_direct(nonParallelBlocked, kmers, expectedKmerCount);
}

template<class HT, class KmerIterator>
static auto mappabilityLookup(HT &ht, KmerIterator &kmers, size_t threadCount, size_t pageCount, size_t pageSize, const size_t expectedKmerCount) {
    if (threadCount == 0) {
        return HashtableActions::mappabilityLookup_accessor(ht, kmers, expectedKmerCount);
    }
    else {
        return HashtableActions::mappabilityLookup_parallel(ht, kmers, threadCount, pageCount, pageSize, expectedKmerCount);
    }
}


template<class HT, class KmerIterator>
static LookupStats lookup(const HT &ht, KmerIterator &kmers, OnFailure onFailure) {
    TakeTime timer;

    class LookupActor {
    protected:
        LookupStats m_stats;
        OnFailure onFailure;
        std::atomic<bool> &stop;

    public:
        LookupActor(OnFailure onFailure, std::atomic<bool> &stop)
            : onFailure(onFailure)
            , stop(stop)
        {}

        LookupActor(const LookupActor &other)
            : m_stats(other.m_stats)
            , onFailure(other.onFailure)
            , stop(other.stop)
        {}

        inline void act(const LookupResult &result) {
            if (result == LOOKUP_EXISTS) {
                ++m_stats.exists;
            }
            else if (unlikely(result == LOOKUP_NOT_EXISTS)) {
                ++m_stats.notExists;
            }
            else if (likely(result == LOOKUP_UNSURE)) {
                ++m_stats.unsure;
                if (onFailure != CONTINUE) {
                    stop = true;
                }
            }
        }

        const auto &stats() const {return m_stats;}
    };

    class ParallelLookupActor : public LookupActor {
        std::mutex mtx;

    public:
        using LookupActor::LookupActor;

        LookupActor detachParallel() {
            return LookupActor(*this);
        }

        void join(const LookupActor &threadInsertActor) {
            mtx.lock();
            LookupActor::m_stats += threadInsertActor.stats();
            mtx.unlock();
        }
    };

    std::atomic<bool> stop (false);
    ParallelLookupActor lookupActor(onFailure, stop);
    ht.initLookup(lookupActor);

    size_t kmerCount = 0;
    while (likely(!kmers.eof()) && !stop) {
        ++kmerCount;
        ht.lookup(*kmers, lookupActor);
        ++kmers;
    }

    ht.finishLookup(lookupActor);

    LookupStats stats = lookupActor.stats();
    stats.timeElapsed = timer.getElapsedTime();
    stats.kmerCount = kmerCount;
    return stats;
}

template<class HT, class KmerIterator, typename std::enable_if<!is_instantiation_of<Runtime::Hashtable::Blocked, HT>::value, int>::type = 0>
static auto lookup_parallel(const HT &ht, KmerIterator &kmers, size_t threadCount, size_t pageCount, size_t pageSize) {
    std::cout << "lookup_parallel using lookup accessors" << std::endl;

    TakeTime timer;

    class LookupAct {
        LookupStats *lookupStats;

    public:
        LookupAct(const LookupAct &other)
            : lookupStats(other.lookupStats) {}

        LookupAct(LookupStats *lookupStats)
            : lookupStats(lookupStats) {}

        FORCE_INLINE void act(typename HT::HT_LookupResult res) {
            ++lookupStats->kmerCount;
            if (res == LOOKUP_EXISTS) {
                ++lookupStats->exists;
            }
            else if (res == LOOKUP_NOT_EXISTS) {
                ++lookupStats->notExists;
            }
            else if (res == LOOKUP_UNSURE) {
                ++lookupStats->unsure;
            }
        }
    };

    using Kmer = decltype (*kmers);

    //each thread needs its own PagedThreadComm for exchange of kmers and actors.
    std::vector<PagedThreadComm<Kmer>*> pagedComm(threadCount, nullptr);
    std::vector<std::thread> threads;
    threads.reserve(threadCount);

    for(size_t i=0; i<threadCount; ++i) {
        pagedComm[i] = new PagedThreadComm<Kmer>(pageCount, pageSize);
    }

    std::mutex mtx;
    LookupStats stats;

    auto lookupThread = [&ht, &stats, &mtx](PagedThreadComm<Kmer>*comm) {
        LookupStats threadStats;
        {
            auto lookupAcc (ht.template getLookupAccessor<LookupAct>());
            LookupAct act(&threadStats);

            while(comm->waitForRead()) {
                lookupAcc.lookup(comm->pop(), act);
            }
        }

        mtx.lock();
        stats += threadStats;
        mtx.unlock();
    };

    size_t currPagedComm = 0;
    size_t elemsInPage = 0;

    for (size_t i=0; i<threadCount; ++i) {
        threads.push_back(std::thread(lookupThread, pagedComm[i]));
    }

    while (likely(!kmers.eof())) {
        pagedComm[currPagedComm]->push(*kmers);
        ++kmers;
        ++elemsInPage;

        if (elemsInPage == pageSize) {
            elemsInPage = 0;
            currPagedComm = (currPagedComm+1) % threadCount;
        }
    }

    for (auto *pagedC : pagedComm) {
        pagedC->setEOF();
    }

    for (auto &thread : threads) {
        thread.join();
    }

    for (auto *pagedC : pagedComm) {
        delete pagedC;
    }

    stats.timeElapsed = timer.getElapsedTime();
    return stats;
}

template<class HT, class KmerIterator, typename std::enable_if<is_instantiation_of<Runtime::Hashtable::Blocked, HT>::value, int>::type = 0>
static auto lookup_parallel(const HT &ht, KmerIterator &kmers, size_t threadCount, size_t pageCount, size_t pageSize) {
    using NonParallelBlocked = Runtime::Hashtable::BlockedHT_nonParallel<typename HT::KeyType, typename HT::ValueType, typename HT::SubHT, typename HT::HFType>;

    NonParallelBlocked nonParallelBlocked(ht);
    return HashtableActions::lookup_parallel<NonParallelBlocked, KmerIterator>(nonParallelBlocked, kmers, threadCount, pageCount, pageSize);
}


template<class HT>
class CountActor {
protected:
    InsertStats m_stats;
    OnFailure onFailure;
    std::atomic<bool> *stop;
    size_t counterBits;

public:
    CountActor(OnFailure onFailure, std::atomic<bool> *stop, size_t counterBits)
        : onFailure(onFailure)
        , stop(stop)
        , counterBits(counterBits)
    {}

    CountActor(const CountActor &other)
        : m_stats(other.m_stats)
        , onFailure(other.onFailure)
        , stop(other.stop)
        , counterBits(other.counterBits)
    {}

    template<class SubHT>
    inline void act(SubHT &ht, const typename SubHT::KeyType &key, typename SubHT::AccessSupport accSupp) {
        if (accSupp.res() == LOOKUP_EXISTS) {
            accSupp.set(std::min((size_t (1)<<counterBits) -1, accSupp.get() +1));
            ++m_stats.existed;
        }
        else {
            ht.insert(key, 0, *this);
        }
        ++m_stats.kmerCount;
    }

    const auto &stats() const {return m_stats;}

    inline void act(typename HT::HT_InsertResult insertResult) {
        switch (insertResult) {
        case APPEND_APPENDED:
            ++m_stats.inserted;
            break;
        case APPEND_EXISTS:
#ifdef DEBUG_COMPILE
            throw std::runtime_error("an existing value should have incremented the counter instead of reinserting");
#endif
            break;
        case APPEND_FULL:
            *stop = true;
            ++m_stats.notInserted;
            break;
        }
    }
};

template<class HT>
class ParallelCountActor : public CountActor<HT> {
    std::mutex mtx;

public:
    using CountActor<HT>::CountActor;

    CountActor<HT> detachParallel() {
        return CountActor(*this);
    }

    void join(const CountActor<HT> &threadInsertActor) {
        mtx.lock();
        CountActor<HT>::m_stats += threadInsertActor.stats();
        mtx.unlock();
    }
};

template<class HT, class KmerIterator>
static InsertStats count(HT &ht, KmerIterator &kmers, OnFailure onFailure, size_t counterBits) {
    TakeTime timer;

    std::atomic<bool> stop = false;
    ParallelCountActor<HT> countActor(onFailure, &stop, counterBits);

    ht.initValue(countActor);

    size_t kmerCount = 0;
    while (likely(!kmers.eof()) && !stop) {
        ++kmerCount;
        ht.value(*kmers, countActor);
        ++kmers;
    }

    ht.finishValue(countActor);

    InsertStats stats = countActor.stats();
    stats.kmerCount = kmerCount;
    stats.timeElapsed = timer.getElapsedTime();
    return stats;
}

template<class HT, class KmerIterator>
static InsertStats insert(HT &ht, KmerIterator &kmers, OnFailure onFailure) {
    TakeTime timer;

    class InsertActor {
    protected:
        InsertStats m_stats;
        OnFailure onFailure;
        bool &stop;

    public:
        InsertActor(OnFailure onFailure, bool &stop)
            : onFailure(onFailure)
            , stop(stop)
        {}

        InsertActor(const InsertActor &other)
            : m_stats(other.m_stats)
            , onFailure(other.onFailure)
            , stop(other.stop)
        {}

        inline void act(const typename HT::HT_InsertResult &result) {
            if (result.res == APPEND_FULL) {
                ++m_stats.notInserted;
                if (onFailure != CONTINUE) {
                    stop = true;
                }
            }
            else if (unlikely(result.res == APPEND_EXISTS)) {
                ++m_stats.existed;
            }
            else if (likely(result.res == APPEND_APPENDED)) {
                ++m_stats.inserted;
            }
        }

        const InsertStats &stats() const {return m_stats;}
    };

    class ParallelInsertActor : public InsertActor {
        std::mutex mtx;

    public:
        using InsertActor::InsertActor;


        InsertActor detachParallel() {
            return InsertActor(*this);
        }

        void join(const InsertActor &threadInsertActor) {
            mtx.lock();
            InsertActor::m_stats += threadInsertActor.stats();
            mtx.unlock();
        }
    };

    bool stop = false;
    ParallelInsertActor insertActor(onFailure, stop);

    ht.initInsert(insertActor);

    size_t kmerCount = 0;
    while (likely(!kmers.eof()) && !stop) {
        ++kmerCount;
        ht.insert(*kmers, insertActor);
        ++kmers;
    }

    ht.finishInsert(insertActor);

    InsertStats stats = insertActor.stats();
    stats.timeElapsed = timer.getElapsedTime();
    stats.kmerCount = kmerCount;
    return stats;
}
};
