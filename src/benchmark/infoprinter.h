#pragma once

#include <iostream>
#include <vector>

#include "tools.h"
#include "template/is_instantiation_of.h"

#include "runtime/hashtables/delimited_linear_probing.h"

#include "runtime/hashtables/blocked_ht.h"
#include "runtime/hashtables/hashtable_multilevel.h"
#include "runtime/hashtables/replacing.h"

#include "runtime/page_layouts/ht_page_container.h"
#include "runtime/page_layouts/lin_probing_container.h"
#include "runtime/hashtables/prefetching_ht.h"
#include "runtime/hashtables/actor_ht.h"

#include "benchmark/actionstatistics.h"

class InfoPrinter {
public:
	template<class T, typename std::enable_if<std::is_same<Runtime::LinProbContainer, T>::value, int>::type = 0>
	static void printContainerStaticData(const T &container) {
		std::cout << "#BEGIN container static data" << std::endl;
		std::cout << "type: " << container.getTypeStr() << std::endl;
		std::cout << "memory: " << printDots(container.memory()) << std::endl;
		std::cout << "capacity: " << printDots(container.elemCapacity()) << std::endl;
		std::cout << "hf position count: " << printDots(container.hashPositionCnt()) << std::endl;
		std::cout << "linear probing range: " << printDots(container.getLinProbRange()) << std::endl;
		std::cout << "bits per fingerprint: " << printDots(container.fingerPrintBitCount()) << std::endl;
		std::cout << "bits per entry: " << printDots(container.bitCntPerKey()) << std::endl;
		std::cout << "#END container static data" << std::endl;
	}

    template<class Container, class ...Args, typename std::enable_if<is_instantiation_of<::Runtime::HT_PageContainer, Container>::value, int>::type = 0>
	static void printContainerStaticData(const Container &container) {
		std::cout << "#BEGIN container static data" << std::endl;
		std::cout << "type: " << container.getTypeStr() << std::endl;
		std::cout << "memory: " << printDots(container.memory()) << std::endl;
		std::cout << "capacity: " << printDots(container.elemCapacity()) << std::endl;
		std::cout << "hf position count: " << printDots(container.hashPositionCnt()) << std::endl;
		std::cout << "words per page: " << printDots(container.wordsPerPage()) << std::endl;
		std::cout << "capacity per page: " << printDots(container.pageCapacity()) << std::endl;
		std::cout << "bits per fingerprint: " << printDots(container.fingerPrintBitCount()) << std::endl;
		std::cout << "bits per entry: " << printDots(container.bitCntPerKey()) << std::endl;
		std::cout << "#END container static data" << std::endl;
	}

	template<class HT, class ...Args, typename std::enable_if<is_instantiation_of<HashtableMultilevel, HT>::value, int>::type = 0>
	static void printStaticData(const HT &ht) {
		std::cout << std::endl;
		std::cout << "#BEGIN hashtable static data" << std::endl;
		std::cout << "hashtable: " << "multilevel" << std::endl;

		auto subHashtables = ht.getSubHashtables();
		std::cout << "part hashtable 1: " << double(std::get<0>(subHashtables).capacity()) / ht.capacity() << std::endl;
		std::cout << std::endl;

		std::cout << "#BEGIN Hashtable 1" << std::endl;
		InfoPrinter::printStaticData(std::get<0>(subHashtables));
		std::cout << "#END Hashtable 1" << std::endl;
		std::cout << std::endl;

		std::cout << "#BEGIN Hashtable 2" << std::endl;
		InfoPrinter::printStaticData(std::get<1>(subHashtables));
		std::cout << "#END Hashtable 2" << std::endl;
		std::cout << std::endl;

		std::cout << "#END hashtable static data" << std::endl;
	}

	template<class HT, typename std::enable_if<is_instantiation_of<Runtime::HashTable_Delimited_linear, HT>::value, int>::type = 0>
	static void printStaticData(const HT &ht) {
		std::cout << std::endl;
		std::cout << "#BEGIN hashtable static data" << std::endl;
		std::cout << "hashtable: " << "delimited linear probing" << std::endl;
        printContainerStaticData(ht.getDataContainer());
		std::cout << "#END hashtable static data" << std::endl;
	}

	template<class HT, typename std::enable_if<is_instantiation_of<Runtime::Hashtable::Blocked, HT>::value, int>::type = 0>
	static void printStaticData(const HT &ht) {
		std::cout << std::endl;
		std::cout << "#BEGIN hashtable static data" << std::endl;
		std::cout << "hashtable: " << "blocked" << std::endl;
        std::cout << "pagedComm_batchCount: " << printDots(ht.pagedComm_batchCount()) << std::endl;
        std::cout << "pagedComm_batchSize: " << printDots(ht.pagedComm_batchSize()) << std::endl;
		auto subHts = ht.getSubHTs();
		std::cout << "hashtableCount: " << printDots(subHts.size()) << std::endl;
		for (auto subHt : subHts) {
			printStaticData(*subHt);
		}
		std::cout << "#END hashtable static data" << std::endl;
	}

	template<class HT, typename std::enable_if<is_instantiation_of<Runtime::Hashtable::Replacing, HT>::value, int>::type = 0>
	static void printStaticData(const HT &ht) {
		std::cout << std::endl;
		std::cout << "#BEGIN hashtable static data" << std::endl;
		std::cout << "hashtable: " << "replacing" << std::endl;
		std::cout << "hfCount: " << printDots(ht.hfCount()) << std::endl;
		std::cout << "walkLength: " << printDots(ht.walkLength()) << std::endl;
		printContainerStaticData(ht.getDataContainer());

#ifdef MANUAL_RAND
        std::cout << "#BEGIN manual replacing distribution" << std::endl;
        auto replacingDist = ht.getReplacingDistribution();

        for (size_t i=0; i<replacingDist.size(); ++i) {
            std::cout << i << ": " << replacingDist[i] << std::endl;
        }
        std::cout << "#END manual replacing distribution" << std::endl;
#endif
		std::cout << "#END hashtable static data" << std::endl;
	}

    template<class HT, typename std::enable_if<is_instantiation_of<Actor_HT, HT>::value, int>::type = 0>
    static void printStaticData(const HT &ht) {
        printStaticData(static_cast<const typename HT::SubHT&>(ht));
    }

#ifdef PREFETCH
    template<class HT, typename std::enable_if<is_instantiation_of<Runtime::Hashtable::PrefetchAhead, HT>::value, int>::type = 0>
	static void printStaticData(const HT &ht) {
		std::cout << "#BEGIN hashtable static data" << std::endl;
		std::cout << "hashtable: " << "prefetching" << std::endl;
		std::cout << "prefetch ahead: " << ht.prefetchAhead() << std::endl;
		printStaticData(ht.baseHT());
		std::cout << "#END hashtable static data" << std::endl;
	}
#endif

	template<class HT, class ...Args, typename std::enable_if<is_instantiation_of<Runtime::Hashtable::Blocked, HT>::value, int>::type = 0>
	static void printInsertStats(const HT &ht) {
		std::cout << std::endl;
		std::cout << "#BEGIN insertion stats" << std::endl;

		std::cout << "hashtable: " << "blocked" << std::endl;
		std::cout << "inserted: " << printDots(ht.elemCount()) << std::endl;
		std::cout << "load: " << double(ht.elemCount()) / ht.capacity() *100 << '%' << std::endl;

		auto subHTs = ht.getSubHTs();
		for(size_t i=0; i<subHTs.size(); ++i) {
			std::cout << "#BEGIN Hashtable " << i << std::endl;
			InfoPrinter::printInsertStats(*subHTs[i]);
            std::cout << "#END Hashtable " << i << std::endl;
		}

		std::cout << "#END insertion stats" << std::endl;
	}

	template<class HT, class ...Args, typename std::enable_if<is_instantiation_of<HashtableMultilevel, HT>::value, int>::type = 0>
	static void printInsertStats(const HT &ht) {
		std::cout << std::endl;
		std::cout << "#BEGIN insertion stats" << std::endl;

		auto subHashtables = ht.getSubHashtables();
		std::cout << "hashtable: " << "multilevel" << std::endl;
		std::cout << "inserted: " << printDots(ht.elemCount()) << std::endl;
		std::cout << "load: " << double(ht.elemCount()) / ht.capacity() *100 << '%' << std::endl;
		std::cout << std::endl;

		std::cout << "#BEGIN Hashtable 1" << std::endl;
		InfoPrinter::printInsertStats(std::get<0>(subHashtables));
		std::cout << "#END Hashtable 1" << std::endl;
		std::cout << std::endl;

		std::cout << "#BEGIN Hashtable 2" << std::endl;
		InfoPrinter::printInsertStats(std::get<1>(subHashtables));
		std::cout << "#END Hashtable 2" << std::endl;
		std::cout << std::endl;

		std::cout << "#END insertion stats" << std::endl;
	}

	template<class HT, typename std::enable_if<is_instantiation_of<Runtime::HashTable_Delimited_linear, HT>::value, int>::type = 0>
	static void printInsertStats(const HT &ht) {
		std::cout << std::endl;
		std::cout << "#BEGIN insertion stats" << std::endl;

		std::cout << "inserted: " << printDots(ht.elemCount()) << std::endl;
		std::cout << "load: " << double(ht.elemCount()) / ht.capacity() *100 << '%' << std::endl;

		std::cout << "#END insertion stats" << std::endl;
	}


    template<class Container, typename std::enable_if<is_instantiation_of<Runtime::HT_PageContainer, Container>::value, int>::type = 0>
    static void printContainerInsertStats(const Container &container) {
        std::cout << "#BEGIN container insert stats" << std::endl;
        std::cout << "container type: " << "delimited linear probing" << std::endl;
        printHistogram(std::cout, "page fillrate", container.getPageFillrates());
        std::cout << "#END container insert stats" << std::endl;
    }

    static void printContainerInsertStats(const Runtime::LinProbContainer &) {
        std::cout << "#BEGIN container insert stats" << std::endl;
        std::cout << "container type: " << "delimited linear probing" << std::endl;
        std::cout << "#END container insert stats" << std::endl;
    }

	template<class HT, typename std::enable_if<is_instantiation_of<Runtime::Hashtable::Replacing, HT>::value, int>::type = 0>
	static void printInsertStats(const HT &ht) {
		std::cout << std::endl;
		std::cout << "#BEGIN insertion stats" << std::endl;

		std::cout << std::endl;

#ifndef RUN_LENGTH_STAT
        std::cerr << "run lengths were not tracked" << std::endl;
#else
		auto &runLengths = ht.getRunLengths();

		size_t lastNonNull=runLengths.size() -1;
		for (; lastNonNull!=0; lastNonNull--) {
			if (runLengths[lastNonNull] != 0) {
				break;
			}
		}

		std::vector<std::size_t> runLFront(runLengths.begin(), runLengths.begin() + lastNonNull + 1);

		printHistogram(std::cout, "run length", runLFront, 1, false);
		std::cout << std::endl;
#endif
		printHistogram(std::cout, "hf choice", ht.getHashFuncStats(), 1);
		std::cout << std::endl;

		std::cout << "elem count: " << printDots(ht.elemCount()) << std::endl;
		std::cout << "load: " << double(ht.elemCount()) / ht.capacity() *100 << '%' << std::endl;

        std::cout << std::endl;
        printContainerInsertStats(ht.getDataContainer());
        std::cout << std::endl;

		std::cout << "#END insertion stats" << std::endl;
	}

#ifdef PREFETCH
    template<class HT, typename std::enable_if<is_instantiation_of<Runtime::Hashtable::PrefetchAhead, HT>::value, int>::type = 0>
	static void printInsertStats(const HT &ht) {
		printInsertStats(ht.baseHT());
	}
#endif

    template<class HT, typename std::enable_if<is_instantiation_of<Actor_HT, HT>::value, int>::type = 0>
    static void printInsertStats(const HT &ht) {
        printInsertStats(static_cast<const typename HT::SubHT&>(ht));
    }

	template<class HT>
	static void printCountStats(const HT &ht) {
		std::cout << std::endl;
		std::cout << "#BEGIN count stats" << std::endl;
		printInsertStats(ht);
		std::cout << std::endl;
//        std::cout << "value counts currently not implemented" << std::endl;
        auto valueCounts = ht.getValueCounts();
//        std::sort(valueCounts.begin(), valueCounts.end());

//        std::map

        //convert the map to a complete vector
        std::vector<size_t> counts;
        for (auto it : valueCounts) {
            if (counts.size() <= it.first) {
                counts.resize(it.first +1, 0);
            }
            counts[it.first] = it.second;
        }

        printHistogram(std::cout, "value counts", counts, 1);
		std::cout << "#END count stats" << std::endl;
	}

	static void printActionStats(const ActionStats &stats) {
		std::cout << stats;
	}

	template<class ActStatType>
	static void printActionStats(const std::vector<ActStatType> &stats) {
		ActStatType accStats;
		for (size_t i=0; i<stats.size(); ++i) {
			std::cout << "#BEGIN statistics for last action for file " << i << std::endl;
			std::cout << stats[i];
			std::cout << "#END statistics for last action for file " << i << std::endl;
			accStats += stats[i];
			std::cout << std::endl;
		}

        std::cout << "#BEGIN accumulated " + stats.front().getName()  << std::endl;
		std::cout << accStats;
        std::cout << "#END accumulated " + stats.front().getName()  << std::endl;
		std::cout << std::endl;
	}
};
