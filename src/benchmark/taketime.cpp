#include "taketime.h"

#include <iostream>

unsigned long long TakeTime::getElapsedTime() const
{
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	return std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
}

LoggingTimer::LoggingTimer(const std::string &name)
	: name(name)
{
}

LoggingTimer::~LoggingTimer()
{
	TimerManagement::addTime(*this);
}


TimerManagement::TimesMap TimerManagement::times = TimerManagement::TimesMap();

void TimerManagement::addTime(const LoggingTimer &takeTime) {
	TimesMap::iterator it = times.find(takeTime.name);
	if (it != times.end()) {
		it->second += takeTime.getElapsedTime();
	}
	else {
		times.insert({takeTime.name, takeTime.getElapsedTime()});
	}
}

unsigned long long TimerManagement::getTime(const std::string &name)
{
	return times.at(name);
}
