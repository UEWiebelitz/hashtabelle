#pragma once

#include <string>
#include <iostream>

struct ActionStats {
	std::size_t timeElapsed = 0;
	std::size_t kmerCount = 0;

	ActionStats operator +(const ActionStats &other) const;
	ActionStats & operator +=(const ActionStats &other);
	virtual std::ostream & printToStream(std::ostream &stream) const;
	virtual std::string getName() const;
};

struct InsertStats : ActionStats {
	std::size_t inserted = 0;
	std::size_t notInserted = 0;
	std::size_t existed = 0;

	inline operator bool() const {
		return notInserted == 0;
	}

	InsertStats operator +(const InsertStats &other) const;
	InsertStats & operator +=(const InsertStats &other);
	virtual std::ostream & printToStream(std::ostream &stream) const;
	virtual std::string getName() const;
};

struct LookupStats : ActionStats {
	std::size_t exists = 0;
	std::size_t notExists = 0;
	std::size_t unsure = 0;

	operator bool() const {
		return unsure == 0;
	}

	LookupStats operator +(const LookupStats &other) const;
	LookupStats & operator +=(const LookupStats &other);
	virtual std::ostream & printToStream(std::ostream &stream) const;
	virtual std::string getName() const;
};

struct MappabilityStats : ActionStats {
    std::size_t uniqueMappable = 0;

    MappabilityStats operator +(const MappabilityStats &other) const;
    MappabilityStats & operator +=(const MappabilityStats &other);

    virtual std::ostream & printToStream(std::ostream &stream) const;
    virtual std::string getName() const;
};

std::ostream &operator <<(std::ostream &stream, const ActionStats &stats);
