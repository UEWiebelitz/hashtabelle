#pragma once

#ifdef ENFORCE_INLINE
#define FORCE_INLINE [[gnu::always_inline]]
//#define FORCE_INLINE inline
#else
#define FORCE_INLINE inline
#endif
