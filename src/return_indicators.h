#ifndef RETURN_INDICATORS_H
#define RETURN_INDICATORS_H

#include <iostream>

enum AppendResult{APPEND_EXISTS, APPEND_FULL, APPEND_APPENDED};

/**
 * @brief The LookupResult enum provides indicators whether a value exists (EXISTS), definitely not exists (NOT_EXISTS) or could exist on another level (UNSURE).
 */
enum LookupResult{LOOKUP_EXISTS, LOOKUP_NOT_EXISTS, LOOKUP_UNSURE};

template<class Key, class Value>
struct InsertResult {
	AppendResult res;
	Key key;
	Value val;

	inline operator AppendResult() {return res;}
//	inline operator Key() {return key;}
//	inline operator Value() {return val;}
};

inline std::ostream& operator<<(std::ostream & os, const AppendResult & res) {
	switch (res) {
	case APPEND_EXISTS:
		os << "EXISTS";
		break;
	case APPEND_FULL:
		os << "FULL";
		break;
	case APPEND_APPENDED:
		os << "APPEND";
		break;
	}

	return os;
}

inline std::ostream& operator<<(std::ostream & os, const LookupResult & res) {
	switch (res) {
	case LOOKUP_EXISTS:
		os << "EXISTS";
		break;
	case LOOKUP_NOT_EXISTS:
		os << "NOT_EXISTS";
		break;
	case LOOKUP_UNSURE:
		os << "UNSURE";
		break;
	}

	return os;
}

#endif // RETURN_INDICATORS_H
