#include <iostream>
#include <random>
#include <args.hxx>
#include <cstdlib>
#include <type_traits>
#include <memory>
#include <thread>

#include "commandline_config.h"
#include "benchmark/ht_instantiations.h"

template<class ValT, class ArgT>
bool overwrite(ValT &val, ArgT &arg) {
	if (arg.Matched()) {
		val = arg.Get();
		return true;
	}
//    val = arg.Get();
	return false;
}

int main(int argc, char **argv)
{
	std::random_device randomDevice;

	args::ArgumentParser parser("This is a kmer tool to store kmers in a hashtable.", "Master thesis project of Uriel Elias Wiebelitz");
	args::HelpFlag help(parser, "help", "display this help menu", {'h', "help"});
	args::CompletionFlag completion(parser, {"complete"});

	args::ValueFlag<size_t> k(parser, "kmer", "number of characters in a k-mer", {'k'}, 25);
	args::ValueFlag<size_t> counterBits(parser, "counter bits", "bits for a counter for each element", {"counter"}, 4);
//	args::Flag parallel(parser, "parallel", "perform the action 'lookup' in parallel", {'p'}, true);
    args::ValueFlag<size_t> parallelChunkCount(parser, "parallel cuck size", "number of thread communication pages", {"pcc"}, 10000);
    args::ValueFlag<size_t> parallelChunkSize(parser, "parallel cuck count", "size of each thread communication page", {"pcs"}, 20);
	args::ValueFlag<size_t> threadCount(parser, "thread count", "", {'t'}, std::thread::hardware_concurrency());

    args::ValueFlag<size_t> expectedKmerCount(parser, "expected kmer count (upper limit) for mappability", "max kmer count", {"kmer_count"});

	args::ValueFlagList<size_t> capacities(parser, "capacity", "capacity of the hashtable", {'c', "cap"});
    args::ValueFlag<unsigned int> randSeed(parser, "seed", "random seed for choice of replacement", {'r', "rand"}, randomDevice());
	args::ValueFlagList<size_t> walkLengths(parser, "walk lengths", "maximum number of replacements before giving up insertion", {"wL", "walkLength"}, {500});
	args::ValueFlag<size_t> prefetchAhead(parser, "prefetch ahead", "number of elements to prefetch before actual action", {"prefetch"}, 10);

	args::MapFlagList<std::string, HTType> htChoices(parser, "hashtabletype", "action when insertion fails (paged|linear|multilevel)", {"ht"}, htTypeMap, {PAGED});
//	args::MapFlag<std::string, HT1Type> ht1Choice(parser, "ht1_type", "action when insertion fails (replacing|linear)", {"ht1_type"}, ht1TypeMap, HT1_LINEAR);

	args::ValueFlagList<size_t> wordsPerPage_linProbingRange(parser, "words per page", "size of a page in machine words", {"wpp"});
	args::ValueFlagList<size_t> hfMultiplierCounts(parser, "hashfunctions for hts", "", {"hfCount"});
	args::ValueFlagList<size_t> hfMultipliers(parser, "hashfunction multipliers", "multipliers for the hashfunctions", {'m', "hfMult"});

	//	args::Group group(parser, "This group:", args::Group::Validators::AtLeastOne);
	args::Group sources(parser, "Sources", args::Group::Validators::AtLeastOne);
	args::ValueFlagList<std::string> fileList(sources, "files", "list of fasta files", {'f', "files"});
	args::ValueFlag<size_t> fileBufferSize(parser, "file buffer size", "", {"fb"}, 1024*1024);

    args::MapPositionalList<std::string, Actions> actions(parser, "actions", "actions to perform on data (nothing | insert | lookup | count | dump | mappability)", actionsMap, {INSERT, LOOKUP});
	args::MapFlagList<std::string, OnFailure> actOnFailure(parser, "failureAct", "action when insertion fails (stop|skip|continue)", {"failure"}, onFailureMap, {STOP});
	args::ValueFlag<std::string> dumpFile(parser, "dump ouput file", "", {'o'});

    args::ValueFlag<size_t> blockCount(parser, "count of sub hashtables", "", {'b'}, 7);
    args::ValueFlag<size_t> blockBatchCount(parser, "batch block count", "number of batch blocks in blocked hashtable", {"bbc"}, 20);
    args::ValueFlag<size_t> blockBatchSize(parser, "batch block size", "size of each batch block in blocked hashtable", {"bbs"}, 10000);

    args::ValueFlagList<size_t> replacingDist(parser, "replacing distribution", "interval size of the replacing distribution", {'d'});

	try {
		parser.ParseCLI(argc, argv);
	}
	catch (const args::Completion& e) {
		std::cout << e.what();
		return 0;
	}
	catch (const args::Help&) {
		std::cout << parser;
		return 0;
	}
	catch (const args::ValidationError &e) {
		std::cerr << e.what() << std::endl << std::endl;
		std::cerr << parser;
		return 0;
	}
	catch (const args::ParseError &e) {
		std::cerr << e.what() << std::endl;
		std::cerr << parser;
		return 0;
	}

	std::cout << "compilation mode: ";
#ifdef DEBUG_COMPILE
	std::cout << "debug" << std::endl;
#else
	std::cout << "release" << std::endl;
#endif

	std::cout << "prefetching compilation: ";
#ifdef PREFETCH
	std::cout << "enabled" << std::endl;
#if !defined(PREFETCH_FIRST_PAGE) && !defined (PREFETCH_ALL_PAGE) && !defined(PREFETCH_FIRST_PAGE_NO_CACHE)
#error To enable prefetching at least one prefetching method has to be enabled!
#endif

#else
	std::cout << "disabled" << std::endl;
#endif

	std::cout << "prefetching mode: ";
#ifdef PREFETCH_FIRST_PAGE
#if !defined(PREFETCH)
#error to enable PREFETCH_FIRST_PAGE prefetching has to be enabled at all by PREFETCH
#endif
	std::cout << "PREFETCH_FIRST_PAGE" << std::endl;
#endif
#ifdef PREFETCH_ALL_PAGE
#if !defined(PREFETCH)
#error to enable PREFETCH_ALL_PAGE prefetching has to be enabled at all by PREFETCH
#endif
	std::cout << "PREFETCH_ALL_PAGE" << std::endl;
#endif
#ifdef PREFETCH_FIRST_PAGE_NO_CACHE
#if !defined(PREFETCH)
#error to enable PREFETCH_FIRST_PAGE_NO_CACHE prefetching has to be enabled at all by PREFETCH
#endif
#if !defined(PREFETCH_FIRST_PAGE)
#error to enable PREFETCH_FIRST_PAGE_NEXT_PAGE you also have to enable PREFETCH_FIRST_PAGE
#endif
	std::cout << "PREFETCH_FIRST_PAGE_NO_CACHE" << std::endl;
#endif
#ifdef PREFETCH_FIRST_PAGE_NEXT_PAGE
#if !defined(PREFETCH)
#error to enable PREFETCH_FIRST_PAGE_NEXT_PAGE prefetching has to be enabled at all by PREFETCH
#endif
#if !defined(PREFETCH_FIRST_PAGE)
#error to enable PREFETCH_FIRST_PAGE_NEXT_PAGE you also have to enable PREFETCH_FIRST_PAGE
#endif
	std::cout << "PREFETCH_FIRST_PAGE_NEXT_PAGE" << std::endl;
#endif
#ifdef PREFETCH_NEXT_CACHE_LINE
    std::cout << "PREFETCH_NEXT_CACHE_LINE" << std::endl;
#endif

    std::cout << "linear probing compilation mode: ";
#ifdef LIN_PROB_MODULO
    std::cout << "modulo";
#ifdef LIN_PROB_IF_DIFF
         #error linear probing cannot be compiled with multiple modes at the same time: LIN_PROB_MODULO and LIN_PROB_IF_DIFF are set.
#endif
#elif defined(LIN_PROB_IF_DIFF)
    std::cout << "if/else";
#else
    std::cout << "oversize";
#endif
    std::cout << std::endl;

#ifndef CACHE_UNALIGNED
    std::cout << "page allocation mode: " << "aligned" << std::endl;
#else
    std::cout << "page allocation mode: " << "unaligned" << std::endl;
#endif

//	if (htChoices.Get().size() != capacities.Get().size()) {
//		std::cerr << "Hashtable types should match " << std::endl;
//	}

	CommandLine_Config config;
	overwrite(config.randomSeed, randSeed);
	overwrite(config.k, k);
	overwrite(config.counterBits, counterBits);
	overwrite(config.prefetchAhead, prefetchAhead);
//	overwrite(config.parallel, parallel);
    overwrite(config.expectedKmerCount, expectedKmerCount);
    overwrite(config.parallelChunkCount, parallelChunkCount);
	overwrite(config.parallelChunkSize, parallelChunkSize);
	overwrite(config.threadCount, threadCount);
	overwrite(config.files, fileList);
	overwrite(config.fileBufferSize, fileBufferSize);
	overwrite(config.dumpFile, dumpFile);
	overwrite(config.actions, actions);
	overwrite(config.actOnFailure, actOnFailure);

    std::cout << "manual replacing distribution: ";
#ifdef MANUAL_RAND
    std::cout << "true" << std::endl;

    if (!replacingDist.Matched()) {
        std::cerr << "you have to give the replacing distribution intervals because the binary was compiled with MANUAL_RAND" << std::endl;
        return 1;
    }
    config.replacingDist = replacingDist.Get();
#else
    std::cout << "false" << std::endl;
    if (replacingDist.Matched()) {
       std::cerr << "replacing distribution  can only get interpreted if compiled with define MANUAL_RAND" << std::endl;
    }
#endif

    if (std::find(config.actions.begin(), config.actions.end(), MAPPABILITY) != config.actions.end() || std::find(config.actions.begin(), config.actions.end(), MAPPABILITY) != config.actions.end()) {
        //mappability check
        if (config.expectedKmerCount == 0) {
            std::cerr << "using mappability without setting expected kmer count is not possible!" << std::endl;
            return 1;
        }
    }

	if (htChoices.Get().front() == MULTI_LEVEL)
	{
		if (htChoices.Get().size() < 2) {
			std::cerr << "a multilevel hashtable has to have at least one sub hash table" << std::endl;
			return 1;
		}

		HTConfig_Multilevel htCfgMultilevel;
		std::vector<HTConfig_Base*> subHtCnf;

		size_t hfMultPos = 0;
		for (size_t htCount = 1; htCount < htChoices.Get().size(); ++htCount) {
			auto type = htChoices.Get()[htCount];
			HTConfig htConf;
			htConf.htType(type);

			if (capacities.Get().size() <= htCount -1) {
				std::cerr << "you have to give the capacity for each hash table but multilevel hashtables" << std::endl;
                return 1;
			}
			htConf.capacity(capacities.Get()[htCount -1]);

			auto wls = walkLengths.Get();
			if (htCount == 1 && !walkLengths.Matched()) {
				htConf.walkLength(1);
			}
			else {
				if (wls.size() > htCount) {
					htConf.walkLength(wls[htCount -1]);
				}
				else {
					htConf.walkLength(wls.back());
				}
			}

			if (wordsPerPage_linProbingRange.Get().size() > htCount -1) {
				htConf.wordsPerPage_linProbingRange(wordsPerPage_linProbingRange.Get()[htCount -1]);
			}

			std::vector<size_t> hfMults;
			std::size_t hfCount;
			if (htCount == 1) {
				hfCount = 1;
			}
			else {
				hfCount  = 3;
			}

			if (hfMultiplierCounts.Get().size() <= htCount -1) {
				std::cerr << "number of hash functions not given - setting " << hfCount << " as default" << std::endl;
			}
			else {
				hfCount = hfMultiplierCounts.Get()[htCount -1];
			}

			for (size_t i=0; i<hfCount; ++i) {
				if (hfMultipliers.Get().size() <= hfMultPos + i) {
					hfMults.push_back(randomDevice() | 1);
				}
				else {
					hfMults.push_back(hfMultipliers.Get()[hfMultPos + i]);
				}
			}
			htConf.hfMultipliers(hfMults);

			subHtCnf.push_back(new HTConfig(htConf));
		}
		htCfgMultilevel.subHtConfigs(subHtCnf);
		config.htConfig = new HTConfig_Multilevel(htCfgMultilevel);
	}
	else { //BLOCKED or LINEAR or PAGED

        HTConfig *htCfg = nullptr;
		if (htChoices.Get().front() == BLOCKED) {
            htCfg = new HTConfig_Blocked();
            size_t subBlockCnt = blockCount.Get();
            auto blockedHtCfg = static_cast<HTConfig_Blocked*>(htCfg);
            if (subBlockCnt % 2 == 0) {
                std::cerr << "count of subblocks is even. This might lead to bad hash characteristics." << std::endl;
            }
            blockedHtCfg->subHtCount(subBlockCnt);
            blockedHtCfg->batchCount(blockBatchCount.Get());
            blockedHtCfg->batchSize(blockBatchSize.Get());

			if (htChoices.Get().size() != 2) {
				std::cerr << "for blocked hash tables you have to give a sub hashtable type!" << std::endl;
                delete htCfg;
				return 1;
			}
            static_cast<HTConfig_Blocked*>(htCfg)->subHtType(htChoices.Get()[1]);
            static_cast<HTConfig_Blocked*>(htCfg)->subHtCount(subBlockCnt);
		}
		else {
            htCfg = new HTConfig();
			if (htChoices.Get().size() != 1) {
				std::cerr << "only multilevel or blocked hashtables can combine multiple hash table types!" << std::endl;
				return 1;
			}
            htCfg->htType(htChoices.Get()[0]);
		}

		if (capacities.Get().size() <= 0) {
			std::cerr << "you have to give the capacity for each hash table but multilevel hashtables" << std::endl;
            return 1;
		}
        htCfg->capacity(capacities.Get()[0]);

		if (walkLengths.Get().size() > 0) {
            htCfg->walkLength(walkLengths.Get()[0]);
		}

		if (wordsPerPage_linProbingRange.Get().size() > 0) {
            htCfg->wordsPerPage_linProbingRange(wordsPerPage_linProbingRange.Get()[0]);
		}

		std::vector<size_t> hfMults;
		std::size_t hfCount = 3;
		if (hfMultiplierCounts.Get().size() <= 0) {
			std::cerr << "number of hash functions not given - setting " << hfCount << " as default" << std::endl;
		}
		else {
			hfCount = hfMultiplierCounts.Get()[0];
		}

		for (size_t i=0; i<hfCount; ++i) {
            if (hfMultipliers.Matched()) {
                if (i < hfMultipliers.Get().size()) {
                    hfMults.push_back(hfMultipliers.Get()[i]);
                    continue;
                }
                std::cerr << "hash multipliers were given, but not enough for all requested hash functions" << std::endl;

                //we just kill it on failure to ensure correct usage :-D
                throw std::invalid_argument("hash multipliers were given, but not enough for all requested hash functions");
            }
			if (hfMultipliers.Get().size() <= i) {
				hfMults.push_back(randomDevice() | 1);
            }
		}
        htCfg->hfMultipliers(hfMults);
        config.htConfig = htCfg;
	}

//	typedef Runtime::Kmer<DNA_Alph> KeyT;
//	typedef Runtime::HF_Fingerprint<Runtime::Kmer<DNA_Alph>> FpHf;
//	typedef Runtime::Hashtable::Replacing<Runtime::Kmer<DNA_Alph>, EmptyDummy, Runtime::PageAccess_Packed_Null, FpHf> RuntimeHT;

	std::srand(randSeed.Get());
	std::cout << "randomSeed: " << config.randomSeed << std::endl;
	std::cout << "thread count: " << config.threadCount << std::endl;
    std::cout << "parallel chunk count: " << config.parallelChunkCount << std::endl;
	std::cout << "parallel chunk size: " << config.parallelChunkSize << std::endl;

	for (size_t i=0; i<hfMultipliers.Get().size(); ++i) {
        std::cout << "hashfunction " << i << " multiplier: " << hfMultipliers.Get()[i] << std::endl;
	}

	actonOnHT(config);

	return 0;
}
