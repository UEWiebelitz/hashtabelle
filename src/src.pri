CONFIG += console c++17

QMAKE_CXXFLAGS += -fconstexpr-depth=24000
QMAKE_CXXFLAGS += -ftemplate-depth=102400
#QMAKE_CXXFLAGS += -S -mllvm --x86-asm-syntax=intel
#QMAKE_CXXFLAGS_RELEASE += -opt-bisect-limit=10
QMAKE_CXXFLAGS_RELEASE += -opt-bisect-limit=10240
QMAKE_CXXFLAGS_DEBUG -= -O
QMAKE_CXXFLAGS_DEBUG -= -O1
QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_DEBUG -= -O3
QMAKE_CXXFLAGS_DEBUG += -O0

QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE -= -O3
QMAKE_CXXFLAGS_RELEASE -= -O0

QMAKE_CXXFLAGS_RELEASE += -O3



DEFINES += RUN_LENGTH_STAT
DEFINES += HASH_FUNC_STAT
DEFINES += COUNT_ELEMS
DEFINES += TRACE_HASH_ACCESS

SOURCES += \
    $$PWD/benchmark/taketime.cpp \
    $$PWD/benchmark/csv.cpp

HEADERS += \
    $$PWD/template/hashfunctions/hf_fingerprint.h \
    $$PWD/template/hashfunctions/hf_simple.h \
    $$PWD/template/hashfunctions/storeinfo.h \
    $$PWD/template/hashfunctions/hf_std_simple.h \
    $$PWD/template/hashfunctions/hf_swap_bytes.h \
    $$PWD/template/hashfunctions/hf_mult_add.h \
    $$PWD/runtime/hashfunctions/mult_add.h \
    $$PWD/runtime/hashfunctions/fingerprint.h \
    $$PWD/hybrid/hashfunctions/fingerprint.h \
    $$PWD/return_indicators.h \
    $$PWD/benchmark/taketime.h \
    $$PWD/benchmark/csv.h \
    $$PWD/benchmark/hashtable_bench.h \
    $$PWD/counters/counter.h \
    $$PWD/template/bitshifting/wordpacking.h \
    $$PWD/template/bitshifting/wordpacking_counting.h \
    $$PWD/template/bitshifting/wordcontainer.h \
    $$PWD/runtime/bitshifting/wordpacking.h \
    $$PWD/runtime/bitshifting/wordpacking_counting.h \
    $$PWD/runtime/bitshifting/combinedwords.h \
    $$PWD/runtime/bitshifting/wordcontainer.h \
    $$PWD/hybrid/bitshifting/combinedwords.h \
    $$PWD/hybrid/page_layouts/page_packed_null.h \
    $$PWD/hybrid/bitshifting/wordpacking.h \
    $$PWD/template/hashtables/hashtable.h \
    $$PWD/template/hashtables/hashtable_replacing.h \
    $$PWD/template/hashtables/hashtable_multilevel.h \
    $$PWD/runtime/hashtables/hashtable.h \
    $$PWD/runtime/hashtables/replacing.h \
    $$PWD/runtime/hashtables/multilevel.h \
    $$PWD/hybrid/hashtables/replacing.h \
    $$PWD/template/counters/bitcounter.h \
    $$PWD/template/page_layouts/access_support.h \
    $$PWD/template/page_layouts/page_packed_counting.h \
    $$PWD/template/page_layouts/page_packed_null.h \
    $$PWD/template/page_layouts/page_bytewise_counting.h \
    $$PWD/template/page_layouts/page_bytewise_null.h \
    $$PWD/runtime/page_layouts/access_support.h \
    $$PWD/runtime/page_layouts/packed_null.h \
    $$PWD/template/counters/countbits.h \
    $$PWD/template/get_nth_type.h \
    $$PWD/dna_seq/kmer.h \
    $$PWD/dna_seq/dna_alph.h \
    $$PWD/dna_seq/kmer_reader.h \
    $$PWD/dna_seq/fileiterator.h \
    $$PWD/runtime/dna_seq/kmer.h \
    $$PWD/runtime/dna_seq/dna_alph.h \
    $$PWD/benchmark/tools.h \
    $$PWD/template/template_magic/ht_replacing_numbers.h \
    $$PWD/template/counters/emptydummy.h \
    $$PWD/template/template_magic/actors.h \
    $$PWD/template/template_magic/list_iterators.h \
    $$PWD/template/template_magic/template_arguments.h \
    $$PWD/template/template_magic/tmpl_lists_recursive.h \
    $$PWD/template/template_magic/instantiator_elem_count.h \
    $$PWD/template/template_magic/instantiator.h \
    $$PWD/template/bitshifting/combinedwords.h \
    $$PWD/hybrid/hashtables/interface.h \
    $$PWD/hybrid/bitshifting/combinedword_plus_one.h \
    $$PWD/benchmark/infoprinter.h \
    $$PWD/benchmark/fillhashtable.h \
    $$PWD/runtime/dna_seq/canonized_kmer.h \
    $$PWD/runtime/dna_seq

INCLUDEPATH += $$PWD
