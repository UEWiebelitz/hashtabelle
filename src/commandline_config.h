#ifndef COMMANDLINE_CONFIG_H
#define COMMANDLINE_CONFIG_H

#include <type_traits>

#include <unordered_map>
#include <vector>
#include <random>

enum Actions {NOTHING, INSERT, LOOKUP, COUNT, DUMP, MAPPABILITY_DIRECT, MAPPABILITY};

static std::unordered_map<std::string, Actions> actionsMap {
	{"nothing", NOTHING},
	{"insert", INSERT},
	{"lookup", LOOKUP},
	{"count", COUNT},
	{"dump", DUMP},
    {"mappability_direct", MAPPABILITY_DIRECT},
    {"mappability", MAPPABILITY},
};

enum HTType {PAGED, LINEAR, MULTI_LEVEL, BLOCKED};

static std::unordered_map<std::string, HTType> htTypeMap {
	{"paged", PAGED},
	{"linear", LINEAR},
	{"multilevel", MULTI_LEVEL},
	{"blocked", BLOCKED}
};

enum OnFailure {SKIP, STOP, CONTINUE};

static std::unordered_map<std::string, OnFailure> onFailureMap {
	{"skip", SKIP},
	{"stop", STOP},
	{"continue", CONTINUE}
};


class HTConfig_Base {
public:
	virtual HTType htType() const = 0;
	virtual ~HTConfig_Base();
};

class HTConfig_Multilevel : public HTConfig_Base {
	std::vector<HTConfig_Base*> m_subHtConfigs;

public:
	HTType htType() const {return MULTI_LEVEL;}

	virtual std::vector<HTConfig_Base*> subHtConfigs() const {return m_subHtConfigs;}
	virtual void subHtConfigs(const std::vector<HTConfig_Base*> &subHtConfigs) {m_subHtConfigs = subHtConfigs;}
};

class HTConfig : public HTConfig_Base {
	HTType m_htType;
	std::size_t m_capacity;
	std::size_t m_walkLength = 500;
	std::size_t m_wordsPerPage_linProbingRange = 8;
	std::vector<std::size_t> m_hfMultipliers;

public:
	virtual HTType htType() const {return m_htType;}
	virtual std::size_t capacity() const {return m_capacity;}
	virtual std::size_t wordsPerPage_linProbingRange() const {return m_wordsPerPage_linProbingRange;}
	virtual std::size_t walkLength() const {return m_walkLength;}
	virtual std::vector<std::size_t> hfMultipliers() const {return m_hfMultipliers;}

	virtual void htType(HTType htType) {m_htType = htType;}
	virtual void capacity(std::size_t capacity) {m_capacity = capacity;}
	virtual void wordsPerPage_linProbingRange(std::size_t wordsPerPage_linProbingRange) {m_wordsPerPage_linProbingRange = wordsPerPage_linProbingRange;}
	virtual void walkLength(std::size_t walkLength) {m_walkLength = walkLength;}
	virtual void hfMultipliers(const std::vector<std::size_t> &hfMultipliers) {m_hfMultipliers = hfMultipliers;}
};

class HTConfig_Blocked : public HTConfig {
	size_t m_subHtCount = 8;
	HTType m_subHtType;
	size_t m_batchCount = 10;
	size_t m_batchSize = 10000;
public:
	HTType htType() const {return BLOCKED;}
	virtual void subHtType(HTType subHtType) {m_subHtType = subHtType;}
	virtual HTType subHtType() const {return m_subHtType;}
	virtual void subHtCount(size_t subHtCount) {m_subHtCount = subHtCount;}
	virtual size_t subHTCount() const {return m_subHtCount;}
	virtual void batchCount(size_t batchCount) {m_batchCount = batchCount;}
	virtual size_t batchCount() const {return m_batchCount;}
	virtual void batchSize(size_t batchSize) {m_batchSize = batchSize;}
	virtual size_t batchSize() const {return m_batchSize;}
};

struct CommandLine_Config {
	std::size_t randomSeed;
    std::size_t k = 25;
    std::size_t counterBits = 0;
#ifdef PREFETCH
    std::size_t prefetchAhead = 10;
#else
    std::size_t prefetchAhead = 0;
#endif
    std::size_t expectedKmerCount = 0;

//	bool parallel;
    std::size_t parallelChunkSize = 10000;
    std::size_t parallelChunkCount = 20;
	std::size_t threadCount;

	std::vector<std::string> files;
    std::size_t fileBufferSize = 1024*1024;

	std::string dumpFile;

	std::vector<Actions> actions;
	std::vector<OnFailure> actOnFailure;

	HTConfig_Base* htConfig;

#ifdef MANUAL_RAND
    std::vector<std::size_t> replacingDist;
#endif

	virtual ~CommandLine_Config();
};

HTConfig_Base::~HTConfig_Base() {}

CommandLine_Config::~CommandLine_Config() {
	delete htConfig;
}

#endif // COMMANDLINE_CONFIG_H
