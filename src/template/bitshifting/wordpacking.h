#ifndef WORDPACKING_H
#define WORDPACKING_H

#include <cstring>
#include <algorithm>
#include <cassert>

template <class WordType, int wordCount, class PackType>
class WordPacking {
	static_assert(sizeof(WordType) * 8 >= PackType::bitCount(), "WordType is too small for PackType");

protected:
	WordType arr[wordCount];

public:
	inline WordPacking() {
		std::memset(arr, 0, sizeof (WordType) * wordCount);
	}

	[[gnu::always_inline]]inline PackType operator[](unsigned int pos) const {
#ifdef DEBUG_TESTS
		assert(pos < elemCapacity() && "pos is out of range");
#endif

		WordType value = arr[getStartingWord(pos)] >> getStartingPosInWord(pos);
		value |= arr[getEndWord(pos)] << (PackType::bitCount() - overflowBitCount(pos));
		value &= mask();
		return unconvert(value);
	}

	[[gnu::always_inline]] inline void reset(unsigned int pos) {
#ifdef DEBUG_TESTS
		assert(pos < elemCapacity() && "pos is out of range");
#endif

		arr[getStartingWord(pos)] &= ~(mask() << getStartingPosInWord(pos));
		arr[getEndWord(pos)] &= ~(mask() >> (PackType::bitCount() - overflowBitCount(pos)));
	}

	[[gnu::always_inline]]inline void resetAndStore(unsigned int pos, const PackType &value) {
#ifdef DEBUG_TESTS
		assert(pos < elemCapacity() && "pos is out of range");
		assert((convert(value) & mask()) == convert(value) && "value is not correclty masked. It consumes more bits than allowed.");
#endif

		reset(pos);
		store(pos, value);
	}

	[[gnu::always_inline]]inline WordType convert(const PackType &value) const {
		WordType word = (*reinterpret_cast<const WordType*>(&value));
		static_assert (sizeof (WordType) >= sizeof (PackType), "PackType has to be smaller than WordType");
//		const WordType mask = ((WordType(1) << (sizeof (PackType) * 8)) -1);
		return word & mask();
//		return word >> ((sizeof (WordType) - sizeof (PackType)) * 8);
	}

	[[gnu::always_inline]]inline PackType unconvert(const WordType &value) const {
//		WordType word = value << ((sizeof (WordType) - sizeof (PackType)) * 8);
//		PackType pack = (*reinterpret_cast<const PackType*>(&word));
		PackType pack = (*reinterpret_cast<const PackType*>(&value));
		return pack;
	}

	[[gnu::always_inline]]inline void store(unsigned int pos, const PackType &value) {
#ifdef DEBUG_TESTS
		assert(pos < elemCapacity() && "pos is out of range");
		assert((convert(value) & mask()) == convert(value)&& "value is not correclty masked. It consumes more bits than allowed.");
#endif

		arr[getStartingWord(pos)] |= (convert(value) << getStartingPosInWord(pos));
		arr[getEndWord(pos)] |= (convert(value) >> (PackType::bitCount() - overflowBitCount(pos)));
	}


protected:
	[[gnu::always_inline]]static inline constexpr unsigned int bitsPerWord() {
		return (sizeof (WordType) * 8);
	}

	[[gnu::always_inline]]inline unsigned int getStartingWord(unsigned int pos) const {
		return (PackType::bitCount() * pos) / bitsPerWord();
	}

	[[gnu::always_inline]]inline unsigned int getEndWord(unsigned int pos) const {
		return (PackType::bitCount() * (pos +1) -1) / bitsPerWord();
	}

	[[gnu::always_inline]]inline unsigned int getStartingPosInWord(unsigned int pos) const {
		return (PackType::bitCount() * pos) % bitsPerWord();
	}

	[[gnu::always_inline]]inline unsigned int overflowBitCount(unsigned int pos) const {
		const int bits = int (PackType::bitCount()) - (bitsPerWord() - getStartingPosInWord(pos));
		return std::max(0, bits);
	}

	[[gnu::always_inline]]static inline constexpr WordType mask() {
		return (WordType(1) << PackType::bitCount()) -1;
	}

	[[gnu::always_inline]]static inline constexpr unsigned int unusedBits() {
		return bitCount() - (elemCapacity() * PackType::bitCount());
	}

public:
	[[gnu::always_inline]]static inline constexpr unsigned int elemCapacity() {
		return bitCount() / PackType::bitCount();
	}

	[[gnu::always_inline]]static inline constexpr unsigned int bitCount() {
		return (bitsPerWord() * wordCount);
	}
};

#endif // WORDPACKING_H
