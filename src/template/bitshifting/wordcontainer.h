#ifndef WORDCONTAINER_H
#define WORDCONTAINER_H

template<class T>
struct WordContainer {
	T val;

	typedef  T WordType;

	inline WordContainer() {}
	inline WordContainer(const T&val) : val(val) {}

//	inline operator const T&() const {
//		return val;
//	}

	inline operator T&() {
		return val;
	}

	inline operator T() const {
		return val;
	}

	inline WordContainer<T>& operator=(const WordContainer<T> &other) {
		val = other.val;
		return *this;
	}

//	inline bool operator==(const WordContainer<T> &other) const {
//		return val == other.val;
//	}

	static inline constexpr unsigned int bitCount() {
		return sizeof(T) * 8;
	}
};

#endif // WORDCONTAINER_H
