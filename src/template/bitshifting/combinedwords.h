#ifndef COMBINDEDWORDS_H
#define COMBINDEDWORDS_H

#include <cstring>
#include <algorithm>

#include "template/get_nth_type.h"

template<class WordType, class ...BitTypes>
class CombinedWords {
	static_assert (std::is_unsigned<WordType>::value,  "the WordType has to be unsigned in order to do correct shifting operations");

protected:
	template<class T, class ...types>
	struct sumBits {
		static constexpr unsigned int value = sumBits<types...>::value + T::bitCount();
	};

	template<class T>
	struct sumBits<T> {
		static constexpr unsigned int value = T::bitCount();
	};

public:
	template<unsigned int pos>
	[[gnu::always_inline]] static inline constexpr unsigned int bitCountAt() {
		return get_Nth_type<pos, BitTypes...>::type::bitCount();
	}

	[[gnu::always_inline]] static inline constexpr unsigned int bitCount() {
		return sumBits<BitTypes...>::value;
	}

protected:
	[[gnu::always_inline]] static inline constexpr unsigned int wordCount() {
		if constexpr (bitCount() == 0) {
			return 0;
		}
		else {
			return ((bitCount() -1) / (sizeof (WordType) * 8)) +1;
		}
	}

	template<unsigned int pos>
	[[gnu::always_inline]] static inline constexpr unsigned int calcFirstBit() {
		static_assert (sizeof ...(BitTypes) >= (pos), "position out of range");

		if constexpr (pos == 0) {
			return 0;
		}
		else {
			return get_Nth_type<pos -1, BitTypes...>::type::bitCount() + calcFirstBit<pos -1>();
		}
	}

	template<unsigned int pos>
	[[gnu::always_inline]] static inline constexpr unsigned int calcLastBit() {
		static_assert (sizeof...(BitTypes) >= pos, "position out of range");
//		static_assert (t::bitCount() > 0, "type does not need any space");

		if constexpr (pos == 0 && get_Nth_type<pos, BitTypes...>::type::bitCount() == 0) {
			return 0;
		}
		else {
			return get_Nth_type<pos, BitTypes...>::type::bitCount() + calcFirstBit<pos>() -1;
		}
	}

	[[gnu::always_inline]] static inline constexpr unsigned int word(unsigned int bitPos) {
		return bitPos / (sizeof (WordType) * 8);
	}

	[[gnu::always_inline]] static inline constexpr unsigned int bitPosInWord(unsigned int bitPos) {
		return bitPos % (sizeof (WordType) * 8);
	}

	template<int pos>
	[[gnu::always_inline]] static inline constexpr unsigned int overflow() {
		const int bits = int (bitCountAt<pos>()) - int ((sizeof (WordType) * 8) - bitPosInWord(calcFirstBit<pos>()));
		return std::max(0, bits);
	}

	template<int pos>
	[[gnu::always_inline]] static inline constexpr WordType mask() {
		return (WordType(1) << bitCountAt<pos>()) -1;
	}

	template<int pos>
	[[gnu::always_inline]] static inline constexpr WordType maskFirstWord() {
		return WordType(mask<pos>() << bitPosInWord(calcFirstBit<pos>()));
	}

	template<int pos>
	[[gnu::always_inline]] static inline constexpr WordType maskSecondWord() {
		return (WordType(1) << overflow<pos>()) -1;
//		return mask<pos>() >> (bitPosInWord(calcLastBit<pos, BitTypes...>()) +1);
	}

	WordType store[wordCount()];

public:
	CombinedWords() {
		std::memset(store, 0, wordCount() * sizeof (WordType));
	}

	template<unsigned int pos>
	[[gnu::always_inline]] inline WordType get() const {
		static_assert (pos < sizeof ...(BitTypes), "pos is out of range");
		if constexpr (get_Nth_type<pos, BitTypes...>::type::bitCount() == 0) {
			return WordType();
		}
		else {
			const unsigned int firstBit = calcFirstBit<pos>();
			const unsigned int lastBit = calcLastBit<pos>();

			const unsigned int firstWord = word(firstBit);
			const unsigned int lastWord = word(lastBit);

			const unsigned int firstBitPos = bitPosInWord(firstBit);
//			const unsigned int secondBitPos = bitPosInWord(lastBit);

			WordType val = (store[firstWord] & maskFirstWord<pos>()) >> firstBitPos;
			val |= ((store[lastWord] & maskSecondWord<pos>()) << (bitCountAt<pos>() - overflow<pos>()));

			return val;
		}
	}

	template<unsigned int pos>
	[[gnu::always_inline]] inline void set(const WordType &val) {
		static_assert (pos < sizeof ...(BitTypes), "pos is out of range");

#ifdef DEBUG_TESTS
		assert(val == (val & mask<pos>())); //overflow!!
#endif

		if constexpr (get_Nth_type<pos, BitTypes...>::type::bitCount() == 0) {
			return;
		}
		else {
			const unsigned int firstBit = calcFirstBit<pos>();
			const unsigned int lastBit = calcLastBit<pos>();

			const unsigned int firstWord = word(firstBit);
			const unsigned int lastWord = word(lastBit);

			const unsigned int firstBitPos = bitPosInWord(firstBit);

			store[firstWord] = (store[firstWord] & ~maskFirstWord<pos>()) | (val << firstBitPos);
			store[lastWord] = (store[lastWord] & ~maskSecondWord<pos>()) | (val >> (bitCountAt<pos>() - overflow<pos>()));
		}
	}

	[[gnu::always_inline]] bool operator==(const CombinedWords &other) const {
		for (unsigned int i=0; i<wordCount(); ++i) {
			if (store[i] != other.store[i]) {
				return false;
			}
		}

		return true;
	}

	[[gnu::always_inline]] inline operator bool() const {
		for (unsigned int i=0; i<wordCount(); ++i) {
			if (store[i] != 0) {
				return true;
			}
		}
		return false;
	}

//	static_assert (wordCount() <= 1, "only one word please");
};

#endif // COMBINDEDWORDS_H
