#pragma once

#include <type_traits>

/**
 * The struct is instantiation of helps to identify if the class T is an instantiation of Template.
 * It evaluates to true type if Template args can be deduced.
 */
template < template <typename...> class Template, typename T >
struct is_instantiation_of : std::false_type {};

template < template <typename...> class Template, typename... Args >
struct is_instantiation_of< Template, Template<Args...> > : std::true_type {};
