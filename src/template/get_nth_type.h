#ifndef GET_NTH_TYPE_H
#define GET_NTH_TYPE_H

#include <type_traits>

template<std::size_t N, typename T, typename... types>
struct get_Nth_type
{
	using type = typename get_Nth_type<N - 1, types...>::type;
};

template<typename T, typename... types>
struct get_Nth_type<0, T, types...>
{
	using type = T;
};


template<std::size_t N, std::size_t T, std::size_t... types>
struct get_Nth_num
{
	static constexpr std::size_t value = get_Nth_num<N - 1, types...>::value;
};

template<std::size_t T, std::size_t... types>
struct get_Nth_num<0, T, types...>
{
	static constexpr std::size_t value = T;
};
#endif // GET_NTH_TYPE_H
