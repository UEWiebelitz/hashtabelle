#ifndef EMPTYDUMMY_H
#define EMPTYDUMMY_H

class EmptyDummy {
public:
	EmptyDummy(unsigned int) {}
	EmptyDummy() {}

	bool operator==(const EmptyDummy &) const {
		return true;
	}

	static inline constexpr unsigned int bitCount() {
		return 0;
	}

	operator unsigned int() const {
		return 0;
	}
};

#endif // EMPTYDUMMY_H
