#ifndef COUNTBITS_H
#define COUNTBITS_H

#include <cstddef>

constexpr size_t countBits(const size_t val) {
	if (val == 0) {
		return 0;
	}
	else {
		return 1 + countBits(val >> 1);
	}
}

constexpr size_t popCount(const size_t val) {
	if (val == 0) {
		return 0;
	}
	else {
		if (val & 1) {
			return 1 + popCount(val >> 1);
		}
		else {
			return popCount(val >> 1);
		}
	}
}

#endif // COUNTBITS_H
