#ifndef BITCOUNTER_H
#define BITCOUNTER_H

#include "counter.h"

template <size_t maxCount, class T>
class BitWiseCounter_MaxVal : public CounterBase<maxCount, T> {
	typedef CounterBase<maxCount, T> Base;

	static_assert (Base::bitCount() <= sizeof (T) * 8, "the counter cannot handle number maxCount ond storage type T");
	T word;

	constexpr inline static T mask() {
		T m = T (1) << Base::bitCount();
		m -= 1;
		return m;
	}

public:
	BitWiseCounter_MaxVal(const T& val) : word(val & mask()) {}

	inline operator T() {
		return word;
	}

	inline BitWiseCounter_MaxVal& operator++() {
		++word & mask();
		return *this;
	}

	inline BitWiseCounter_MaxVal& operator--() {
		--word & mask();
		return *this;
	}

	inline BitWiseCounter_MaxVal& operator=(const BitWiseCounter_MaxVal &other) {
		word = other.word;
		return *this;
	}
};

template <class T, size_t valBitCount>
class MaxBitWiseCounter {
	T word;

	inline static constexpr T mask() {
		T m = T (1) << bitCount();
		m -= 1;
		return m;
	}

public:
	inline MaxBitWiseCounter(const T& val = 0)
		: word(val & mask()) {}

	inline operator T() {
		return word;
	}

	inline MaxBitWiseCounter& operator++() {
		if (word == mask()) {
			return *this;
		}
		++word;
		return *this;
	}

	inline MaxBitWiseCounter& operator--() {
		if (word == 0) {
			return *this;
		}
		--word;
		return *this;
	}

	inline MaxBitWiseCounter& operator=(const MaxBitWiseCounter &other) {
		word = other.word;
		return *this;
	}

	inline static constexpr size_t bitCount() {
		return valBitCount;
	}

	inline operator T() const {
		return word;
	}

	static_assert (std::is_unsigned<T>::value, "type T should be unsigned");
	static_assert (bitCount() <= sizeof (T) * 8, "the counter cannot handle number maxCount ond storage type T");
};

#endif // BITCOUNTER_H
