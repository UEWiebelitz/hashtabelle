#ifndef COUNTER_H
#define COUNTER_H

#include <bitset>

#include "countbits.h"

template<unsigned long long maxCount, class T>
class CounterBase {
public:
	static inline constexpr unsigned short bitCount()
	{
		return countBits(maxCount);
	}
};


/**
 * implementation for right aligned counter
 */
template<unsigned long long maxCount, class T, bool rightAlign = true>
class Counter {};

template<unsigned long long maxCount, class T>
class Counter<maxCount, T, true> : public CounterBase<maxCount, T> {
protected:
	typedef CounterBase<maxCount, T> Base;

	constexpr inline static T mask() {
		T m = T (1) << bitCount();
		m -= 1;
		return m;
	}

public:
	class CounterAccessor  {
		T &val;

	public:
		inline CounterAccessor(T &val) : val(val) {}

		inline CounterAccessor& operator++() {
			inc(val);
			return *this;
		}

		inline CounterAccessor& operator--() {
			dec(val);
			return *this;
		}

		inline CounterAccessor& operator=(unsigned int value) {
			setValue(val, value);
			return *this;
		}

		inline operator T() {
			return value(val);
		}
	};

	class Const_CounterAccessor  {
		T &val;

	public:
		Const_CounterAccessor(T &val) : val(val) {}

		operator unsigned int() {
			return value(val);
		}
	};

	using Base::bitCount;

	static inline constexpr void inc(T &word) {
		word = (word & ~mask()) | ((word +1) & mask());
	}

	static inline constexpr void dec(T &word) {
		word = (word & ~mask()) | ((word -1) & mask());
	}

	static inline constexpr unsigned long long value(const T &word) {
		return word & mask();
	}

	static inline constexpr void setValue(T &word, const T &val) {
		word = (word & ~mask()) | (val & mask());
	}
};


/**
 * implementation for left aligned counter
 */
template<unsigned long long maxCount, class T>
class Counter<maxCount, T, false> : public CounterBase<maxCount, T> {
protected:
	typedef CounterBase<maxCount, T> Base;

	constexpr inline static T mask() {
		T m = T (1) << bitCount();
		m -= 1;
		return shiftBack(m);
	}

	static inline constexpr T shift(const T &word) {
		return word >> (sizeof (T) * 8 - bitCount());
	}

	static inline constexpr T shiftBack(const T &word) {
		return word << (sizeof (T) * 8 - bitCount());
	}

public:
	class CounterAccessor  {
		T &val;

	public:
		CounterAccessor(T &val) : val(val) {}

		inline CounterAccessor& operator++() {
			inc(val);
			return *this;
		}

		inline CounterAccessor& operator--() {
			dec(val);
			return *this;
		}

		inline CounterAccessor& operator=(unsigned int value) {
			setValue(val, value);
			return *this;
		}

		inline operator unsigned int() {
			return value(val);
		}
	};

	class Const_CounterAccessor  {
		const T &val;

	public:
		inline Const_CounterAccessor(const T &val) : val(val) {}

		operator T() {
			return value(val);
		}
	};

	using Base::bitCount;
	static inline constexpr void inc(T &word) {
		word = (word & ~mask()) | (shiftBack(shift(word) +1));
	}

	static inline constexpr void dec(T &word) {
		word = (word & ~mask()) | (shiftBack(shift(word) -1));
	}

	static inline constexpr unsigned long long value(const T &word) {
		return shift(word & mask());
	}

	static inline constexpr void setValue(T &word, const T &val) {
		word = (word & ~mask()) | shiftBack(val);
	}
};

#endif // COUNTER_H
