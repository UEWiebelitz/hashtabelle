#pragma once

#include <algorithm>
#include "compile_hints/inline_enforce.h"

class Wordpacking_Base {
protected:
	typedef  size_t WordType;

    const std::size_t m_packBitCount;
	const WordType m_mask;
//	const long long m_overflowConst;


	Wordpacking_Base(size_t packBitCount)
        : m_packBitCount(packBitCount)
		, m_mask(mask())
//		, m_overflowConst((long long)(packBitCount) - bitsPerWord())
	{

	}

   FORCE_INLINE  static constexpr unsigned int bitsPerWord() {
		return (sizeof (WordType) * 8);
	}

   FORCE_INLINE size_t getStartingWord(unsigned int pos) const {
        return (m_packBitCount * pos) / bitsPerWord();
	}

   FORCE_INLINE size_t getEndWord(unsigned int pos) const {
        return (m_packBitCount * (pos +1) -1) / bitsPerWord();
	}

   FORCE_INLINE size_t getStartingPosInWord(unsigned int pos) const {
        return (m_packBitCount * pos) % bitsPerWord();
	}

   FORCE_INLINE long long overflowBitCount(unsigned int pos) const {
        const int bits = int (m_packBitCount) - (bitsPerWord() - getStartingPosInWord(pos));
		return bits;
//		return std::max(0, bits);
	}

   FORCE_INLINE WordType mask() const {
        return (WordType(1) << m_packBitCount) -1;
	}

   FORCE_INLINE size_t packBitCount() const {
       return m_packBitCount;
   }
};
