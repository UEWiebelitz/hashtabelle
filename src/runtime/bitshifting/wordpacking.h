 #pragma once

#include <cstring>
#include <cassert>
#include <stdexcept>
#include <unistd.h>

#include "template/counters/countbits.h"
#include "wordpacking_access.h"

namespace Runtime {

class WordPacking : public WordPacking_access {
public:
	typedef  size_t WordType;
	typedef  size_t PackType;

protected:
	WordType *arr;

public:
	WordPacking(size_t wordCount, size_t packBitCount)
        : WordPacking_access(packBitCount, wordCount)
	{
		assert(sizeof(WordType) * 8 >= packBitCount && "WordType is too small for PackType");

        void *mem = aligned_alloc(static_cast<size_t>(sysconf(_SC_LEVEL2_CACHE_LINESIZE)), m_wordCount *sizeof(WordType));
		if (!mem) {
			throw std::bad_alloc();
		}
		arr = reinterpret_cast<WordType*>(mem);

		std::memset(arr, 0, sizeof (WordType) * wordCount);
	}

	~WordPacking() {
		free(arr);
	}

#ifdef PREFETCH
    FORCE_INLINE void prefetch(size_t pos) const {
        size_t bitPos = pos * m_packBitCount;
		size_t startingWord = bitPos >> countBits(bitsPerWord()-1);
        __builtin_prefetch(arr + startingWord);
#ifdef PREFETCH_NEXT_CACHE_LINE
        __builtin_prefetch(arr + startingWord + (64 / sizeof(WordType)));
#endif
//		__builtin_prefetch(arr + startingWord +1);
	}
#endif

    FORCE_INLINE PackType get(size_t pos) const {
		return WordPacking_access::get(arr, pos);
	}

    FORCE_INLINE PackType getAtBitPos(size_t bitPos) const {
        return WordPacking_access::getAtBitPos(arr, bitPos);
    }

    FORCE_INLINE PackType operator[](size_t pos) const {
		return get(pos);
	}

    FORCE_INLINE void reset(size_t pos) {
		return WordPacking_access::reset(arr, pos);
	}

    FORCE_INLINE void resetAndStore(size_t pos, const PackType &value) {
		return WordPacking_access::resetAndStore(arr, pos, value);
	}

//    inline WordType convert(const PackType &value) const {
//		static_assert (sizeof (WordType) >= sizeof (PackType), "PackType has to be smaller than WordType");
//		return (*reinterpret_cast<const WordType*>(&value));
//	}

//	inline PackType unconvert(const WordType &value) const {
//		return (*reinterpret_cast<const PackType*>(&value));
//	}

    FORCE_INLINE void store(size_t pos, const PackType &value) {
		return WordPacking_access::store(arr, pos, value);
	}

    inline size_t elemCapacity() const {
        return WordPacking_access::elemCapacity();
    }
};

}
