#pragma once

#include <cstring>
#include <algorithm>
#include <array>
#include <vector>

#include "template/get_nth_type.h"
#include "compile_hints/inline_enforce.h"

namespace Runtime {

class BitWord {
	const size_t m_bitCount;

public:
	BitWord(size_t bitCount)
		: m_bitCount(bitCount) {}

    FORCE_INLINE size_t bitCount() {return m_bitCount;}
};

template<size_t elemCount>
class CombinedWords {
//	static_assert (std::is_unsigned<WordType>::value,  "the WordType has to be unsigned in order to do correct shifting operations");

public:
    typedef size_t WordType;

protected:
	size_t m_startingBit[elemCount];
	size_t m_bitCount[elemCount];
    WordType m_masks[elemCount];
	size_t m_maskFirstWord[elemCount];

public:
    CombinedWords(const std::array<size_t, elemCount> &bitCounts) {
		for (size_t i=0; i<elemCount; ++i) {
			addType(i, bitCounts[i]);
		}

//		m_wordCount = 	bitCount() ? ((bitCount() -1) / (sizeof (WordType) * 8)) +1 : 0;
	}

    void addType(size_t pos, size_t bC) {
		if (pos == 0) {
			m_startingBit[pos] = 0;
		}
		else {
			m_startingBit[pos] = m_startingBit[pos -1] + m_bitCount[pos -1];
		}

		m_bitCount[pos] = bC;
		m_masks[pos] = (size_t(1) << bC) -1;
		m_maskFirstWord[pos] = m_masks[pos] << m_startingBit[pos];
	}

	FORCE_INLINE size_t bitCount(size_t pos) const {
		return m_bitCount[pos];
	}

    FORCE_INLINE size_t bitCount() const {
		return m_startingBit[elemCount -1] + m_bitCount[elemCount -1];
	}

protected:
    FORCE_INLINE size_t bitPosInWord(size_t bitPos) const {
		return bitPos % (sizeof (WordType) * 8);
	}

public:
    FORCE_INLINE WordType get(size_t pos, const WordType &arr) const {
#ifdef DEBUG_TESTS
		if (pos >= elemCount) {
			throw std::out_of_range("access to combindedwords is out of range!");
		}
#endif

		return (arr >> m_startingBit[pos]) & m_masks[pos];
	}

    FORCE_INLINE void set(size_t pos, WordType &arr, WordType val) const {
#ifdef DEBUG_TESTS
		if (pos >= elemCount) {
			throw std::out_of_range("access to combindedwords is out of range!");
		}

		if ((val & m_masks[pos]) != val) {
			throw std::invalid_argument("given val overflows bitCount!");
		}
#endif

        arr &= ~m_maskFirstWord[pos];
		arr |= val << m_startingBit[pos];
	}

    FORCE_INLINE bool operator==(const CombinedWords &other) = delete;

    FORCE_INLINE bool isNull(const WordType &arr) const {
		return arr == 0;
	}
};

}
