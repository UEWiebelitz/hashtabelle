#pragma once

#include <cstring>
#include <cassert>
#include <stdexcept>

#include "wordpacking_base.h"
#include "template/counters/countbits.h"

namespace Runtime {

class WordPacking_access : public Wordpacking_Base {
//	static_assert(sizeof(WordType) * 8 >= packBitCount, "WordType is too small for PackType");

public:
	typedef  size_t WordType;
	typedef  size_t PackType;

protected:
	const size_t m_wordCount;
	const size_t m_elemCapacity;

    constexpr static size_t log_bitsPerWord = countBits(bitsPerWord()-1);

public:
	WordPacking_access(size_t packBitCount, size_t wordCount)
		: Wordpacking_Base(packBitCount)
		, m_wordCount(wordCount)
		, m_elemCapacity(bitCount() / packBitCount)
	{
		assert(sizeof(WordType) * 8 >= packBitCount && "WordType is too small for PackType");
	}

    FORCE_INLINE PackType getAtBitPos(const WordType *arr, size_t bitPos) const {
//#ifdef DEBUG_TESTS
//        assert(pos < elemCapacity() && "pos is out of range");
//#endif

        size_t startingWord = bitPos >> log_bitsPerWord;
        size_t endWord = (bitPos + m_packBitCount - 1) >> log_bitsPerWord;
        size_t startingPosInWord = bitPos & (bitsPerWord() -1);

        WordType value = arr[startingWord] >> startingPosInWord;
        value |= arr[endWord] << (bitsPerWord() - startingPosInWord);
        value &= m_mask;
        return value;
    }

    FORCE_INLINE PackType get(const WordType *arr, size_t pos) const {
#ifdef DEBUG_TESTS
		assert(pos < elemCapacity() && "pos is out of range");
#endif

        size_t bitPos = pos * m_packBitCount;
        size_t startingWord = bitPos >> log_bitsPerWord;
        size_t endWord = (bitPos + m_packBitCount - 1) >> log_bitsPerWord;
		size_t startingPosInWord = bitPos & (bitsPerWord() -1);

		WordType value = arr[startingWord] >> startingPosInWord;
		value |= arr[endWord] << (bitsPerWord() - startingPosInWord);
        value &= m_mask;
		return value;
	}

    FORCE_INLINE void reset(WordType *arr, size_t pos) const {
#ifdef DEBUG_TESTS
		assert(pos < elemCapacity() && "pos is out of range");
#endif

        size_t bitPos = pos * m_packBitCount;
        size_t startingWord = bitPos >> log_bitsPerWord;
        size_t endWord = (bitPos + m_packBitCount - 1) >> log_bitsPerWord;
		size_t startingPosInWord = bitPos & (bitsPerWord() -1);

        arr[startingWord] &= ~(m_mask << startingPosInWord);
        arr[endWord] &= ~(m_mask >> (bitsPerWord() - startingPosInWord));
	}

    FORCE_INLINE void resetAndStore(WordType *arr, size_t pos, const PackType &value) const {
#ifdef DEBUG_TESTS
		assert(pos < elemCapacity() && "pos is out of range");
        assert((value & m_mask) == value && "value is not correclty masked. It consumes more bits than allowed.");
#endif

        size_t bitPos = pos * m_packBitCount;
        size_t startingWord = bitPos >> log_bitsPerWord;
        size_t endWord = (bitPos + m_packBitCount - 1) >> log_bitsPerWord;
		size_t startingPosInWord = bitPos & (bitsPerWord() -1);

        arr[startingWord] &= ~(m_mask << startingPosInWord);
        arr[endWord] &= ~(m_mask >> (bitsPerWord() - startingPosInWord));

		arr[startingWord] |= value << startingPosInWord;
		arr[endWord] |= value >> (bitsPerWord() - startingPosInWord);
	}

    FORCE_INLINE void store(WordType *arr, size_t pos, const PackType &value) const {
#ifdef DEBUG_TESTS
		assert(pos < elemCapacity() && "pos is out of range");
        assert((value & m_mask) == value && "value is not correclty masked. It consumes more bits than allowed.");
#endif

        size_t bitPos = pos * m_packBitCount;
        size_t startingWord = bitPos >> log_bitsPerWord;
        size_t endWord = (bitPos + m_packBitCount - 1) >> log_bitsPerWord;
		size_t startingPosInWord = bitPos & (bitsPerWord() -1);

		arr[startingWord] |= value << startingPosInWord;
		arr[endWord] |= value >> (bitsPerWord() - startingPosInWord);
	}

protected:
    FORCE_INLINE size_t unusedBits() const {
        return bitCount() - (elemCapacity() * m_packBitCount);
	}

public:
    FORCE_INLINE size_t elemCapacity() const {
		return m_elemCapacity;
	}

    FORCE_INLINE size_t bitCount() const {
		return (bitsPerWord() * m_wordCount);
	}

    FORCE_INLINE size_t wordCount() const {
		return m_wordCount;
	}

    using Wordpacking_Base::packBitCount;
};

}
