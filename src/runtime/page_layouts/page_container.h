#pragma once

#include <type_traits>
#include <functional>
#include <unistd.h>
#include <cstring>

#include "compile_hints/inline_enforce.h"
#include "return_indicators.h"

template <class PageAccess>
class Page_Container {
    const size_t m_pageCount;
    const size_t m_bitCountPerKey;
    const PageAccess &pAccess;
	using StoreType = size_t;
	typename PageAccess::WordType *arr;


    size_t elemCountOf(size_t val, size_t wordsPerPage, std::function<std::size_t(std::size_t /*hashPositionsCnt*/)> bitCount) const {
        size_t keyBitCnt = bitCount(val);
        std::size_t elemsPerPage = wordsPerPage * sizeof (typename PageAccess::WordType) * 8 / keyBitCnt;
		return val * elemsPerPage;
	}

    size_t binSizeSearch(size_t elemCount, size_t wordsPerPage, std::function<std::size_t(std::size_t /*hashPositionsCnt*/)> bitCount) const {
		size_t val = 1;
        while (elemCountOf(val, wordsPerPage, bitCount) < elemCount) {
			val *=2;
		}

		size_t lower = val/2;
		size_t upper = val;
		while (lower < upper) {
			size_t mid = (upper + lower) /2;

            size_t midElemCount = elemCountOf(mid, wordsPerPage, bitCount);

            if (midElemCount < elemCount) {
				lower = mid +1;
			}
			else {
				upper = mid;
			}
		}

		return upper;
	}

	inline typename PageAccess::WordType *getPage(size_t pageIdx) const {
		return arr + (pageIdx * pAccess.wordCount());
	}

public:
    Page_Container(std::size_t elemCount, size_t wordsPerPage, const PageAccess &pageAccess, std::function<std::size_t(std::size_t /*hashPositionsCnt*/)> bitCount)
        : m_pageCount(binSizeSearch(elemCount, wordsPerPage, bitCount) | 1)
        , m_bitCountPerKey(bitCount(m_pageCount))
		, pAccess(pageAccess)
	{
        size_t memory = m_pageCount * wordsPerPage * sizeof (typename PageAccess::WordType);
#ifdef CACHE_UNALIGNED
        arr = reinterpret_cast<typename PageAccess::WordType*>(aligned_alloc(sysconf(_SC_LEVEL2_CACHE_LINESIZE), memory + sysconf(_SC_LEVEL2_CACHE_LINESIZE)));
        std::memset(arr, 0, memory + sysconf(_SC_LEVEL2_CACHE_LINESIZE));
        arr = reinterpret_cast<typename PageAccess::WordType*>(reinterpret_cast<char*>(arr) + sysconf(_SC_LEVEL2_CACHE_LINESIZE) -1);
#else
        arr = reinterpret_cast<typename PageAccess::WordType*>(aligned_alloc(sysconf(_SC_LEVEL2_CACHE_LINESIZE), memory));
        std::memset(arr, 0, memory);
#endif
	}

	~Page_Container() {
#ifdef CACHE_UNALIGNED
        arr = reinterpret_cast<typename PageAccess::WordType*>(reinterpret_cast<char*>(arr) - (sysconf(_SC_LEVEL2_CACHE_LINESIZE) -1));
		free(arr);
#else
        free(arr);
#endif
	}

    FORCE_INLINE void prefetch(size_t pos) const {
		__builtin_prefetch(getPage(pos));
	}

    FORCE_INLINE AppendResult insert(size_t pos, StoreType storeVal) {
		return pAccess.insert(getPage(pos), storeVal);
	}

    FORCE_INLINE LookupResult lookup(size_t pos, StoreType storeVal) const {
		return pAccess.lookup(getPage(pos), storeVal);
	}

    FORCE_INLINE StoreType get(size_t pos, size_t posInPage) const {
        return pAccess.get(getPage(pos), posInPage);
    }

    FORCE_INLINE typename PageAccess::Pack_AccessSupport value(size_t pos, StoreType storeVal) {
		return pAccess.value(getPage(pos), storeVal);
	}

    FORCE_INLINE StoreType replace(size_t page, size_t posInPage, StoreType storeVal) {
		return pAccess.replace(getPage(page), posInPage, storeVal);
	}

    FORCE_INLINE size_t hashPositionCnt() const {
		return m_pageCount;
	}

    FORCE_INLINE size_t bitCntPerKey() const {
        return m_bitCountPerKey;
    }

    FORCE_INLINE size_t pageCapacity() const {
		return pAccess.elemCapacity();
	}

    FORCE_INLINE size_t elemCapacity() const {
		return pageCapacity() * m_pageCount;
	}

    FORCE_INLINE size_t elemCount(size_t pagePos) const {
        return pAccess.elemCount(getPage(pagePos));
	}

    FORCE_INLINE size_t elemCount() const {
		size_t elemCountSum = 0;
		for (int i=0; i<m_pageCount; ++i) {
			elemCountSum += elemCount(i);
		}

		return elemCountSum;
	}

    FORCE_INLINE size_t wordsPerPage() const {
		return pAccess.wordCount();
	}

    FORCE_INLINE size_t memory() const {
		return m_pageCount * pAccess.wordCount() * sizeof (typename PageAccess::WordType);
	}

    std::vector<size_t> getPageFillrates() const {
        std::vector<size_t> counts(pAccess.elemCapacity() +1);
        for (size_t i=0; i<=pAccess.elemCapacity(); ++i) {
            counts[i] = 0;
        }

        for (size_t i=0; i<m_pageCount; ++i) {
            ++counts[elemCount(i)];
        }

        return counts;
    }
};
