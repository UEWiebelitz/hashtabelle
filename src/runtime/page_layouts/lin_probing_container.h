#pragma once

#include <functional>

#include "runtime/bitshifting/wordpacking.h"
#include "runtime/bitshifting/combinedwords.h"

#include "compile_hints/inline_enforce.h"
#include "return_indicators.h"
#include "compile_hints/likely_hint.h"

namespace Runtime {

class LinProbContainer : public WordPacking {
    const size_t m_linProbRange;
	const size_t m_fingerprintBitCnt;

    size_t calcWordCount(size_t elemCount, std::function<std::size_t(std::size_t /*hashPositionsCnt*/)> bitCount) const {
        size_t bitCntElem = bitCount(elemCount);
        size_t bitCntAll = bitCntElem * elemCount;
        size_t wordBitCnt = sizeof (WordType) * 8;
        size_t wordCnt = bitCntAll / wordBitCnt;

        if (wordCnt * wordBitCnt == bitCntAll) {
            return wordCnt;
        }
        else {
            return wordCnt +1;
        }
    }

    struct StoreAccess : public CombinedWords<4> { //combination of [0]: linProbOffset, [1]: hashFuncIdx +1, [2]: key, [3] value
        const size_t m_mask;

        StoreAccess(const std::array<size_t, 4> &bitCounts)
            : CombinedWords(bitCounts)
            , m_mask(m_maskFirstWord[0] | m_maskFirstWord[1] | m_maskFirstWord[2])
        {}

        FORCE_INLINE bool equals(const WordType &val1, const WordType &val2) const {
            return (val1 & m_mask) == (val2 & m_mask);
        }
    };

    const StoreAccess storeAccess;

#ifdef LIN_PROB_MODULO
    const size_t m_bitModulo;

    FORCE_INLINE size_t mapPos(size_t pos) const {
        return  pos % m_elemCount;
    }

    FORCE_INLINE PackType getAtBitPos(size_t bitPos) const {
        return WordPacking::getAtBitPos(bitPos % m_bitModulo);
    }

    FORCE_INLINE PackType get(size_t pos) const {
        return WordPacking::get(mapPos(pos));
    }

    FORCE_INLINE void reset(size_t pos) {
        return WordPacking::reset(mapPos(pos));
    }

    FORCE_INLINE void resetAndStore(size_t pos, const PackType &value) {
        return WordPacking::resetAndStore(mapPos(pos), value);
    }

    FORCE_INLINE void store(size_t pos, const PackType &value) {
        return WordPacking::store(mapPos(pos), value);
    }

#elif defined(LIN_PROB_IF_DIFF)
    const size_t m_bitModulo;

    FORCE_INLINE size_t mapPos(size_t pos) const {
        if (unlikely(pos >= m_elemCount)) {
            return  pos - m_elemCount;
        }
        else {
            return pos;
        }
    }

    FORCE_INLINE PackType getAtBitPos(size_t bitPos) const {
        if (unlikely(bitPos >= m_bitModulo)) {
            return WordPacking::getAtBitPos(bitPos - m_bitModulo);
        }
        else {
            return WordPacking::getAtBitPos(bitPos);
        }
    }

    FORCE_INLINE PackType get(size_t pos) const {
        return WordPacking::get(mapPos(pos));
    }

    FORCE_INLINE void reset(size_t pos) {
        return WordPacking::reset(mapPos(pos));
    }

    FORCE_INLINE void resetAndStore(size_t pos, const PackType &value) {
        return WordPacking::resetAndStore(mapPos(pos), value);
    }

    FORCE_INLINE void store(size_t pos, const PackType &value) {
        return WordPacking::store(mapPos(pos), value);
    }
#endif

    const size_t m_elemCount;

public:
#if defined(LIN_PROB_MODULO) || defined (LIN_PROB_IF_DIFF)
    LinProbContainer(size_t elemCount, size_t linProbRange, size_t hfCnt, size_t valBitCnt, std::function<std::size_t(std::size_t /*hashPositionsCnt*/)> bitCount)
        : WordPacking(
              calcWordCount(elemCount | 1, [&](size_t hashPosCnt){return bitCount(hashPosCnt) + countBits(linProbRange) + countBits(hfCnt) + valBitCnt;})
            , bitCount(elemCount|1) + countBits(linProbRange) + countBits(hfCnt) + valBitCnt)
        , m_linProbRange(linProbRange)
        , m_fingerprintBitCnt(bitCount(elemCount | 1))
        , storeAccess({countBits(linProbRange), countBits(hfCnt), bitCount(elemCount | 1), valBitCnt})
        , m_bitModulo((elemCount | 1) * WordPacking::packBitCount())
        , m_elemCount(elemCount | 1)
    {}
#else
    LinProbContainer(size_t elemCount, size_t linProbRange, size_t hfCnt, size_t valBitCnt, std::function<std::size_t(std::size_t /*hashPositionsCnt*/)> bitCount)
		: WordPacking(calcWordCount((elemCount | 1) + linProbRange, [&](size_t hashPosCnt){return bitCount(hashPosCnt) + countBits(linProbRange) + countBits(hfCnt) + valBitCnt;})
            , bitCount(elemCount|1) + countBits(linProbRange) + countBits(hfCnt) + valBitCnt)
		, m_linProbRange(linProbRange)
		, m_fingerprintBitCnt(bitCount(elemCount | 1))
        , storeAccess({countBits(linProbRange), countBits(hfCnt), bitCount(elemCount | 1), valBitCnt})
        , m_elemCount(elemCount | 1)
    {}
#endif

	const std::string getTypeStr() const {
		static std::string type("linear probing");
		return type;
	}

    template<class StoreInfo>
    FORCE_INLINE LookupResult lookup(const StoreInfo &si) {
        size_t storeVal = 0;
        storeAccess.set(1, storeVal, si.hfIdx +1);
        storeAccess.set(2, storeVal, si.storeVal);

        size_t bitPos = si.pos * packBitCount();
        for (size_t i=0; i<=m_linProbRange; ++i) {
            storeAccess.set(0, storeVal, i);
            WordType lookupVal = getAtBitPos(bitPos);
//            WordType lookupVal = get(si.pos + i);
            if (unlikely(storeAccess.equals(lookupVal, storeVal))) {
                return LOOKUP_EXISTS;
            }
            else if (unlikely(storeAccess.equals(lookupVal, 0))) {
                return LOOKUP_NOT_EXISTS;
            }
            bitPos += packBitCount();
        }

        return LOOKUP_UNSURE;
    }

    template<class StoreInfo>
    FORCE_INLINE AppendResult insert(const StoreInfo &si) {
        size_t storeVal = 0;
        storeAccess.set(1, storeVal, si.hfIdx +1);
        storeAccess.set(2, storeVal, si.storeVal);
        storeAccess.set(3, storeVal, si.val);

        size_t bitPos = si.pos * packBitCount();
        for (size_t i=0; i<=m_linProbRange; ++i) {
            storeAccess.set(0, storeVal, i);
            WordType lookupVal = getAtBitPos(bitPos);
//            WordType lookupVal = get(si.pos + i);
            if (unlikely(storeAccess.equals(lookupVal, storeVal))) {
                resetAndStore(si.pos + i, storeVal);
                return APPEND_EXISTS;
            }
            else if (unlikely(storeAccess.equals(lookupVal,0))) {
                store(si.pos + i, storeVal);
                return APPEND_APPENDED;
            }
            bitPos += packBitCount();
        }

        return APPEND_FULL;
    }

    class AccessSupport {
        LookupResult m_res;
        LinProbContainer *m_linProbCont;
        size_t m_pos;

    public:
        AccessSupport(LookupResult res, LinProbContainer *linProbCont = nullptr, size_t pos = 0)
            : m_res(res)
            , m_linProbCont(linProbCont)
    #ifdef LIN_PROB_MODULO
            , m_pos(m_linProbCont->mapPos(pos))
    #else
            , m_pos(pos)
    #endif
        {}

        explicit inline operator LookupResult() const {
            return m_res;
        }

		using WordType = LinProbContainer::WordType;

        explicit inline operator WordType() const {
            WordType storeVal = m_linProbCont->get(m_pos);
            return m_linProbCont->storeAccess.get(3, storeVal);
        }

        inline bool isValid() const {
            return m_res == LOOKUP_EXISTS;
        }

        inline WordType get() const {
#ifdef DEBUG_TESTS
                assert(isValid() && "accessing a non existend value");
#endif

                return operator WordType();
        }

        inline LookupResult res() const {
                return m_res;
        }

        inline void set(const WordType &val) {
#ifdef DEBUG_TESTS
                assert(isValid() && "accessing a non existend value");
#endif

                WordType storeVal = m_linProbCont->get(m_pos);
                m_linProbCont->storeAccess.set(3, storeVal, val);
                m_linProbCont->resetAndStore(m_pos, storeVal);
        }

//        inline bool operator==(const WordType &other) {
//                return get() == other;
//        }
    };

    template<class StoreInfo>
    FORCE_INLINE auto value(const StoreInfo &si) {
        size_t storeVal = 0;
        storeAccess.set(1, storeVal, si.hfIdx +1);
        storeAccess.set(2, storeVal, si.storeVal);

        for (size_t i=0; i<=m_linProbRange; ++i) {
            storeAccess.set(0, storeVal, i);
            WordType lookupVal = get(si.pos + i);
            if (storeAccess.equals(lookupVal, storeVal)) {
                return AccessSupport (LOOKUP_EXISTS, this, si.pos + i);
            }
            else if (storeAccess.equals(lookupVal, 0)) {
                return AccessSupport (LOOKUP_NOT_EXISTS);
            }
        }
        return AccessSupport (LOOKUP_UNSURE);
    }

    template<class ReplaceInfo>
    FORCE_INLINE ReplaceInfo replace(const ReplaceInfo &ri) {
        typename StoreAccess::WordType storeVal = 0;
        storeAccess.set(1, storeVal, ri.hfIdx +1);
        storeAccess.set(2, storeVal, ri.storeVal);
        storeAccess.set(3, storeVal, ri.val);

        typename StoreAccess::WordType removedstoreVal = get(ri.pos + ri.offset);
        resetAndStore(ri.pos + ri.offset, storeVal);

        ReplaceInfo removed;

        removed.pos = (ri.pos + ri.offset - storeAccess.get(0, removedstoreVal) + hashPositionCnt()) % hashPositionCnt();
        removed.offset = 0;
        removed.hfIdx = storeAccess.get(1, removedstoreVal) -1;
        removed.storeVal = storeAccess.get(2, removedstoreVal);
        removed.val = storeAccess.get(3, removedstoreVal);

        return removed;
    }

    FORCE_INLINE size_t pageCapacity() const {
        return 1;
    }

//    using WordPacking::elemCapacity;
    inline size_t elemCount(size_t pos) const {
        return storeAccess.equals(get(pos), 0) ? 0 : 1;
    }

    inline size_t memory() const {
        return m_wordCount * sizeof (WordType);
    }

    FORCE_INLINE size_t hashPositionCnt() const {
        return m_elemCount;
    }

	inline size_t getLinProbRange() const {
		return m_linProbRange;
    }

	inline size_t bitCntPerKey() const {
		return storeAccess.bitCount();
	}

	inline size_t fingerPrintBitCount() const {
		return m_fingerprintBitCnt;
	}

    inline size_t getValueBitCount() const {
        return storeAccess.bitCount(3);
    }

    std::unordered_map<size_t, size_t> getValueCounts() const {
        std::unordered_map<size_t, size_t> valueCounts;

#if defined(LIN_PROB_MODULO) || defined (LIN_PROB_IF_DIFF)
        for (size_t i=0; i<hashPositionCnt(); ++i) {
#else
        for (size_t i=0; i<hashPositionCnt() + m_linProbRange; ++i) {
#endif
            WordType lookupVal = WordPacking::get(i);
            if (storeAccess.get(1, lookupVal)) { //the field is not empty
                size_t val = storeAccess.get(3, lookupVal);
                auto it = valueCounts.find(val);
                if (it == valueCounts.end()) { //this value count is currently not in the map
                    valueCounts.insert({val, 1});
                }
                else {
                    ++it->second;
                }
            }
        }

        return valueCounts;
    }

};

}
