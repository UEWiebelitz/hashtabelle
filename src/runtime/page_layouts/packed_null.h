#pragma once

#include <cstring>
#include <unistd.h>
#include <functional>

#include "return_indicators.h"
#include "access_support.h"

#include "runtime/bitshifting/wordpacking_access.h"
#include "template/counters/counter.h"
#include "compile_hints/inline_enforce.h"
#include "compile_hints/likely_hint.h"

namespace Runtime {

/**
 * The class Page_Bytewise_Counting provides a very basic page layout: Each value is simply stored in at least one byte. The values are byte-aligned.
 */
template<class PackType, class Comparer>
class PageAccess_Null {
    const PackType pack;
    const Comparer &comp;
//	std::function<bool(const typename PackType::PackType &val1, const typename PackType::PackType &val2)> equals;

public:
	using PackT = PackType;
	typedef  typename PackType::WordType WordType;
	typedef  size_t StoreType;

	typedef  WordType WordT;
	typedef  StoreType StoreT;

//    PageAccess_Null(size_t packBitCount, size_t wordCount, const std::function<bool(const typename PackType::PackType &val1, const typename PackType::PackType &val2)> &comp)
    PageAccess_Null(size_t packBitCount, size_t wordCount, const Comparer &comp)
		: pack(packBitCount, wordCount)
        , comp(comp) {}

	/**
	 * @brief elemCapacity
	 * @return largest number of elements that can be stored in one page.
	 */
    FORCE_INLINE size_t elemCapacity() const {
		return pack.elemCapacity();
	}

    FORCE_INLINE StoreType get(const WordType *arr, unsigned int pos) const {
		return pack.get(arr, pos);
	}

    FORCE_INLINE StoreType getAtBitPos(const WordType *arr, unsigned int pos) const {
        return pack.getAtBitPos(arr, pos);
    }

    FORCE_INLINE StoreType replace(WordType *arr, unsigned int pos, const StoreType &val) const {
		StoreType valueBefore = pack.get(arr, pos);
		pack.resetAndStore(arr, pos, val);
		return valueBefore;
	}

	/**
	 * @brief append adds a value to this page if the page is not full and val is not already in page.
	 * @param val
	 * @return
	 */
    FORCE_INLINE AppendResult insert(WordType *arr, const StoreType &val) const {
//		asm volatile("NOP #insert_begin");

        size_t pos = 0;
		for(size_t i=0; i<elemCapacity(); ++i) {
            StoreType lookupVal = getAtBitPos(arr, pos);
//			StoreType lookupVal = get(arr, i);
            if (unlikely(comp.equals(lookupVal, val))) {
				pack.resetAndStore(arr, i, val);
				return APPEND_EXISTS;
			}
            else if (unlikely(comp.equals(lookupVal, 0))) {
				storeAt(arr, i, val);
				return APPEND_APPENDED;
			}
            pos += pack.packBitCount();
		}

//		asm volatile("NOP #insert_end");
		return APPEND_FULL;
	}

    FORCE_INLINE LookupResult lookup(WordType *arr, const StoreType &val) const {
//		asm volatile("NOP #lookup_begin");

#ifdef LOOKUP_SIMD
		bool exists = false;
//		#pragma clang loop vectorize(enable) interleave(enable)
		#pragma clang loop vectorize(enable)
		for(size_t i=0; i<elemCapacity(); ++i) {
			StoreType lookupVal = get(arr, i);
            exists |= comp.equals(lookupVal, val);
		}
		if (exists) {
			return LOOKUP_EXISTS;
		}
		if (comp.equals(get(arr, elemCapacity() -1), 0)) {
			return LOOKUP_NOT_EXISTS;
		}

#else
        size_t pos = 0;
        for(size_t i=0; i<elemCapacity(); ++i) {
            StoreType lookupVal = getAtBitPos(arr, pos);
//            StoreType lookupVal = get(arr, i);
            if (unlikely(comp.equals(lookupVal, val))) {
				return LOOKUP_EXISTS;
			}
            else if (unlikely(comp.equals(lookupVal, 0))) {
				return LOOKUP_NOT_EXISTS;
			}
            pos += pack.packBitCount();
		}
#endif

//		asm volatile("NOP #lookup end");
		return LOOKUP_UNSURE;
	}

	typedef  AccessSupport<PageAccess_Null> Pack_AccessSupport;
	friend Pack_AccessSupport;

    FORCE_INLINE Pack_AccessSupport value(WordType *arr, const StoreType &val) const {
		for(size_t i=0; i<elemCapacity(); ++i) {
			StoreType lookupVal = get(arr, i);
            if (unlikely(comp.equals(lookupVal, val))) {
				return Pack_AccessSupport(LOOKUP_EXISTS, arr, this, i);
			}
            else if (unlikely(comp.equals(lookupVal, 0))) {
				return Pack_AccessSupport(LOOKUP_NOT_EXISTS);
			}
		}

		return Pack_AccessSupport(LOOKUP_UNSURE);
	}

	/**
	 * @brief elemCount
	 * @return number of elements that are actually stored in this page
	 */
    FORCE_INLINE size_t elemCount(WordType *arr) const {
		for (size_t i=0; i<elemCapacity(); ++i) {
            if (unlikely(comp.equals(get(arr, i), 0))) {
				return i;
			}
		}
		return  elemCapacity();
	}

	/**
	 * @brief elemCount
	 * @return number of elements that are actually stored in this page
	 */
    FORCE_INLINE size_t wordCount() const {
		return pack.wordCount();
	}

protected:
    FORCE_INLINE bool isEmpty(WordType *arr, unsigned int pos) const {
		return get(arr, pos) == 0;
	}

    FORCE_INLINE void storeAt(WordType *arr, unsigned int pos, const StoreType &val) const {
		pack.store(arr, pos, val);
	}
};

}
