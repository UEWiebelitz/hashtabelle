#pragma once

#include "page_container.h"
#include "runtime/bitshifting/combinedwords.h"
#include "template/counters/countbits.h"
#include "runtime/page_layouts/packed_null.h"

#include "compile_hints/inline_enforce.h"

namespace Runtime {

/**
 * @brief The KeyStoreType class is a word packing container that encodes the used hashfunction, the key and the value.
 * The compare-operator is overloaded and does only use the hashfunktion and the key. The value is not considered on comparison.
 */
class StoreAccess : public CombinedWords<3> { //combination of [0]: hashFuncIdx +1, [1]: key, [2] value
protected:
    const size_t firstTwoMask;
public:
    inline StoreAccess(const std::array<size_t, 3> &bitCounts)
        : CombinedWords(bitCounts)
        ,  firstTwoMask (m_maskFirstWord[0] | m_maskFirstWord[1])
    {
//        firstTwoMask = m_maskFirstWord[0] | m_maskFirstWord[1];
    }

    using CombinedWords::CombinedWords;

    FORCE_INLINE bool equals(const WordType arr1, const WordType arr2) const {
        return (arr1 & firstTwoMask) == (arr2 & firstTwoMask);
//			return CombinedWords::get(0, arr1) == CombinedWords::get(0, arr2) && CombinedWords::get(1, arr1) == CombinedWords::get(1, arr2);
    }
};

template<class PackType> //Runtime::PageAccess_Packed_Null<WordPacking_access>
class HT_PageContainer : public Page_Container<Runtime::PageAccess_Null<PackType, StoreAccess>> {
    const StoreAccess storeAccess;
    using PageAccess = Runtime::PageAccess_Null<PackType, StoreAccess>;
    const PageAccess pageAccess;
    using WordType = size_t;

public:
	HT_PageContainer(std::size_t elemCount, size_t wordsPerPage, size_t hfCnt, size_t valBitCnt, const std::function<std::size_t(std::size_t /*hashPositionsCnt*/)> &bitCount)
        : Page_Container<PageAccess>(elemCount, wordsPerPage, pageAccess, [&](size_t hashPositions){return bitCount(hashPositions) + countBits(hfCnt) + valBitCnt;})
		, storeAccess({countBits(hfCnt), bitCount(Page_Container<PageAccess>::hashPositionCnt()), valBitCnt})
        , pageAccess(bitCount(Page_Container<PageAccess>::hashPositionCnt()) + countBits(hfCnt) + valBitCnt, wordsPerPage, storeAccess)
	{

	}

	const std::string &getTypeStr() const {
		static std::string typeStr("paged");
		return typeStr;
	}
	
	

	template<class StoreInfo>
    FORCE_INLINE AppendResult insert(const StoreInfo &si) {
		typename StoreAccess::WordType storeVal = 0;
		storeAccess.set(0, storeVal, si.hfIdx +1);
		storeAccess.set(1, storeVal, si.storeVal);
		storeAccess.set(2, storeVal, si.val);

		return Page_Container<PageAccess>::insert(si.pos, storeVal);
	}

	template<class StoreInfo>
    FORCE_INLINE LookupResult lookup(const StoreInfo &si) const {
		typename StoreAccess::WordType storeVal = 0;
		storeAccess.set(0, storeVal, si.hfIdx +1);
		storeAccess.set(1, storeVal, si.storeVal);

		return Page_Container<PageAccess>::lookup(si.pos, storeVal);
    }

    class AccessSupport : public PageAccess::Pack_AccessSupport {
        const StoreAccess *storeAcc;
    public:
        using Base = typename PageAccess::Pack_AccessSupport;

        explicit inline AccessSupport(const StoreAccess *storeAcc, const Base &baseAccSupp)
                : Base(baseAccSupp)
                , storeAcc(storeAcc) {}

        AccessSupport(const AccessSupport &other)
            : Base(other)
            , storeAcc(other.storeAcc) {}

        explicit AccessSupport(LookupResult res)
            : Base(res) {}

        explicit inline operator WordType() const {
            WordType value = Base::get();
            return storeAcc->get(2, value);
        }

//	inline operator bool() const {
//		return isValid();
//	}

        inline WordType get() const {
#ifdef DEBUG_TESTS
                assert(Base::isValid() && "accessing a non existend value");
#endif

                return operator WordType();
        }

        inline void set(const WordType &val) {
#ifdef DEBUG_TESTS
                assert(Base::isValid() && "accessing a non existend value");
#endif
                WordType storeVal = Base::get();
                storeAcc->set(2, storeVal, val);
                Base::set(storeVal);
        }

//        inline bool operator==(const WordType &other) {
//                return get() == other;
//        }
    };

	template<class StoreInfo>
    FORCE_INLINE auto value(const StoreInfo &si) {
		typename StoreAccess::WordType storeVal = 0;
		storeAccess.set(0, storeVal, si.hfIdx +1);
		storeAccess.set(1, storeVal, si.storeVal);

        return AccessSupport(&storeAccess, Page_Container<PageAccess>::value(si.pos, storeVal));
	}

    template<class ReplaceInfo>
    FORCE_INLINE ReplaceInfo replace(const ReplaceInfo &ri) {
		typename StoreAccess::WordType storeVal = 0;
		storeAccess.set(0, storeVal, ri.hfIdx +1);
		storeAccess.set(1, storeVal, ri.storeVal);
		storeAccess.set(2, storeVal, ri.val);

		ReplaceInfo removed;
		removed.pos = ri.pos;
		removed.offset = ri.offset;

		typename StoreAccess::WordType removedstoreVal = Page_Container<PageAccess>::replace(ri.pos, ri.offset, storeVal);

        removed.hfIdx = storeAccess.get(0, removedstoreVal) -1;
		removed.storeVal = storeAccess.get(1, removedstoreVal);
		removed.val = storeAccess.get(2, removedstoreVal);

		return removed;
	}

	FORCE_INLINE size_t fingerPrintBitCount() const {
		return storeAccess.bitCount(1);
	}

    inline size_t getValueBitCount() const {
        return storeAccess.bitCount(2);
    }

    std::unordered_map<size_t, size_t> getValueCounts() const {
        std::unordered_map<size_t, size_t> valueCounts;
        for (size_t i=0; i<Page_Container<PageAccess>::hashPositionCnt(); ++i) {
            for (size_t j=0; j<pageAccess.elemCapacity(); ++j) {
                auto storeVal = Page_Container<PageAccess>::get(i, j);
                if (storeAccess.get(0, storeVal)) { //the field is not empty
                    auto val = storeAccess.get(2, storeVal);

                    auto it = valueCounts.find(val);
                    if (it == valueCounts.end()) { //the value did not exist in the map valueCounts
                        valueCounts.insert({val, 1});
                    }
                    else {
                        ++it->second;
                    }
                }
                else {
                    break;
                }
            }
        }

        return valueCounts;
    }
};

}
