#pragma once

#include "return_indicators.h"

namespace Runtime {

template<class Page>
class AccessSupport {
protected:
		const LookupResult result;
		const Page *p = nullptr;
		const size_t pos = 0;
		size_t *arr;

        using PageType = Page;

public:
		explicit inline AccessSupport(LookupResult result, size_t *arr = nullptr, const Page *page = nullptr, size_t pos = 0)
				: result(result)
				, arr(arr)
				, p(page)
				, pos(pos) {}

		explicit inline operator LookupResult() const {
				return result;
		}

		explicit inline operator typename Page::StoreT() const {
				return p->get(arr, pos);
		}

		inline bool isValid() const {
				return result == LOOKUP_EXISTS;
		}

//	inline operator bool() const {
//		return isValid();
//	}

		inline typename Page::StoreT get() const {
#ifdef DEBUG_TESTS
				assert(isValid() && "accessing a non existend value");
#endif

				return operator typename Page::StoreT();
		}

		inline LookupResult res() const {
				return result;
		}

		inline void set(const typename Page::StoreT &val) {
#ifdef DEBUG_TESTS
				assert(isValid() && "accessing a non existend value");
#endif
				p->pack.resetAndStore(arr, pos, val);
		}

//        inline bool operator==(const typename Page::StoreT &other) {
//				return get() == other;
//		}
};
}
