#pragma once

#include <thread>
#include <functional>

#include "blocked_ht_base.h"

#include "return_indicators.h"
#include "thread_communication/paged_thread_comm.h"

namespace Runtime {

namespace Hashtable {

template<class Key, class Value, class HT, class HF>
class Blocked : public Blocked_Base<Key, Value, HT, HF> {
    using Base = Blocked_Base<Key, Value, HT, HF>;

public:
    struct InsertInfo {
        Key key;
        Value val;
    };

protected:
    size_t m_pagedComm_pageSize;
    size_t m_pagedComm_pageCount;

    mutable std::vector<std::thread> m_threads;
    std::vector<PagedThreadComm<InsertInfo>*> m_pagedComm_insert;
    mutable std::vector<PagedThreadComm<Key>*> m_pagedComm_lookup;
    std::vector<PagedThreadComm<Key>*> m_pagedComm_value;

public:
    using typename Base::KeyType;
    using typename Base::ValueType;

    using typename Base::SubHT;
    using typename Base::HFType;
    using typename Base::HT_InsertResult;
    using typename Base::HT_LookupResult;

    Blocked(std::vector<HT*> &hashtables, HF &hf, size_t batchCount, size_t batchSize)
        : Base(hashtables, hf)
        , m_pagedComm_pageCount(batchCount)
        , m_pagedComm_pageSize(batchSize)
    {

    }

    ~Blocked() {
        for (auto p : m_pagedComm_insert) {
            delete p;
        }
        for (auto p : m_pagedComm_lookup) {
            delete p;
        }
        for (auto p : m_pagedComm_value) {
            delete p;
        }
    }

    template<class InsertAction>
    inline auto insert(const KeyType &key, const ValueType &val, InsertAction &) {
#ifdef DEBUG_COMPILE
        assert(m_threads.size() == Base::hashtables.size() && "thread cound is not sufficient. Call initInsert() before insert().");
        assert(m_pagedComm_insert.size() == Base::hashtables.size() && "m_pagedComm_insert does not contain the needed elements. Call initInsert() before insert().");
#endif

        auto hi = Base::hf.hash(key, Base::hashtables.size());
        m_pagedComm_insert[hi.pos]->push(InsertInfo{hi.storeVal, val});
    }

    template<class InsertAction>
    inline auto insert(const KeyType &key, InsertAction &insertAction) {
        return insert(key, Value(), insertAction);
    }

    template<class LookupAction>
    inline auto lookup(const KeyType &key, LookupAction &) const {
        auto hi = Base::hf.hash(key, Base::hashtables.size());
        m_pagedComm_lookup[hi.pos]->push(hi.storeVal);
    }

    template<class ValueAction>
    inline void value(const Key &key, ValueAction &) {
#ifdef DEBUG_COMPILE
        assert(m_threads.size() == Base::hashtables.size() && "thread cound is not sufficient. Call initInsert() before insert().");
        assert(m_pagedComm_value.size() == Base::hashtables.size() && "m_pagedComm_insert does not contain the needed elements. Call initInsert() before insert().");
#endif

        auto hi = Base::hf.hash(key, Base::hashtables.size());
        m_pagedComm_value[hi.pos]->push(hi.storeVal);
    }

    template<class InsertActor>
    void initInsert(InsertActor &insertAct) {
        //		 assert(m_pagedComm_insert.empty() && "expect m_pagedComm_insert to be empty when initializing");
        assert(m_threads.empty() && "expect m_pagedComm_insert to be empty when initializing");

        //		 m_pagedComm_insert.reserve(hashtables.size());
        m_threads.reserve(Base::hashtables.size());

        for (size_t i=m_pagedComm_insert.size(); i<Base::hashtables.size(); ++i) {
            m_pagedComm_insert.push_back(new PagedThreadComm<InsertInfo>(m_pagedComm_pageCount, m_pagedComm_pageSize));
        }

        auto insertWorker = [&insertAct] (HT *ht, PagedThreadComm<InsertInfo> *comm) {
            auto threadInsertActor = insertAct.detachParallel();
            ht->initInsert(threadInsertActor);
            while (comm->waitForRead()) {
                auto insertInfo = comm->pop();
                ht->insert(insertInfo.key, insertInfo.val, threadInsertActor);
            }
            ht->finishInsert(threadInsertActor);
            insertAct.join(threadInsertActor);
        };

        for (size_t i=0; i<Base::hashtables.size(); ++i) {
            m_threads.push_back(std::thread(insertWorker, Base::hashtables[i], m_pagedComm_insert[i]));
        }
    }

    template<class LookupActor>
    void initLookup(LookupActor &lookupAct) const {
        assert(m_threads.empty() && "expect m_pagedComm_insert to be empty when initializing");

        m_pagedComm_lookup.reserve(Base::hashtables.size());
        m_threads.reserve(Base::hashtables.size());

        for (size_t i=m_pagedComm_lookup.size(); i<Base::hashtables.size(); ++i) {
            m_pagedComm_lookup.push_back(new PagedThreadComm<Key>(m_pagedComm_pageCount, m_pagedComm_pageSize));
        }

        auto lookupWorker = [&lookupAct] (HT *ht, PagedThreadComm<Key> *comm) {
            auto threadLookupActor = lookupAct.detachParallel();
            ht->initLookup(threadLookupActor);
            while (comm->waitForRead()) {
                auto key = comm->pop();
                ht->lookup(key, threadLookupActor);
            }
            ht->finishLookup(threadLookupActor);
            lookupAct.join(threadLookupActor);
        };

        for (size_t i=0; i<Base::hashtables.size(); ++i) {
            m_threads.push_back(std::thread(lookupWorker , Base::hashtables[i], m_pagedComm_lookup[i]));
        }
    }

    template<class ValueActor>
    void initValue(ValueActor &valueAct) {
        assert(m_threads.empty() && "expect m_pagedComm_insert to be empty when initializing");

        m_pagedComm_value.reserve(Base::hashtables.size());
        m_threads.reserve(Base::hashtables.size());

        for (size_t i=m_pagedComm_value.size(); i<Base::hashtables.size(); ++i) {
            m_pagedComm_value.push_back(new PagedThreadComm<Key>(m_pagedComm_pageCount, m_pagedComm_pageSize));
        }

        auto valueWorker = [&valueAct] (HT *ht, PagedThreadComm<Key> *comm) {
            auto threadValueActor = valueAct.detachParallel();
            ht->initValue(threadValueActor);
            while (comm->waitForRead()) {
                    auto key = comm->pop();
                    ht->value(key, threadValueActor);
            }
            ht->finishValue(threadValueActor);
            valueAct.join(threadValueActor);
        };

        for (size_t i=0; i<Base::hashtables.size(); ++i) {
            m_threads.push_back(std::thread(valueWorker , Base::hashtables[i], m_pagedComm_value[i]));
        }
    }

    template<class ValueActor>
    void finishValue(const ValueActor &) {
        for (auto &comm : m_pagedComm_value) {
            comm->setEOF();
        }

        for (auto &thread : m_threads) {
            thread.join();
        }
        m_threads.clear();
        for (auto &comm : m_pagedComm_value) {
            delete comm;
        }
        m_pagedComm_value.clear();
    }

    template<class InsertActor>
    void finishInsert(InsertActor &) {
        for (auto &comm : m_pagedComm_insert) {
            comm->setEOF();
        }

        for (auto &thread : m_threads) {
            thread.join();
        }
        m_threads.clear();
        for (auto &comm : m_pagedComm_insert) {
            delete comm;
        }
        m_pagedComm_insert.clear();
    }

    template<class LookupActor>
    void finishLookup(LookupActor &) const {
        for (auto &comm : m_pagedComm_lookup) {
            comm->setEOF();
        }

        for (auto &thread : m_threads) {
            thread.join();
        }
        m_threads.clear();
        for (auto &comm : m_pagedComm_lookup) {
            delete comm;
        }
        m_pagedComm_lookup.clear();
    }



    template<class Actor>
    class LookupAccessor {
        Blocked *blockedHT = nullptr;
        mutable std::vector<std::thread> threads;

        struct LookupInfo {
            Key key;
            Actor act;
        };
        std::vector<PagedThreadComm<LookupInfo>*> pagedComm_lookup;

        public:
        LookupAccessor(Blocked *blockedHT)
            : blockedHT(blockedHT)
        {
            pagedComm_lookup.reserve(blockedHT->hashtables.size());
            threads.reserve(blockedHT->hashtables.size());

            for (size_t i=pagedComm_lookup.size(); i<blockedHT->getSubHTs().size(); ++i) {
                pagedComm_lookup.push_back(
                            new PagedThreadComm<LookupInfo>(blockedHT->m_pagedComm_pageCount, blockedHT->m_pagedComm_pageSize));
            }

            auto valueWorker = [] (HT *ht, PagedThreadComm<Key> *comm) {
                auto subLookupAcc = ht->getLookupAccessor();
                while (comm->waitForRead()) {
                    auto lookupInfo = comm->pop();
                    subLookupAcc.lookup(lookupInfo.key, lookupInfo.act);
                }
            };

            for (size_t i=0; i<Base::hashtables.size(); ++i) {
                threads.push_back(std::thread(valueWorker , blockedHT->hashtables[i], pagedComm_lookup[i]));
            }
        }

        ~LookupAccessor() {
            for (auto &comm : pagedComm_lookup) {
                comm->setEOF();
            }

            for (auto &thread : threads) {
                thread.join();
            }
            threads.clear();
            for (auto &comm : pagedComm_lookup) {
                delete comm;
            }
            pagedComm_lookup.clear();
        }

        FORCE_INLINE void lookup(const typename HT::KeyType & key, const Actor &lookupAct) {
            auto hi = blockedHT->hashInfo(key);
            pagedComm_lookup[hi.pos]->push({hi.storeVal, lookupAct});
        }
    };

    template<class Actor>
    LookupAccessor<Actor> getLookupAccessor() const {
        return LookupAccessor<Actor>(this);
    }


    template<class Actor>
    class ValueAccessor {
        Blocked *blockedHT = nullptr;
        mutable std::vector<std::thread> threads;

        struct LookupInfo {
            Key key;
            Actor act;
        };
        std::vector<PagedThreadComm<LookupInfo>*> pagedComm_value;

        public:
        ValueAccessor(Blocked *blockedHT)
            : blockedHT(blockedHT)
        {
            pagedComm_value.reserve(blockedHT->hashtables.size());
//            threads.reserve(blockedHT.hashtables.size());

            for (size_t i=pagedComm_value.size(); i<blockedHT->getSubHTs().size(); ++i) {
                pagedComm_value.push_back(
                            new PagedThreadComm<LookupInfo>(blockedHT->m_pagedComm_pageCount, blockedHT->m_pagedComm_pageSize));
            }

            auto valueWorker = [] (HT *ht, PagedThreadComm<LookupInfo> *comm) {
                auto subLookupAcc = ht->template getValueAccessor<Actor>();
                while (comm->waitForRead()) {
                        auto lookupInfo = comm->pop();
                        subLookupAcc.value(lookupInfo.key, lookupInfo.act);
                }
            };

            for (size_t i=0; i<blockedHT->getSubHTs().size(); ++i) {
                threads.push_back(std::move(std::thread(valueWorker , blockedHT->hashtables[i], pagedComm_value[i])));
            }
        }

        ValueAccessor(ValueAccessor &&other)
            : threads(std::move(other.threads))
            , pagedComm_value(std::move(other.pagedComm_value))
        {
            std::swap(blockedHT, other.blockedHT);
        }

        ~ValueAccessor() {
            for (auto &comm : pagedComm_value) {
                comm->setEOF();
            }

            for (auto &thread : threads) {
                thread.join();
            }
            threads.clear();
            for (auto &comm : pagedComm_value) {
                delete comm;
            }
            pagedComm_value.clear();
        }

        FORCE_INLINE void value(const typename HT::KeyType & key, const Actor &lookupAct) {
            auto hi = blockedHT->hashInfo(key);
            pagedComm_value[hi.pos]->push({hi.storeVal, lookupAct});
        }
    };

    template<class Actor>
    ValueAccessor<Actor> getValueAccessor() {
        return ValueAccessor<Actor>(this);
    }

    inline size_t pagedComm_batchCount() const    {
        return m_pagedComm_pageCount;
    }
    inline size_t pagedComm_batchSize() const {
        return m_pagedComm_pageSize;
    }
};
}
}
