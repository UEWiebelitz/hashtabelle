#pragma once

#include "return_indicators.h"

#include "template/counters/bitcounter.h"
#include "runtime/bitshifting/combinedwords.h"
#include "runtime/bitshifting/wordpacking.h"
#include "runtime/page_layouts/packed_null.h"
#include "hashtable_base.h"

#include "compile_hints/likely_hint.h"

#ifdef PREFETCH
#include "circular_buffer.h"
#endif

#include <unordered_map>
#include <cassert>
#include <vector>
#include <unistd.h>

namespace Runtime {

template<class Key, class Value, class HF/*, size_t hfCount*/>
class HashTable_Delimited_linear : public Hashtable_base<Key, Value> {
	/**
	 * @brief The KeyStoreType class is a word packing container that encodes the used hashfunction, the key and the value.
	 * The compare-operator is overloaded and does only use the hashfunktion and the key. The value is not considered on comparison.
	 */
	class KeyStoreType : public CombinedWords<3> {
	protected:
		size_t firstTwoMask;
	public:
		inline KeyStoreType(const std::array<size_t, 3> &bitCounts) : CombinedWords(bitCounts) {
			firstTwoMask = m_maskFirstWord[0] | m_maskFirstWord[1];
		}

		using CombinedWords::CombinedWords;

		inline bool equals(const WordType arr1, const WordType arr2) const {
			return (arr1 & firstTwoMask) == (arr2 & firstTwoMask);
//			return CombinedWords::get(0, arr1) == CombinedWords::get(0, arr2) && CombinedWords::get(1, arr1) == CombinedWords::get(1, arr2);
		}
	};

	HF m_hashfunction;
//	size_t m_pageCount;
	size_t m_linProbingRange;
	KeyStoreType store;
	size_t m_valueBitCount;

	struct CompareStores {
		KeyStoreType &store;

		inline bool equals(size_t val1, size_t val2) const {
			return store.equals(val1, val2);
		}
	};

	typedef size_t WordType;

//	typedef PageType<WordPacking, CompareStores> PageT;
	WordPacking m_arr;

	size_t elemCountOf(size_t val, const HF &hf) const {
		KeyStoreType kst(std::array<size_t, 3> ({countBits(m_linProbingRange), hf.fpBitCount(val), m_valueBitCount}));
		return sizeof(WordType) * 8 * val / kst.bitCount();
//		WordPacking p(val, kst.bitCount());
//		return p.elemCapacity();
	}

	size_t binSizeSearch(size_t elemCount, const HF &hf) const {
		size_t val = 1;
		while (elemCountOf(val, hf) < elemCount) {
			val *=2;
		}

		size_t lower = val/2;
		size_t upper = val;
		while (lower < upper) {
			size_t mid = (upper + lower) /2;
			size_t midElemCount = elemCountOf(mid, hf);

			if (midElemCount < elemCount) {
				lower = mid +1;
			}
			else {
				upper = mid;
			}
		}

		return upper;
	}

public:
//	static constexpr size_t pWordCount = pageWordCount;
//	static constexpr size_t pCount = pageCount;
	typedef  Key KeyType;
	typedef  Value ValueType;

	HashTable_Delimited_linear(size_t valueBitCount, HF &hf, size_t elemCount, size_t linProbingRange)
		: m_hashfunction(hf)
		, m_linProbingRange(linProbingRange)
		, store(std::array<size_t, 3> ({countBits(m_linProbingRange), m_hashfunction.fpBitCount(elemCount), valueBitCount})) ///@todo the fingerprint size might be wrong this way!
		, m_valueBitCount(valueBitCount)
		, m_arr(binSizeSearch(elemCount, m_hashfunction), store.bitCount())

	{
	}

//	~Hashtable::Replacing() {
//		free(pages);
//	}

	typedef InsertResult<Key, Value> HT_InsertResult;

#ifdef PREFETCH
//	inline void prefetch(const KeyType &key) {
//		size_t pos = m_hashfunction.hashPos(key, m_arr.elemCapacity());
//		m_arr.prefetch(pos);
//	}

	struct PrefetchInfo {
		KeyType key;
		typename HF::StoreInfo storeInfo;
	};

	inline PrefetchInfo prefetch(const KeyType &key) {
		PrefetchInfo pI{key, m_hashfunction.hash(key, m_arr.elemCapacity())};
		m_arr.prefetch(pI.storeInfo.pos);

		return pI;
	}

	inline HT_InsertResult insert(const PrefetchInfo &pI, const Value &val = Value()) {
#ifdef DEBUG_COMPILE
		if ((val & (1 << m_valueBitCount)) != val) {
			throw std::invalid_argument("given val overflows valueBitCount!");
		}
#endif

		return HT_InsertResult{insert_hf(pI.storeInfo, val), pI.key, val};
	}

	inline LookupResult lookup(const PrefetchInfo &prefetch) {
		const auto &si = prefetch.storeInfo;

		WordType storeVal = 0;
		store.set(1, storeVal, si.storeVal);

		for (size_t i=0; i<m_linProbingRange; ++i) {
			store.set(0, storeVal, i+1);
			const size_t m_arrEntry = m_arr[(si.pos + i) % m_arr.elemCapacity()];
			if (unlikely(store.equals(storeVal, m_arrEntry))) {
				return LOOKUP_EXISTS;
			}
			else if (likely(m_arrEntry == 0)) {
				return LOOKUP_NOT_EXISTS;
			}
		}

		return LOOKUP_UNSURE;
	}
#endif

	inline HT_InsertResult insert(const KeyType &key, const Value &val = Value()) {
		return HT_InsertResult{insert_hf(m_hashfunction.hash(key, m_arr.elemCapacity()), val), key, val};
	}

	inline LookupResult lookup(const Key &key) {
		auto si = m_hashfunction.hash(key, m_arr.elemCapacity());

		WordType storeVal = 0;
		store.set(1, storeVal, si.storeVal);

		for (size_t i=0; i<m_linProbingRange; ++i) {
			store.set(0, storeVal, i+1);
			const size_t m_arrEntry = m_arr[(si.pos + i) % m_arr.elemCapacity()];
			if (unlikely(store.equals(storeVal, m_arrEntry))) {
				return LOOKUP_EXISTS;
			}
			else if (likely(m_arrEntry == 0)) {
				return LOOKUP_NOT_EXISTS;
			}
		}

		return LOOKUP_UNSURE;
	}

	auto value(const KeyType &key) {
		auto si = m_hashfunction.hash(key, m_arr.elemCapacity());

		WordType storeVal = 0;
		store.set(1, storeVal, si.storeVal);

		for (size_t i=0; i<m_linProbingRange; ++i) {
			store.set(0, storeVal, i+1);
			const size_t m_arrEntry = m_arr[(si.pos + i) % m_arr.elemCapacity()];
			if (unlikely(store.equals(storeVal, m_arrEntry))) {
				return AccessSupport(LOOKUP_EXISTS, &store, &m_arr, si.pos +i);
			}
			else if (likely(m_arrEntry == 0)) {
				return AccessSupport(LOOKUP_NOT_EXISTS);
			}
		}

		return AccessSupport(LOOKUP_UNSURE);
	}

	inline size_t capacity() const {
		return m_arr.elemCapacity();
	}

	inline size_t pageCount() const {
		return 1;
	}

	inline size_t pageCapacity() const {
		return 0;
	}

	inline size_t wordsPerPage() const {
		return 0;
	}

	inline size_t memory() const {
		return m_arr.bitCount() / 8;
	}

	inline size_t hfCount() const {
		return 1;
	}

	inline size_t fingerPrintBitCount() const {
		return m_hashfunction.fpBitCount(m_arr.elemCapacity());
	}

	inline size_t storeBitCount() const {
		return store.bitCount();
	}

	inline size_t walkLength() const {
		return 1;
	}

#ifdef TRACE_HASH_ACCESS
	inline const std::vector<size_t> &getHashAccess_insert() const {return hash_access_insert;}
	inline const std::vector<size_t> &getHashAccess_Lookup() const {return hash_access_lookup;}
#endif

#ifdef COUNT_ELEMS
	inline size_t elemCount() const {return elems;}
#endif

	std::unordered_map<Value, size_t> getValueCounts() const {
		std::unordered_map<Value, size_t> valueCounts;

		for (int i=0; i<m_arr.elemCapacity(); ++i) {
			auto elem = m_arr[i];
			if (elem == 0) {
				continue;
			}
			auto value = store.get(2, elem);
			auto it = valueCounts.find(value);
			if (unlikely(it == valueCounts.end())) {
				valueCounts.insert({value, 1});
			}
			else {
				++it->second;
			}
		}

		return valueCounts;
	}

protected:
#ifdef COUNT_ELEMS
	size_t elems = 0;
#endif

#ifdef TRACE_HASH_ACCESS
	mutable std::vector<size_t> hash_access_insert;
	mutable std::vector<size_t> hash_access_lookup;
#endif

	inline AppendResult insert_hf(const typename HF::StoreInfo &storeInfo, const Value &val) {
#ifdef DEBUG_COMPILE
		if ((val & ((1 << m_valueBitCount) -1)) != val) {
			throw std::invalid_argument("the given val overflows value bitcount!");
		}
#endif

		WordType storeVal = 0;
//		store.set(0, storeVal, 0);
		store.set(1, storeVal, storeInfo.storeVal);
		store.set(2, storeVal, val);

		for (size_t i=0; i<m_linProbingRange; ++i) {
			store.set(0, storeVal, i+1);
			size_t pos = (storeInfo.pos + i) % m_arr.elemCapacity();
			WordType m_arrEntry = m_arr[pos];
			if (unlikely(store.equals(storeVal, m_arrEntry))) {
				return APPEND_EXISTS;
			}
			else if (likely(m_arrEntry == 0)) {
				m_arr.store(pos, storeVal);
#ifdef COUNT_ELEMS
				++elems;
#endif
				return APPEND_APPENDED;
			}
		}

		return APPEND_FULL;
	}

public:
	class AccessSupport {
		LookupResult m_res;
		WordPacking *m_arr;
		size_t m_pos;
		const KeyStoreType *m_store;

	public:
		AccessSupport(LookupResult res, const KeyStoreType *store = nullptr, WordPacking *arr = nullptr, size_t pos = 0)
			: m_res(res)
			, m_arr(arr)
			, m_store(store)
			, m_pos(pos)
		{}

		inline operator LookupResult() const {
			return res();
		}

		inline LookupResult res() const {
				return m_res;
		}

		inline Value get() const {
			return Value(m_store->get(2, m_arr->get(m_pos)));
		}

		inline operator Value() const {
			return get();
		}

//		inline operator Value() {
//			return get();
//		}

		inline void set(const Value &val) {
			size_t storeVal = m_arr->get(m_pos);
			m_store->set(2, storeVal, val);
			m_arr->resetAndStore(m_pos, storeVal);
		}

		inline void operator=(const Value &val) {
			set(val);
		}
	};
};
}

