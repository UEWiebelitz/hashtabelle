#ifdef PREFETCH
#pragma once

#include <type_traits>
#include <functional>

#include "circular_buffer.h"
#include "inline_enforce.h"
#include "return_indicators.h"

namespace Runtime {
namespace Hashtable {

template<class HT>
class PrefetchAhead
{
	HT &ht;
	std::size_t prefetchCnt;

#ifdef PREFETCH
	using HT_PrefetchResult = decltype (ht.prefetch(std::declval<typename HT::KeyType>()));
#else
    using HT_PrefetchResult = typename HT::KeyType;
#endif

	struct PrefetchInfo {
		HT_PrefetchResult keyPrefetch;
		typename HT::ValueType val;
	};

	CircularBuffer<PrefetchInfo> insertBuffer;
	mutable CircularBuffer<HT_PrefetchResult> lookupBuffer;
    CircularBuffer<HT_PrefetchResult> valueBuffer;

public:
	using BaseHT = HT;

    using HT_InsertResult = decltype (ht.insert(std::declval<HT_PrefetchResult>()));
    using HT_LookupResult = decltype (ht.lookup(std::declval<HT_PrefetchResult>()));
    using HT_ValueResult = typename HT::HT_ValueResult;//decltype (ht.value(std::declval<HT_PrefetchResult>()));
//    using ValueActor = std::function<void(HT_ValueResult)>;

public:
	using KeyType = typename HT::KeyType;
	using ValueType = typename HT::ValueType;

    PrefetchAhead(HT &ht, std::size_t prefetchCnt)
		: ht(ht)
		, prefetchCnt(prefetchCnt)
		, insertBuffer(prefetchCnt +1) //oversize the buffer to be able to store one extra value before one element is consumed
        , lookupBuffer(prefetchCnt +1)
        , valueBuffer(prefetchCnt +1)
	{
	}
	
	HT &baseHT() {
		return ht;
	}

	const HT &baseHT() const {
		return ht;
	}

    template<class InsertActor>
    FORCE_INLINE void insert(const typename HT::KeyType & key, const typename HT::ValueType &val, InsertActor &insertAct) {
		insertBuffer.push({ht.prefetch(key), val});
		if (insertBuffer.elemCount() > prefetchCnt) {
			auto insertInfo = insertBuffer.pop();
            ht.insert(insertInfo.keyPrefetch, insertInfo.val, insertAct);
		}
	}

    template<class InsertActor>
    FORCE_INLINE void insert(const typename HT::KeyType & key, InsertActor &insertAct) {
        insertBuffer.push({ht.prefetch(key)});
        if (insertBuffer.elemCount() > prefetchCnt) {
            auto insertInfo = insertBuffer.pop();
            ht.insert(insertInfo.keyPrefetch, insertAct);
        }
    }

    template<class LookupActor>
    FORCE_INLINE void lookup(const typename HT::KeyType & key, LookupActor &lookupAct) const {
		lookupBuffer.push(ht.prefetch(key));
		if (lookupBuffer.elemCount() > prefetchCnt) {
            ht.lookup(lookupBuffer.pop(), lookupAct);
		}
	}

    FORCE_INLINE auto directValue(const typename HT::KeyType & key) {
        return ht.directValue(key);
    }

    template<class ValueActor>
    FORCE_INLINE void value(const typename HT::KeyType & key, ValueActor &valueAct) {
        valueBuffer.push(ht.prefetch(key));
        if (valueBuffer.elemCount() > prefetchCnt) {
//            ht.insert(valueBuffer.pop());
            ht.value(valueBuffer.pop(), valueAct);
        }
    }

    template<class InsertActor>
    FORCE_INLINE void finishInsert(InsertActor &insertAct) {
		while (insertBuffer.elemCount()) {
			auto insertInfo = insertBuffer.pop();
            ht.insert(insertInfo.keyPrefetch, insertInfo.val, insertAct);
		}
        ht.finishInsert(insertAct);
	}

    template<class LookupActor>
    FORCE_INLINE auto finishLookup(LookupActor &lookupAct) const {
		while (lookupBuffer.elemCount()) {
            ht.lookup(lookupBuffer.pop(), lookupAct);
		}
        ht.finishLookup(lookupAct);
	}

    template<class ValueActor>
    FORCE_INLINE void finishValue(ValueActor &valueAct) {
        while (valueBuffer.elemCount()) {
            ht.value(valueBuffer.pop(), valueAct);
        }
        ht.finishValue(valueAct);
    }

    template<class InsertActor>
    void initInsert(const InsertActor &)
	{
	}

    template<class LookupActor>
    void initLookup(const LookupActor &) const
    {
	}

    template<class ValueActor>
    void initValue(const ValueActor &) const
    {
    }

	size_t prefetchAhead() const {
		return prefetchCnt;
	}

    size_t elemCount() const {
        return ht.elemCount();
    }

    size_t capacity() const {
        return ht.capacity();
    }

    std::unordered_map<typename HT::ValueType, size_t> getValueCounts() const {
        return ht.getValueCounts();
    }



    template<class Actor>
    class LookupAccessor {
        using Base_LookupAccessor = typename BaseHT::template LookupAccessor<Actor>;
        Base_LookupAccessor baseLookupAcc;
        const PrefetchAhead *ht = nullptr;

        struct LookupInfo {
            HT_PrefetchResult prefetch;
            Actor act;
        };

        CircularBuffer<LookupInfo> lookupBuffer;

        public:
        LookupAccessor(const PrefetchAhead *ht)
            : lookupBuffer(ht->prefetchCnt +1)
            , baseLookupAcc(ht->ht.template getLookupAccessor<Actor>())
            , ht(ht)
        {}

        LookupAccessor(LookupAccessor &&other)
            : baseLookupAcc(std::move(other.baseLookupAcc))
            , lookupBuffer(std::move(other.lookupBuffer))
        {
            std::swap(ht, other.ht);
        }
//        LookupAccessor(const LookupAccessor &other) = default;

        ~LookupAccessor() {
            while (lookupBuffer.elemCount()) {
                auto lookupInfo = lookupBuffer.pop();
                baseLookupAcc.lookup(lookupInfo.prefetch, lookupInfo.act);
            }
        }

        FORCE_INLINE void lookup(const typename HT::KeyType & key, const Actor &lookupAct) {
            lookupBuffer.push({ht->ht.prefetch(key), lookupAct});
            if (lookupBuffer.elemCount() == ht->prefetchCnt +1) {
                auto lookupInfo = lookupBuffer.pop();
                baseLookupAcc.lookup(lookupInfo.prefetch, lookupInfo.act);
            }
        }
    };

    template<class Actor>
    LookupAccessor<Actor> getLookupAccessor() const {
        return LookupAccessor<Actor>(this);
    }


    template<class Actor>
    class ValueAccessor {
        using Base_ValueAccessor = typename BaseHT::template ValueAccessor<Actor>;
        Base_ValueAccessor baseValueAcc;
        PrefetchAhead *ht = nullptr;

        struct LookupInfo {
            HT_PrefetchResult prefetch;
            Actor act;
        };

        CircularBuffer<LookupInfo> buffer;

        public:
        ValueAccessor(PrefetchAhead *ht)
            : buffer(ht->prefetchCnt +1)
            , baseValueAcc(ht->ht.template getValueAccessor<Actor>())
            , ht(ht)
        {}

        ValueAccessor(ValueAccessor &&other)
            : baseValueAcc(std::move(other.baseValueAcc))
            , ht(std::move(other.ht))
            , buffer(std::move(other.buffer))
        {}
//        ValueAccessor(const ValueAccessor &other) = default;

        ~ValueAccessor() {
            while (buffer.elemCount()) {
                auto lookupInfo = buffer.pop();
                baseValueAcc.value(lookupInfo.prefetch, lookupInfo.act);
            }
        }

        FORCE_INLINE void value(const typename HT::KeyType & key, const Actor &act) {
            buffer.push({ht->ht.prefetch(key), act});
            if (buffer.elemCount() == ht->prefetchCnt +1) {
                auto lookupInfo = buffer.pop();
                baseValueAcc.value(lookupInfo.prefetch, lookupInfo.act);
            }
        }
    };

    template<class Actor>
    ValueAccessor<Actor> getValueAccessor() {
        return ValueAccessor<Actor>(this);
    }
};

}
}
#endif
