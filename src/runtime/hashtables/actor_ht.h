#pragma once

#include "compile_hints/inline_enforce.h"

#include <utility>

template<class HT>
class Actor_HT : public HT{
#ifdef PREFETCH
    using PrefetchResult = decltype (std::declval<HT>().prefetch(std::declval<typename HT::KeyType>()));
#else
    using PrefetchResult = typename HT::KeyType;
#endif

public:
    using HT::HT;

    using HT::insert;
    using HT::lookup;
    using HT::value;

    using SubHT = HT;
    using KeyType = typename HT::KeyType;
    using ValueType = typename HT::ValueType;

    using HT_InsertResult = typename HT::HT_InsertResult;
    using HT_LookupResult = typename HT::HT_LookupResult;
    using HT_ValueResult = typename HT::HT_ValueResult;



    template<class InsertActor>
    FORCE_INLINE void insert(const KeyType &key, const ValueType &val, InsertActor &insAct) {
        insAct.act(HT::insert(key, val));
    }

    template<class InsertActor>
    FORCE_INLINE void insert(const KeyType &key, InsertActor &insAct) {
        insAct.act(HT::insert(key));
    }

    template<class InsertActor, class Prefetch, typename std::enable_if<std::is_same<Prefetch, PrefetchResult>::value && !std::is_same<PrefetchResult, KeyType>::value, int>::type = 0>
    FORCE_INLINE void insert(const Prefetch &key, const ValueType &val, InsertActor &insAct) {
        insAct.act(HT::insert(key, val));
    }

    template<class InsertActor, class Prefetch, typename std::enable_if<std::is_same<Prefetch, PrefetchResult>::value && !std::is_same<PrefetchResult, KeyType>::value, int>::type = 0>
    FORCE_INLINE void insert(const Prefetch &key, InsertActor &insAct) {
        insAct.act(HT::insert(key));
    }

    template<class LookupActor, class Prefetch, typename std::enable_if<std::is_same<Prefetch, PrefetchResult>::value && !std::is_same<PrefetchResult, KeyType>::value, int>::type = 0>
    FORCE_INLINE void lookup(const Prefetch &key, LookupActor &lookupAct) const {
        lookupAct.act(HT::lookup(key));
    }

    template<class LookupActor>
    FORCE_INLINE void lookup(const KeyType &key, LookupActor &lookupAct) const {
        lookupAct.act(HT::lookup(key));
    }

    template<class Prefetch, typename std::enable_if<std::is_same<Prefetch, PrefetchResult>::value && !std::is_same<PrefetchResult, KeyType>::value, int>::type = 0>
    FORCE_INLINE auto directValue(const Prefetch &key) {
        return HT::value(key);
    }

    FORCE_INLINE auto directValue(const KeyType &key) {
        return HT::value(key);
    }

    template<class ValueActor, class Prefetch, typename std::enable_if<std::is_same<Prefetch, PrefetchResult>::value && !std::is_same<PrefetchResult, KeyType>::value, int>::type = 0>
    FORCE_INLINE void value(const Prefetch &key, ValueActor &valueAct) {
        valueAct.act(*this, key.key, HT::value(key));
    }

    template<class ValueActor>
    FORCE_INLINE void value(const KeyType &key, ValueActor &valueAct) {
        valueAct.act(*this, key, HT::value(key));
    }

    template<class InsertActor>
    FORCE_INLINE void initInsert(InsertActor&) {}

    template<class LookupActor>
    FORCE_INLINE void initLookup(LookupActor&) const {}

    template<class ValueActor>
    FORCE_INLINE void initValue(ValueActor&) {}

    template<class InsertActor>
    FORCE_INLINE void finishInsert(InsertActor&) {}

    template<class LookupActor>
    FORCE_INLINE void finishLookup(LookupActor&) const {}

    template<class ValueActor>
    FORCE_INLINE void finishValue(ValueActor&) {}


    template<class Actor>
    class LookupAccessor {
        const HT &ht;

        public:
        LookupAccessor(const HT &ht)
            : ht(ht) {}

        ~LookupAccessor() {}

        FORCE_INLINE void lookup(const typename HT::KeyType & key, Actor &lookupAct) const {
            lookupAct.act(ht.lookup(key));
        }

        template<class Prefetch, typename std::enable_if<std::is_same<Prefetch, PrefetchResult>::value && !std::is_same<PrefetchResult, KeyType>::value, int>::type = 0>
        FORCE_INLINE void lookup(const Prefetch &key, Actor &lookupAct) const {
            lookupAct.act(ht.lookup(key));
        }
    };

    template<class Actor>
    LookupAccessor<Actor> getLookupAccessor() const {
        return LookupAccessor<Actor>(*this);
    }


    template<class Actor>
    class ValueAccessor {
        HT &ht;

        public:
        ValueAccessor(HT &ht)
            : ht(ht) {}

        ~ValueAccessor() {}

        FORCE_INLINE void value(const typename HT::KeyType & key, Actor &valueAct) {
            valueAct.act(ht.value(key));
        }

        template<class Prefetch, typename std::enable_if<std::is_same<Prefetch, PrefetchResult>::value && !std::is_same<PrefetchResult, KeyType>::value, int>::type = 0>
        FORCE_INLINE void value(const Prefetch &key, Actor &valueAct) const {
            valueAct.act(ht.value(key));
        }
    };

    template<class Actor>
    ValueAccessor<Actor> getValueAccessor() {
        return ValueAccessor<Actor>(*this);
    }
};
