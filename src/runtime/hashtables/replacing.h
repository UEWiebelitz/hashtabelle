#pragma once

#include "return_indicators.h"

#include "hashtable_base.h"
#include "template/counters/bitcounter.h"
#include "runtime/bitshifting/combinedwords.h"
#include "runtime/page_layouts/packed_null.h"
#include "runtime/page_layouts/page_container.h"

#include "compile_hints/likely_hint.h"

#ifdef MANUAL_RAND
#include "random_unequally_dist.h"
#endif

#include <cassert>
#include <vector>
#include <unistd.h>
#include <unordered_map>
#include <random>

#define RANDOM_GENERATOR std::mt19937_64

namespace Runtime {

namespace Hashtable {

template<class Key, class Value, class HF, class DataContainer>
class Replacing : public Hashtable_base<Key, Value> {

    RANDOM_GENERATOR rand_gen;

#ifdef MANUAL_RAND
    Random::UnequallyDist<decltype (rand_gen)> uneqDist;
#endif

	std::vector<HF> m_hashfunctions;

	using KeyStoreType = typename HF::KeyStoreType;

	DataContainer &dataContainer;

    const size_t m_hfCnt;

    struct StoreInfo : HF::StoreInfo {
        FORCE_INLINE StoreInfo(const typename HF::StoreInfo &baseSi, size_t hfIdx)
            : HF::StoreInfo(baseSi)
            , hfIdx(hfIdx) {}
        StoreInfo() = default;

		size_t hfIdx;
	};

	struct InsertStoreInfo : StoreInfo {
        FORCE_INLINE InsertStoreInfo(const StoreInfo &baseSi, const Value &val)
            : StoreInfo(baseSi)
            , val(val) {}
        FORCE_INLINE InsertStoreInfo(const typename HF::StoreInfo &baseSi, size_t hfIdx, const Value &val)
            : StoreInfo(baseSi, hfIdx)
            , val(val) {}
        InsertStoreInfo() = default;

		Value val;
	};

	struct ReplaceStoreInfo : InsertStoreInfo {
        FORCE_INLINE ReplaceStoreInfo(const InsertStoreInfo &baseSi, size_t offset)
            : InsertStoreInfo(baseSi)
            , offset(offset) {}
        FORCE_INLINE ReplaceStoreInfo(const typename HF::StoreInfo &baseSi, size_t hfIdx, const Value &val, size_t offset)
            : InsertStoreInfo(baseSi, hfIdx, val)
            , offset(offset) {}
//        FORCE_INLINE ReplaceStoreInfo(const StoreInfo &baseSi, const Value &val, size_t offset)
//            : InsertStoreInfo(baseSi, val)
//            , offset(offset) {}

        ReplaceStoreInfo() = default;

		size_t offset;
	};

public:
	typedef  Key KeyType;
	typedef  Value ValueType;

    using HT_LookupResult = LookupResult;

	Replacing() = delete;
	Replacing(const Replacing&) = delete;
#ifdef MANUAL_RAND
    Replacing(const std::vector<HF> &hfs, DataContainer &dataContainer, const std::vector<size_t> &replacingDist, size_t maxIterations = 10000, size_t randSeed = std::random_device()())
#else
    Replacing(const std::vector<HF> &hfs, DataContainer &dataContainer, size_t maxIterations = 10000, size_t randSeed = std::random_device()())
#endif
        : rand_gen(randSeed)
    #ifdef MANUAL_RAND
        , uneqDist(replacingDist, rand_gen)
    #endif
        , m_hashfunctions(hfs)
		, dataContainer(dataContainer)
		, maxIterations(maxIterations)
        , m_hfCnt(m_hashfunctions.size())
	{
#ifdef TRACE_HASH_ACCESS
        hash_access_insert.resize(m_hfCnt, 0);
        hash_access_lookup.resize(m_hfCnt, 0);
#endif

#ifdef RUN_LENGTH_STAT
		runLengths.resize(maxIterations +1, 0);
#endif

#ifdef HASH_FUNC_STAT
        hashFuncStats.resize(m_hfCnt, 0);
#endif

#ifdef MANUAL_RAND
        if (hfs.size() != replacingDist.size()) {
            throw std::invalid_argument("the number of hashfunctions and the number of parts of the replacing distribution must be equal");
        }
#endif

        if (m_hfCnt == 0) {
            throw std::invalid_argument("the replacing hashtable cannot be created without at least one hashfunction!");
        }
	}

	~Replacing() {
	}

#ifdef MANUAL_RAND
    const std::vector<size_t> &getReplacingDistribution() const {
        return uneqDist.getProbs();
    }
#endif

	const DataContainer &getDataContainer() const {
		return dataContainer;
	}

	typedef InsertResult<Key, Value> HT_InsertResult;

#ifdef PREFETCH
#ifdef PREFETCH_FIRST_PAGE_NO_CACHE
	inline KeyType prefetch(const KeyType &key) const {
        dataContainer.prefetch(m_hashfunctions.front().hash(key, dataContainer.hashPositionCnt()).pos);
		return key;
	}
#elif defined(PREFETCH_FIRST_PAGE)
	struct PrefetchInfo_FirstHT {
		KeyType key;
		StoreInfo si;
	};

	inline PrefetchInfo_FirstHT prefetch(const KeyType &key) const {
//		PrefetchInfo pI{key, m_hashfunctions.front().hash(key, pages.hashPositionCnt())};
		PrefetchInfo_FirstHT prefetchInfo;

		prefetchInfo.key = key;
		prefetchInfo.si.hfIdx = 0;
		prefetchInfo.si = {m_hashfunctions.front().hash(key, dataContainer.hashPositionCnt()), 0};

		dataContainer.prefetch(prefetchInfo.si.pos);

		return prefetchInfo;
	}

#ifdef PREFETCH_FIRST_PAGE_NEXT_PAGE
	inline AppendResult insert_hf(const PrefetchInfo_FirstHT &keyPrefetch, const Value &val) {
#ifdef DEBUG_COMPILE
        if ((val & ((1 << dataContainer.getValueBitCount()) -1)) != val) {
			throw std::invalid_argument("the given val overflows value bitcount!");
		}
#endif

#ifdef TRACE_HASH_ACCESS
		++hash_access_insert[0];
#endif

        if (unlikely(m_hashfunctions.size() == 1)) {
            return dataContainer.insert(InsertStoreInfo{keyPrefetch.si, val});
        }

		InsertStoreInfo nextSi {m_hashfunctions[1].hash(keyPrefetch.key, dataContainer.hashPositionCnt()), 1, val};
		dataContainer.prefetch(nextSi.pos);

		AppendResult res = dataContainer.insert(InsertStoreInfo{keyPrefetch.si, val});

		if (likely(res == APPEND_EXISTS || res ==  APPEND_APPENDED)) {
#ifdef HASH_FUNC_STAT
			if(res == APPEND_APPENDED) {
				++hashFuncStats[0];
			}
#endif
			return res;
		}

        for (size_t hashIdx=1; hashIdx<m_hfCnt -1; ++hashIdx) {

#ifdef TRACE_HASH_ACCESS
			++hash_access_insert[hashIdx];
#endif

			auto si = nextSi;
			nextSi = InsertStoreInfo{m_hashfunctions[hashIdx +1].hash(keyPrefetch.key, dataContainer.hashPositionCnt()), hashIdx +1, val};
			dataContainer.prefetch(nextSi.pos);

			AppendResult res = dataContainer.insert(si);

#ifdef HASH_FUNC_STAT
			if (res == APPEND_APPENDED) {
				++hashFuncStats[hashIdx];
			}
#endif

			if (likely(res == APPEND_EXISTS || res ==  APPEND_APPENDED)) {
				return res;
			}
		}

#ifdef TRACE_HASH_ACCESS
		++hash_access_insert[hashIdx];
#endif

		res = dataContainer.insert(nextSi);

#ifdef HASH_FUNC_STAT
		if (res == APPEND_APPENDED) {
            ++hashFuncStats[m_hfCnt -1];
		}
#endif

		return res;
	}

#else // !PREFETCH_FIRST_PAGE_NEXT_PAGE
	inline AppendResult insert_hf(const PrefetchInfo_FirstHT &keyPrefetch, const Value &val) {
#ifdef DEBUG_COMPILE
        if ((val & ((1 << dataContainer.getValueBitCount()) -1)) != val) {
			throw std::invalid_argument("the given val overflows value bitcount!");
		}
#endif

#ifdef TRACE_HASH_ACCESS
		++hash_access_insert[0];
#endif

        AppendResult res = dataContainer.insert(InsertStoreInfo{keyPrefetch.si, val});

		if (likely(res == APPEND_EXISTS || res ==  APPEND_APPENDED)) {
			if(res == APPEND_APPENDED) {
				++hashFuncStats[0];
			}
			return res;
		}

        for (size_t hashIdx=1; hashIdx<m_hfCnt; ++hashIdx) {

	#ifdef TRACE_HASH_ACCESS
			++hash_access_insert[hashIdx];
	#endif

            auto si = InsertStoreInfo{m_hashfunctions[hashIdx].hash(keyPrefetch.key, dataContainer.hashPositionCnt()), hashIdx, val};
            AppendResult res = dataContainer.insert(si);

	#ifdef HASH_FUNC_STAT
			if (res == APPEND_APPENDED) {
				++hashFuncStats[hashIdx];
			}
	#endif

			if (likely(res == APPEND_EXISTS || res ==  APPEND_APPENDED)) {
				return res;
			}
		}

		return APPEND_FULL;
	}
#endif

	///@attention This function walks one iteration further than intended
	inline HT_InsertResult insert(const PrefetchInfo_FirstHT &keyPrefetch, Value val = Value(0)) {
		AppendResult res = insert_hf(keyPrefetch, val);

		if (res != APPEND_FULL) {
#ifdef COUNT_ELEMS
			if (res == APPEND_APPENDED) {
				++elems;
			}
#endif
			return HT_InsertResult{res, keyPrefetch.key, val};
		}

#ifdef MANUAL_RAND
            unsigned int hfIdx = uneqDist() % m_hfCnt;
            unsigned int posInPage = std::rand() % dataContainer.pageCapacity();
#else
        unsigned int randPos = rand_gen() % (m_hfCnt * dataContainer.pageCapacity());

        unsigned int hfIdx = randPos / dataContainer.pageCapacity();
        unsigned int posInPage = randPos % dataContainer.pageCapacity();
#endif

		ReplaceStoreInfo hiInsert = {m_hashfunctions[hfIdx].hash(keyPrefetch.key, dataContainer.hashPositionCnt()), hfIdx, val, posInPage};

		auto hiRemove = dataContainer.replace(hiInsert);

#ifdef HASH_FUNC_STAT
		++hashFuncStats[hfIdx];
		--hashFuncStats[hiRemove.hfIdx];
#endif

		KeyType insertKey = m_hashfunctions[hiRemove.hfIdx].value_word(hiRemove, dataContainer.hashPositionCnt());
		return insert(insertKey, hiRemove.val);
	}
#elif defined(PREFETCH_ALL_PAGE)
	struct PrefetchInfo_AllHTs {
		KeyType key;
        std::vector<StoreInfo> prefetch;
	};

	inline PrefetchInfo_AllHTs prefetch(const KeyType &key) const {
//		PrefetchInfo pI{key, m_hashfunctions.front().hash(key, pages.hashPositionCnt())};
		PrefetchInfo_AllHTs prefetchInfo;

		prefetchInfo.key = key;
        prefetchInfo.prefetch.reserve(m_hfCnt);


        for (size_t i=0; i<m_hfCnt ; ++i) {
            auto &hf = m_hashfunctions[i];
            prefetchInfo.prefetch.push_back(StoreInfo{hf.hash(key, dataContainer.hashPositionCnt()), i});
            dataContainer.prefetch(prefetchInfo.prefetch.back().pos);
		}

		return prefetchInfo;
	}

	inline PrefetchInfo_AllHTs prefetchRest(size_t hfIdx, const typename HF::StoreInfo &si) {
		PrefetchInfo_AllHTs prefetchInfo;
		prefetchInfo.key = m_hashfunctions[hfIdx].value(si);
        prefetchInfo.prefetch.reserve(m_hfCnt);

        for (size_t i=0; i<m_hfCnt; ++i) {
			if (i == hfIdx) {
				continue;
			}
            prefetchInfo.push_back(StoreInfo{m_hashfunctions[i].hash(prefetchInfo.key, dataContainer.hashPositionCnt()), i});
            dataContainer.prefetch(prefetchInfo.prefetch.back().pos);
		}

		return prefetchInfo;
	}

	inline AppendResult insert_hf(const PrefetchInfo_AllHTs &keyPrefetch, const Value &val) {
#ifdef DEBUG_COMPILE
        if ((val & ((1 << dataContainer.getValueBitCount()) -1)) != val) {
			throw std::invalid_argument("the given val overflows value bitcount!");
		}
#endif

		for (size_t hashIdx=0; hashIdx<keyPrefetch.prefetch.size(); ++hashIdx) {

	#ifdef TRACE_HASH_ACCESS
			++hash_access_insert[hashIdx];
    #endif

            InsertStoreInfo si = {keyPrefetch.prefetch[hashIdx], val};

            AppendResult res = dataContainer.insert(si);

	#ifdef HASH_FUNC_STAT
			if (res == APPEND_APPENDED) {
				++hashFuncStats[hashIdx];
			}
	#endif

			if (likely(res == APPEND_EXISTS || res ==  APPEND_APPENDED)) {
				return res;
			}
		}

		return APPEND_FULL;
	}

	inline HT_InsertResult insert(const PrefetchInfo_AllHTs &keyPrefetch, Value val = Value(0)) {
		unsigned int iterations = 0;
		AppendResult res;

#ifdef DEBUG_COMPILE
        if ((val & ((1 << dataContainer.getValueBitCount()) -1)) != val) {
			throw std::invalid_argument("given val overflows valueBitCount!");
		}
#endif

		if ((res = insert_hf(keyPrefetch, val)) == APPEND_FULL) {
			auto insertKey = keyPrefetch.key;
			Value insertVal  = val;

			do {
                unsigned int randPos = rand_gen() % (m_hfCnt * dataContainer.pageCapacity());

                unsigned int hfIdx = randPos / dataContainer.pageCapacity();
                unsigned int posInPage = randPos % dataContainer.pageCapacity();

                ReplaceStoreInfo hiInsert {m_hashfunctions[hfIdx].hash(insertKey, dataContainer.hashPositionCnt()), hfIdx, insertVal, posInPage};

                ReplaceStoreInfo hiRemove = dataContainer.replace(hiInsert);

                size_t hfIdx_remove = hiRemove.hfIdx;

#ifdef HASH_FUNC_STAT
				++hashFuncStats[hfIdx];
				--hashFuncStats[hfIdx_remove];
#endif

                insertKey = m_hashfunctions[hfIdx_remove].value_word(hiRemove, dataContainer.hashPositionCnt());
                insertVal = hiRemove.val;

				++iterations;
			} while(iterations < maxIterations && (res = insert_hf(insertKey, insertVal)) == APPEND_FULL);

            if (res == APPEND_FULL) {
				return HT_InsertResult{APPEND_FULL, insertKey, insertVal};
			}
		}

#ifdef RUN_LENGTH_STAT
		++runLengths[iterations];
#endif

		if (unlikely(res == APPEND_EXISTS)) {
#ifdef DEBUG_TESTS
			if (iterations > 0) {
				std::cout << "this should never happen!" << std::endl;
			}
			assert(iterations == 0 && "a reinserted value should never exist!");
#endif
			return HT_InsertResult{APPEND_EXISTS, keyPrefetch.key, val};
		}
		else { //APPEND_APPENDED
#ifdef COUNT_ELEMS
			++elems;
#endif
			return HT_InsertResult{res, keyPrefetch.key, val};
		}
	}
#endif
#endif


	inline HT_InsertResult insert(const KeyType &key, const Value &val = Value(0)) {
		unsigned int iterations = 0;
		AppendResult res;
		KeyType insertKey = key; ///@todo for larger key types we should take another WordType
		Value insertVal = val;
		size_t startCheckAtHF = 0;

#ifdef DEBUG_COMPILE
        if ((val & ((1 << dataContainer.getValueBitCount()) -1)) != val) {
			throw std::invalid_argument("given val overflows valueBitCount!");
		}
#endif

		while ((res = insert_hf(insertKey, insertVal, startCheckAtHF)) == APPEND_FULL && iterations < maxIterations) {
#ifdef MANUAL_RAND
            unsigned int hfIdx = uneqDist() % m_hfCnt;
            unsigned int posInPage = std::rand() % dataContainer.pageCapacity();
#else
            unsigned int randPos = std::rand() % (m_hfCnt * dataContainer.pageCapacity());
            unsigned int hfIdx = randPos / dataContainer.pageCapacity();
            unsigned int posInPage = randPos % dataContainer.pageCapacity();
#endif

			ReplaceStoreInfo hiInsert {m_hashfunctions[hfIdx].hash(insertKey, dataContainer.hashPositionCnt()), hfIdx, insertVal, posInPage};

			ReplaceStoreInfo removed = dataContainer.replace(hiInsert);
            startCheckAtHF = removed.hfIdx +1;

#ifdef HASH_FUNC_STAT
			++hashFuncStats[hfIdx];
			--hashFuncStats[startCheckAtHF -1];
#endif

            insertKey = m_hashfunctions[startCheckAtHF -1].value_word(removed, dataContainer.hashPositionCnt());
			insertVal = removed.val;

			++iterations;
		}

#ifdef RUN_LENGTH_STAT
			++runLengths[iterations];
#endif

		switch (res) {
		case APPEND_EXISTS:
#ifdef DEBUG_TESTS
			assert(iterations == 0 && "a reinserted value should never exist!");
#endif
			return HT_InsertResult{APPEND_EXISTS, key, val};
		case APPEND_FULL:
			return HT_InsertResult{APPEND_FULL, insertKey, insertVal};
		case APPEND_APPENDED:
#ifdef COUNT_ELEMS
			++elems;
#endif
			return HT_InsertResult{res, key, val};
		}

        assert(false && "switch did not catch!");
	}


#ifdef PREFETCH
#ifndef PREFETCH_FIRST_PAGE_NO_CACHE
#if defined(PREFETCH_FIRST_PAGE)
#ifdef PREFETCH_FIRST_PAGE_NEXT_PAGE
	inline LookupResult lookup(const PrefetchInfo_FirstHT &keyPrefetch) const {
#ifdef TRACE_HASH_ACCESS
		++hash_access_insert[0];
#endif

        if (unlikely(m_hashfunctions.size() == 1)) {
             return dataContainer.lookup(keyPrefetch.si);
        }

        StoreInfo nextSi {m_hashfunctions[1].hash(keyPrefetch.key, dataContainer.hashPositionCnt()), 1};
		dataContainer.prefetch(nextSi.pos);

        LookupResult res (dataContainer.lookup(keyPrefetch.si));
		if (res == LOOKUP_EXISTS || res == LOOKUP_NOT_EXISTS) {
			return res;
		}

		//hashfunctions between (there is a hashfunction to calculate ahead)
        for (size_t hashIdx=1; hashIdx<m_hfCnt -1; ++hashIdx) {
#ifdef TRACE_HASH_ACCESS
			++hash_access_lookup[hashIdx];
#endif
			auto currSI = nextSi;

            nextSi = StoreInfo {m_hashfunctions[hashIdx +1].hash(keyPrefetch.key, dataContainer.hashPositionCnt()), hashIdx +1};
			dataContainer.prefetch(nextSi.pos);

			res = dataContainer.lookup(currSI);
			if (res == LOOKUP_EXISTS || res == LOOKUP_NOT_EXISTS) {
				return res;
			}
		}

#ifdef TRACE_HASH_ACCESS
        ++hash_access_lookup[m_hfCnt -1];
#endif

		res = dataContainer.lookup(nextSi);
		if (res == LOOKUP_EXISTS || res == LOOKUP_NOT_EXISTS) {
			return res;
		}

		return LOOKUP_UNSURE;
	}
#else
	inline LookupResult lookup(const PrefetchInfo_FirstHT &keyPrefetch) const {

#ifdef TRACE_HASH_ACCESS
		++hash_access_insert[0];
#endif

        auto si = StoreInfo {m_hashfunctions[0].hash(keyPrefetch.key, dataContainer.hashPositionCnt()), 0};
        LookupResult res = dataContainer.lookup(si);

		if (res == LOOKUP_EXISTS || res == LOOKUP_NOT_EXISTS) {
			return res;
		}

        for (size_t hashIdx=1; hashIdx<m_hfCnt; ++hashIdx) {
#ifdef TRACE_HASH_ACCESS
			++hash_access_lookup[hashIdx];
#endif

            auto si = StoreInfo {m_hashfunctions[hashIdx].hash(keyPrefetch.key, dataContainer.hashPositionCnt()), hashIdx};
            res = dataContainer.lookup(si);

			if (res == LOOKUP_EXISTS || res == LOOKUP_NOT_EXISTS) {
				return res;
			}
		}

		return LOOKUP_UNSURE;
	}
#endif
#elif defined(PREFETCH_ALL_PAGE)
    inline LookupResult lookup(const PrefetchInfo_AllHTs &prefetched) const {
        for (size_t hashIdx=0; hashIdx<m_hfCnt; ++hashIdx) {
#ifdef TRACE_HASH_ACCESS
            ++hash_access_lookup[hashIdx];
#endif

            const auto &si = prefetched.prefetch[hashIdx];

            LookupResult res = dataContainer.lookup(si);

            if (res == LOOKUP_EXISTS || res == LOOKUP_NOT_EXISTS) {
                return res;
            }
        }

        return LOOKUP_UNSURE;
    }
#endif
#endif
#endif

	inline LookupResult lookup(const Key &key) const {
        for (size_t hashIdx=0; hashIdx<m_hfCnt; ++hashIdx) {
#ifdef TRACE_HASH_ACCESS
			++hash_access_lookup[hashIdx];
#endif

			StoreInfo si {m_hashfunctions[hashIdx].hash(key, dataContainer.hashPositionCnt()), hashIdx};

			LookupResult res = dataContainer.template lookup<StoreInfo>(si);

			if (res == LOOKUP_EXISTS || res == LOOKUP_NOT_EXISTS) {
				return res;
			}
		}

		return LOOKUP_UNSURE;
	}

	using AccessSupport = typename DataContainer::AccessSupport;
    using HT_ValueResult = AccessSupport;

	auto value(const KeyType &key) {
        for (size_t hashIdx=0; hashIdx<m_hfCnt; ++hashIdx) {
#ifdef TRACE_HASH_ACCESS
			++hash_access_lookup[hashIdx];
#endif
			StoreInfo si {m_hashfunctions[hashIdx].hash(key, dataContainer.hashPositionCnt()), hashIdx};

			AccessSupport res(dataContainer.value(si));

			if (res.operator LookupResult() == LOOKUP_EXISTS || res.operator LookupResult() == LOOKUP_NOT_EXISTS) {
				return res;
			}
		}

		return AccessSupport(LOOKUP_UNSURE);
	}

#ifdef PREFETCH
#ifndef PREFETCH_FIRST_PAGE_NO_CACHE
#if defined(PREFETCH_FIRST_PAGE)
#ifdef PREFETCH_FIRST_PAGE_NEXT_PAGE
    inline auto value(const PrefetchInfo_FirstHT &keyPrefetch) const {
#ifdef TRACE_HASH_ACCESS
        ++hash_access_insert[0];
#endif

        if (unlikely(m_hashfunctions.size() == 1))  {
            AccessSupport res(dataContainer.value(keyPrefetch.si));
            return res;
        }

        StoreInfo nextSi {m_hashfunctions[1].hash(keyPrefetch.key, dataContainer.hashPositionCnt()), 1};
        dataContainer.prefetch(nextSi.pos);

        {
            AccessSupport res(dataContainer.value(keyPrefetch.si));
            if (res.operator LookupResult() == LOOKUP_EXISTS || res.operator LookupResult() == LOOKUP_NOT_EXISTS) {
                return res;
            }
        }

        //hashfunctions between (there is a hashfunction to calculate ahead)
        for (size_t hashIdx=1; hashIdx<m_hfCnt -1; ++hashIdx) {
#ifdef TRACE_HASH_ACCESS
            ++hash_access_lookup[hashIdx];
#endif
            auto currSI = nextSi;

            nextSi = StoreInfo {m_hashfunctions[hashIdx +1].hash(keyPrefetch.key, dataContainer.hashPositionCnt()), hashIdx +1};
            dataContainer.prefetch(nextSi.pos);

            AccessSupport res (dataContainer.value(currSI));
            if (res.operator LookupResult() == LOOKUP_EXISTS || res.operator LookupResult() == LOOKUP_NOT_EXISTS) {
                return res;
            }
        }

#ifdef TRACE_HASH_ACCESS
        ++hash_access_lookup[m_hfCnt -1];
#endif

        AccessSupport res (dataContainer.value(nextSi));
        if (res.operator LookupResult() == LOOKUP_EXISTS || res.operator LookupResult() == LOOKUP_NOT_EXISTS) {
            return res;
        }

        return AccessSupport(LOOKUP_UNSURE);
    }
#else
    inline auto value(const PrefetchInfo_FirstHT &keyPrefetch) const {

#ifdef TRACE_HASH_ACCESS
        ++hash_access_insert[0];
#endif

        auto si = StoreInfo {m_hashfunctions[0].hash(keyPrefetch.key, dataContainer.hashPositionCnt()), 0};
        AccessSupport res(dataContainer.value(si));

        if (res.operator LookupResult() == LOOKUP_EXISTS || res.operator LookupResult() == LOOKUP_NOT_EXISTS) {
            return res;
        }

        for (size_t hashIdx=1; hashIdx<m_hfCnt; ++hashIdx) {
#ifdef TRACE_HASH_ACCESS
            ++hash_access_lookup[hashIdx];
#endif

            auto si = StoreInfo {m_hashfunctions[hashIdx].hash(keyPrefetch.key, dataContainer.hashPositionCnt()), hashIdx};
            AccessSupport res(dataContainer.value(si));

            if (res.operator LookupResult() == LOOKUP_EXISTS || res.operator LookupResult() == LOOKUP_NOT_EXISTS) {
                return res;
            }
        }

        return AccessSupport(LOOKUP_UNSURE);
    }
#endif
#elif defined(PREFETCH_ALL_PAGE)
    auto value(const PrefetchInfo_AllHTs &prefetched) const {
        for (size_t hashIdx=0; hashIdx<m_hfCnt; ++hashIdx) {
#ifdef TRACE_HASH_ACCESS
            ++hash_access_lookup[hashIdx];
#endif
            const auto &si = prefetched.prefetch[hashIdx];

            AccessSupport res(dataContainer.value(si));

            if (res.operator LookupResult() == LOOKUP_EXISTS || res.operator LookupResult() == LOOKUP_NOT_EXISTS) {
                return res;
            }
        }

        return AccessSupport(LOOKUP_UNSURE);
    }
#endif
#endif
#endif

	inline size_t capacity() const {
		return dataContainer.elemCapacity();
	}

	inline size_t hashPosCnt() const {
		return dataContainer.hashPositionCnt();
	}

	inline size_t hfCount() const {
        return m_hfCnt;
	}

	inline size_t walkLength() const {
		return maxIterations;
	}

#ifdef TRACE_HASH_ACCESS
	inline const std::vector<size_t> &getHashAccess_insert() const {return hash_access_insert;}
	inline const std::vector<size_t> &getHashAccess_Lookup() const {return hash_access_lookup;}
#endif

#ifdef COUNT_ELEMS
	inline size_t elemCount() const {return elems;}
#endif

#ifdef RUN_LENGTH_STAT
	const std::vector<size_t> &getRunLengths() const {
		return runLengths;
	}
#endif

#ifdef HASH_FUNC_STAT
	const std::vector<size_t> &getHashFuncStats() const {
		return hashFuncStats;
	}
#endif

	std::unordered_map<Value, size_t> getValueCounts() const {


        for (size_t i=0; i<dataContainer.elemCapacity(); ++i) {

        }

        return dataContainer.getValueCounts();
	}

    template<class InsertActor>
    FORCE_INLINE void finishInsert(InsertActor&) const {}

    template<class LookupActor>
    FORCE_INLINE void finishLookup(LookupActor&) const {}

    template<class ValueActor>
    FORCE_INLINE void finishValue(ValueActor&) const {}

protected:
	const size_t maxIterations;
#ifdef COUNT_ELEMS
	size_t elems = 0;
#endif

#ifdef TRACE_HASH_ACCESS
	mutable std::vector<size_t> hash_access_insert;
	mutable std::vector<size_t> hash_access_lookup;
#endif

#ifdef RUN_LENGTH_STAT
	std::vector<size_t> runLengths;
#endif

#ifdef HASH_FUNC_STAT
	std::vector<size_t> hashFuncStats;
#endif

#ifdef PREFETCH_FIRST_PAGE_NEXT_PAGE
	inline AppendResult insert_hf(const KeyType &key, const Value &val, size_t startAtHF = 0) {
#ifdef DEBUG_COMPILE
        if ((val & ((1 << dataContainer.getValueBitCount()) -1)) != val) {
			throw std::invalid_argument("the given val overflows value bitcount!");
		}
#endif

        if (startAtHF == m_hfCnt) {
			return APPEND_FULL;
		}

        InsertStoreInfo nextSi {m_hashfunctions[startAtHF].hash(key, dataContainer.hashPositionCnt()), startAtHF, val};
		dataContainer.prefetch(nextSi.pos);

        for (size_t hashIdx=startAtHF; hashIdx<m_hfCnt -1; ++hashIdx) {
	#ifdef TRACE_HASH_ACCESS
			++hash_access_insert[hashIdx];
	#endif

            InsertStoreInfo si = nextSi;
            nextSi = InsertStoreInfo{m_hashfunctions[hashIdx +1].hash(key, dataContainer.hashPositionCnt()), hashIdx +1, val};
			dataContainer.prefetch(nextSi.pos);

			AppendResult res = dataContainer.insert(si);

	#ifdef HASH_FUNC_STAT
			if (res == APPEND_APPENDED) {
				++hashFuncStats[hashIdx];
			}
	#endif

			if (likely(res == APPEND_EXISTS || res ==  APPEND_APPENDED)) {
				return res;
			}
		}

#ifdef TRACE_HASH_ACCESS
		++hash_access_insert[hashIdx];
#endif

		AppendResult res = dataContainer.insert(nextSi);

#ifdef HASH_FUNC_STAT
		if (res == APPEND_APPENDED) {
            ++hashFuncStats[m_hfCnt -1];
		}
#endif

		return res;
	}
#else
	inline AppendResult insert_hf(const KeyType &key, const Value &val, size_t startAtHF = 0) {
#ifdef DEBUG_COMPILE
        if ((val & ((1 << dataContainer.getValueBitCount()) -1)) != val) {
			throw std::invalid_argument("the given val overflows value bitcount!");
		}
#endif

        for (size_t hashIdx=startAtHF; hashIdx<m_hfCnt; ++hashIdx) {

	#ifdef TRACE_HASH_ACCESS
			++hash_access_insert[hashIdx];
	#endif

			InsertStoreInfo si {m_hashfunctions[hashIdx].hash(key, dataContainer.hashPositionCnt()), hashIdx, val};

			AppendResult res = dataContainer.insert(si);

	#ifdef HASH_FUNC_STAT
			if (res == APPEND_APPENDED) {
				++hashFuncStats[hashIdx];
			}
	#endif

			if (likely(res == APPEND_EXISTS || res ==  APPEND_APPENDED)) {
				return res;
			}
		}

		return APPEND_FULL;
	}
#endif
};

}

}
