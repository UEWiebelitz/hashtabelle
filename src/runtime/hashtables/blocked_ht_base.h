#ifndef BLOCKED_HT_BASE_H
#define BLOCKED_HT_BASE_H

#include <vector>
#include <unordered_map>
#include "compile_hints/inline_enforce.h"

namespace Runtime {
namespace Hashtable {

template<class Key, class Value, class HT, class HF>
class Blocked_Base {
public:
    using KeyType = Key;
    using ValueType = Value;

    using SubHT = HT;
    using HFType = HF;
    using HT_InsertResult = typename SubHT::HT_InsertResult;
    using HT_LookupResult = typename SubHT::HT_LookupResult;
    using HT_ValueResult = typename SubHT::HT_ValueResult;

    Blocked_Base(const std::vector<HT*> &hashtables, HF &hf)
        : hashtables(hashtables)
        , hf(hf)
    {}

    FORCE_INLINE const auto &getSubHTs() const {
        return hashtables;
    }

    size_t elemCount() const {
        size_t elemCountSum = 0;
        for (auto subHt : hashtables) {
            elemCountSum += subHt->elemCount();
        }

        return elemCountSum;
    }

    size_t capacity() const {
        size_t capacitySum = 0;
        for (auto subHt : hashtables) {
            capacitySum += subHt->capacity();
        }

        return capacitySum;
    }

    std::unordered_map<Value, size_t> getValueCounts() const {
        std::unordered_map<Value, size_t> valueCounts;
//        std::cerr << "getValueCounts is currently not implemented" << std::endl;

        for (auto *hashtable : hashtables) {
            auto htValueCounts = hashtable->getValueCounts();

            for (auto &ht_elem : htValueCounts) {
                auto it = valueCounts.find(ht_elem.first);
                if (it == valueCounts.end()) {
                    valueCounts.insert(ht_elem);
                }
                else {
                    it->second += ht_elem.second;
                }
            }
        }

        return valueCounts;
    }

    FORCE_INLINE auto hashInfo(const KeyType &key) const {
        return hf.hash(key, hashtables.size());
    }

protected:
    std::vector<HT*> hashtables;
    HF &hf;
};
}
}

#endif // BLOCKED_HT_BASE_H
