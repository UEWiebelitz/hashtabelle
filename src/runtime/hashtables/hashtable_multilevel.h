#ifndef HASHTABLE_MULTILAYER_H
#define HASHTABLE_MULTILAYER_H

#include "return_indicators.h"
#include "template/get_nth_type.h"
#include "hashtable_base.h"

#include <tuple>
#include <cassert>

#include <cstring>
#include <utility>
#include <unordered_map>



template<class T, class ...Rest>
struct MaxSize {
	constexpr static size_t t_size = sizeof (T);
	constexpr static size_t rest_size = MaxSize<Rest...>::value;
	constexpr static size_t value = t_size > rest_size ? t_size : rest_size;
};

template<class T>
struct MaxSize<T> {
	constexpr static size_t value = sizeof(T);
};

template<class T>
constexpr size_t maxSize() {
    return sizeof (T);
};

template<class T, class ...Rest>
constexpr size_t maxSize() {
    return std::max(sizeof (T), maxSize<Rest...>());
};

template<class T>
[[noreturn]] void fatal(T arg) {
	std::cout << arg << std::endl << std::flush;
	exit(1);
}

template<class T, class ...Types>
[[noreturn]] void fatal(T arg, Types ...args) {
	std::cout << arg;
	fatal(std::forward<Types...>(args...));
}

template <class Key, class Value, class ...HTs>
class HashtableMultilevel : public Hashtable_base<Key, Value> {
	static_assert (sizeof ...(HTs) > 0, "no Hastable given");

	template<size_t pos>
	size_t capacity() const {
        return std::get<pos, HTs&...>(hts).capacity() + capacity<pos-1>();
	}

    template<>
    size_t capacity<0>() const {
        return std::get<0, HTs&...>(hts).capacity();
    }

	template<size_t pos>
	size_t memory() const {
        return std::get<pos, HTs&...>(hts).memory() + memory<pos-1>();
	}

    template<>
    size_t memory<0>() const {
        return std::get<0, HTs&...>(hts).memory();
    }

#ifdef COUNT_ELEMS
	template<size_t pos>
	size_t elemCount() const {
        return std::get<pos, HTs&...>(hts).elemCount() + elemCount<pos-1>();
	}

    template<>
    size_t elemCount<0>() const {
        return std::get<0, HTs&...>(hts).elemCount();
    }
#endif

public:
	HashtableMultilevel(HTs& ...hts) : hts(hts...) {
	}

	const std::tuple<HTs&...> &getSubHashtables() const {
		return hts;
	}

    typedef InsertResult<Key, Value> HT_InsertResult;
    typedef LookupResult HT_LookupResult;

	inline size_t capacity() const {
		return capacity<sizeof ...(HTs) -1>();
	}

	inline size_t memory() const {
		return memory<sizeof ...(HTs) -1>();
	}

#ifdef COUNT_ELEMS
	inline size_t elemCount() const {
		return elemCount<sizeof ...(HTs) -1>();
	}
#endif

	std::unordered_map<Value, size_t> getValueCounts() const {
		std::unordered_map<Value, size_t> valueCounts;
		getValueCounts<0>(valueCounts);
		return valueCounts;
	}

    class AccessSupport {
        const size_t hashtable_idx;
        char storage[MaxSize<typename HTs::AccessSupport...>::value];

        template<int hashtable>
        inline Value get(size_t hashtable_idx) const {
            if (hashtable_idx == hashtable) {
                typedef const typename get_Nth_type<hashtable, typename HTs::AccessSupport...>::type HT_AccessSupport;
//				HT_AccessSupport accessSupport;
//				std::memcpy(&accessSupport, storage, sizeof (HT_AccessSupport));
                return reinterpret_cast<HT_AccessSupport*>(storage)->get();
            }

            return get<hashtable +1>(hashtable_idx);
        }

        template<>
        inline Value get<sizeof ...(HTs)>(size_t) const {
             fatal("hashtable_idx is out of range!");
        }

        template<int hashtable>
        inline void set(size_t hashtable_idx, const Value &val) {
            if (hashtable_idx == hashtable) {
                auto accessSupport = reinterpret_cast<typename get_Nth_type<hashtable, typename HTs::AccessSupport...>::type *>(storage);
                return accessSupport->set(val);
            }

            return set<hashtable +1>(hashtable_idx, val);
        }

        template<>
        inline void set<sizeof ...(HTs)>(size_t, const Value &) {
            fatal("hashtable_idx is out of range!");
        }

        template<int hashtable>
        inline LookupResult getResult(size_t hashtable_idx) const {
            if (hashtable_idx == hashtable) {
                auto accessSupport = reinterpret_cast<const typename get_Nth_type<hashtable, typename HTs::AccessSupport...>::type *>(storage);
                return accessSupport->operator LookupResult();
            }

            return getResult<hashtable +1>(hashtable_idx);
        }

        template<>
        inline LookupResult getResult<sizeof ...(HTs)>(size_t) const {
            fatal("hashtable_idx is out of range!");
        }

    public:
        template<class HT_AccessSupport>
        inline AccessSupport(size_t hashtable_idx, const HT_AccessSupport &accessSupport)
            : hashtable_idx(hashtable_idx)
        {
            std::memcpy(storage, &accessSupport, sizeof (HT_AccessSupport));
        }

        inline AccessSupport(LookupResult result)
            : hashtable_idx(0)
        {
            typename get_Nth_type<0, HTs...>::type::AccessSupport accSupp (result);
            std::memcpy(storage + sizeof (storage) - sizeof (typename get_Nth_type<0, HTs...>::type::AccessSupport), &accSupp, sizeof (typename get_Nth_type<0, HTs...>::type::AccessSupport));
        }

        inline operator LookupResult() const {
            return getResult<0>(0);
        }

        inline LookupResult res() const {
            return operator LookupResult();
        }

        inline Value get() const {
            return get<0>(hashtable_idx);
        }

        inline operator Value() const {
            return get();
        }

        inline void set(const Value &val) {
            set<0>(hashtable_idx, val);
        }

        inline void operator=(const Value &val) {
            set(val);
        }
    };

#ifdef PREFETCH
	using HT1_PrefetchType = decltype ((std::declval<typename get_Nth_type<0, HTs...>::type>().prefetch(std::declval<Key>())));

    inline HT1_PrefetchType prefetch(const Key &key) const {
        return std::get<0, HTs&...>(hts).prefetch(key);
    }

	template<class T, typename std::enable_if<!std::is_same<T, Key>::value && std::is_same<T, HT1_PrefetchType>::value, int>::type = 0>
    inline HT_InsertResult insert(const T &prefetchInfo, const Value &val = Value()) {
        HT_InsertResult res = std::get<0, HTs&...>(hts).insert(prefetchInfo, val);

		if (res.res != APPEND_FULL) {
#ifdef COUNT_ELEMS
			if (res == APPEND_APPENDED) {
				++elems;
			}
#endif

			return res;
		}

#if __cplusplus >= 201703L
        if constexpr (1 != sizeof ...(HTs)) {
#else
        if (1 != sizeof ...(HTs)) {
#endif
			return insert<1>(res.key, res.val);
		}
		else { //APPEND_FULL
			return res;
		}
	}

	template<class T, typename std::enable_if<!std::is_same<T, Key>::value && std::is_same<T, HT1_PrefetchType>::value, int>::type = 0>
	inline LookupResult lookup(const T &prefetched) const {
        LookupResult res = std::get<0, HTs&...>(hts).lookup(prefetched);

		if (res != LOOKUP_UNSURE) {
			return res;
		}

#if __cplusplus >= 201703L
        if constexpr (1 != sizeof ...(HTs)) {
#else
        if (1 != sizeof ...(HTs)) {
#endif
			return lookup<1>(prefetched.key);
		}
		else {
			return LOOKUP_NOT_EXISTS;
		}
	}

    template<class T, typename std::enable_if<!std::is_same<T, Key>::value && std::is_same<T, HT1_PrefetchType>::value, int>::type = 0>
    inline AccessSupport value(const T &prefetched) {
        AccessSupport res (0, std::get<0, HTs&...>(hts).value(prefetched));

        if (res != LOOKUP_UNSURE) {
            return res;
        }

#if __cplusplus >= 201703L
        if constexpr (1 != sizeof ...(HTs)) {
#else
        if (1 != sizeof ...(HTs)) {
#endif
            return value<1>(prefetched.key);
        }
        else {
            return LOOKUP_NOT_EXISTS;
        }
    }
#endif

    inline HT_InsertResult insert(const Key &key, const Value &val = Value()) {
        HT_InsertResult res = HashtableMultilevel<Key, Value, HTs...>::template insert<0>(key, val);
#ifdef COUNT_ELEMS
		if (res == APPEND_APPENDED) {
			++elems;
		}
#endif
		return res;
	}

	inline LookupResult lookup(const Key &key) const {
        return HashtableMultilevel::template lookup<0>(key);
	}

    using HT_ValueResult = AccessSupport;

	inline AccessSupport value(const Key &key) {
        return HashtableMultilevel::template value<0>(key);
	}

protected:
	std::tuple<HTs&...> hts;

#ifdef COUNT_ELEMS
	size_t elems = 0;
#endif

	template<class HT, class ...Rest>
	struct sumCapacities {
		static constexpr unsigned long long value = sumCapacities<Rest...>::value + HT::capacity();
	};

	template<class HT>
	struct sumCapacities<HT> {
		static constexpr unsigned long long value = HT::capacity();
	};

	template<int hfIdx>
	inline HT_InsertResult insert(const Key &key, const Value &val) {
        HT_InsertResult res = std::get<hfIdx, HTs&...>(hts).insert(key, val);

        if (res.res != APPEND_FULL) {
            return res;
        }

        return insert<hfIdx +1>(res.key, res.val);
	}

    template<>
    inline HT_InsertResult insert<sizeof ...(HTs) -1>(const Key &key, const Value &val) {
        HT_InsertResult res = std::get<sizeof ...(HTs) -1, HTs&...>(hts).insert(key, val);

        if (res.res != APPEND_FULL) {
            return res;
        }

        return res;
    }


	/**
	 * @brief lookup checks if key exists in the hastable using the hashtable at position hfIdx.
	 * If this cannot determined in this hashtable the next hashtable is also inspected.
	 * @param key
	 * @return LOOKUP_UNSURE if the corresponding page is full,
	 * LOOKUP_NOT_EXISTS if the page was not full and the value does not exist and
	 * LOOKUP_EXISTS if the value could be found.
	 */
	template<int hfIdx>
	inline LookupResult lookup(const Key &key) const {
        LookupResult res = std::get<hfIdx, HTs&...>(hts).lookup(key);

		if (res != LOOKUP_UNSURE) {
			return res;
		}

        return lookup<hfIdx +1>(key);
	}

    template<>
    inline LookupResult lookup<sizeof ...(HTs) -1>(const Key &key) const {
        LookupResult res = std::get<sizeof ...(HTs) -1, HTs&...>(hts).lookup(key);

        if (res != LOOKUP_UNSURE) {
            return res;
        }
        return LOOKUP_NOT_EXISTS;
    }


	template<int hfIdx>
	inline AccessSupport value(const Key &key) {
        AccessSupport res(hfIdx, std::get<hfIdx, HTs&...>(hts).value(key));

		if (res != LOOKUP_UNSURE) {
			return res;
		}

        return value<hfIdx +1>(key);
	}

    template<>
    inline AccessSupport value<sizeof ...(HTs) -1>(const Key &key) {
        AccessSupport res(sizeof ...(HTs) -1, std::get<sizeof ...(HTs) -1, HTs&...>(hts).value(key));

        if (res != LOOKUP_UNSURE) {
            return res;
        }

        return AccessSupport(LOOKUP_NOT_EXISTS);
    }

	template<int hfIdx>
	inline void getValueCounts(std::unordered_map<Value, size_t> &valuesMap) const {
        std::unordered_map<Value, size_t> ht_valuesMap = std::get<hfIdx, HTs&...>(hts).getValueCounts();

		for (auto ht_elem : ht_valuesMap) {
			auto it = valuesMap.find(ht_elem.first);
			if (it == valuesMap.end()) {
				valuesMap.insert(ht_elem);
			}
			else {
				it->second += ht_elem.second;
			}
		}
	}
};

#endif // HASHTABLE_MULTILAYER_H
