#ifndef BLOCKED_HT_NON_PARALLEL_H
#define BLOCKED_HT_NON_PARALLEL_H

#include "blocked_ht_base.h"
#include "compile_hints/inline_enforce.h"

namespace Runtime {

namespace Hashtable {

template<class Key, class Value, class HT, class HF>
class BlockedHT_nonParallel : public Blocked_Base<Key, Value, HT, HF> {
    using Base = Blocked_Base<Key, Value, HT, HF>;

public:
    using KeyType = typename Base::KeyType;
    using ValueType = typename Base::ValueType;

    BlockedHT_nonParallel(const Base &other)
        : Base(other)
    {
    }

    template<class InsertAction>
    inline auto insert(const KeyType &key, const ValueType &val, InsertAction &act) {
        auto hi = Base::hf.hash(key, Base::hashtables.size());
        Base::hashtables[hi.pos]->insert(hi.storeVal, val, act);
    }

    template<class InsertAction>
    inline auto insert(const KeyType &key, InsertAction &insertAction) {
        return insert(key, Value(), insertAction);
    }

    template<class LookupAction>
    inline auto lookup(const KeyType &key, LookupAction &act) const {
        auto hi = Base::hf.hash(key, Base::hashtables.size());
        Base::hashtables[hi.pos]->lookup(hi.storeVal, act);
    }

    inline auto directValue(const Key &key) {
        auto hi = Base::hf.hash(key, Base::hashtables.size());
        return Base::hashtables[hi.pos]->directValue(hi.storeVal);
    }

    template<class ValueAction>
    inline void value(const Key &key, ValueAction &act) {
        auto hi = Base::hf.hash(key, Base::hashtables.size());
        Base::hashtables[hi.pos]->value(hi.storeVal, act);
    }


    template<class InsertActor>
    void initInsert(InsertActor &insertAct) {
        for (auto *ht : Base::hashtables) {
            ht->initInsert(insertAct);
        }
    }

    template<class LookupActor>
    void initLookup(LookupActor &lookupAct) const {
        for (auto *ht : Base::hashtables) {
            ht->initLookup(lookupAct);
        }
    }

    template<class ValueActor>
    void initValue(ValueActor &valueAct) {
        for (auto *ht : Base::hashtables) {
            ht->initValue(valueAct);
        }
    }

    template<class ValueActor>
    void finishValue(ValueActor &act) {
        for (auto *ht : Base::hashtables) {
            ht->finishValue(act);
        }
    }

    template<class InsertActor>
    void finishInsert(InsertActor &act) {
        for (auto *ht : Base::hashtables) {
            ht->finishInsert(act);
        }
    }

    template<class LookupActor>
    void finishLookup(LookupActor &act) const {
        for (auto *ht : Base::hashtables) {
            ht->finishLookup(act);
        }
    }


    template<class Actor>
    class LookupAccessor {
         const Base *blockedHT = nullptr;
         std::vector<typename Base::SubHT::template LookupAccessor<Actor>> subLookupAcc;

        public:
        LookupAccessor(const Base *blockedHT)
            : blockedHT(blockedHT)
        {
            for (auto *subHT : blockedHT->getSubHTs()) {
                subLookupAcc.push_back(subHT->template getLookupAccessor<Actor>());
            }
        }

        LookupAccessor(LookupAccessor &&other)
            : subLookupAcc(std::move(other.subLookupAcc))
        {
            std::swap(blockedHT, other.blockedHT);
        }

        ~LookupAccessor() {}

        FORCE_INLINE void lookup(const typename HT::KeyType & key, Actor &lookupAct) {
            auto hi = blockedHT->hashInfo(key);
            subLookupAcc[hi.pos].lookup(hi.storeVal, lookupAct);
        }
    };

    template<class Actor>
    LookupAccessor<Actor> getLookupAccessor() const {
        return LookupAccessor<Actor>(this);
    }


    template<class Actor>
    class ValueAccessor {
         BlockedHT_nonParallel *blockedHT = nullptr;
         std::vector<typename BlockedHT_nonParallel::SubHT::template ValueAccessor<Actor>> subValueAcc;

        public:
        ValueAccessor(BlockedHT_nonParallel *blockedHT)
            : blockedHT(blockedHT)
        {
            for (auto *subHT : blockedHT->getSubHTs()) {
                subValueAcc.push_back(subHT->template getValueAccessor<Actor>());
            }
        }

        ValueAccessor(ValueAccessor &&other)
            : subValueAcc(std::move(other.subValueAcc))
        {
            std::swap(blockedHT, other.blockedHT);
        }

        ~ValueAccessor() {}

        FORCE_INLINE void value(const KeyType & key, Actor &valueAct) {
            auto hi = blockedHT->hashInfo(key);
            subValueAcc[hi.pos].value(hi.storeVal, valueAct);
        }
    };

    template<class Actor>
    ValueAccessor<Actor> getValueAccessor() {
        return ValueAccessor<Actor>(this);
    }

};

}

}

#endif // BLOCKED_HT_NON_PARALLEL_H
