#ifndef STOREINFO_H
#define STOREINFO_H

#include <cstddef>

template<class StoreType>
struct HashInfo {
	size_t pos;
	StoreType storeVal;
};

#endif // STOREINFO_H
