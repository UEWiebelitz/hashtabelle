#pragma once

#include <cstddef>
#include <cmath>
#include <bitset>
#include <type_traits>
#include <iostream>

#include "storeinfo.h"
#include "template/counters/bitcounter.h"

#include "compile_hints/inline_enforce.h"

namespace Runtime {

template<class StoreType>
struct HF_Fingerprint_KeyStoreType {
	StoreType val;

    FORCE_INLINE HF_Fingerprint_KeyStoreType(const StoreType &val = 0)
		: val(val)
	{}

    FORCE_INLINE operator StoreType() const {
		return val;
	}

    FORCE_INLINE bool operator ==(const HF_Fingerprint_KeyStoreType &other) const {
		return val == other.val;
	}

    FORCE_INLINE HF_Fingerprint_KeyStoreType& operator=(const StoreType &other) {
		val = other;
		return *this;
	}

//    FORCE_INLINE HF_Fingerprint_KeyStoreType& operator=(const HF_Fingerprint_KeyStoreType &other) {
//		val = other.val;
//		return *this;
//	}
};

template<class KeyT>
class HF_Fingerprint {
//	static_assert (popCount(KeyT::bitCount()) == 1, "bitCount has to be a power of 2!");

	size_t m_mult;
	size_t m_bitCount;
	size_t m_universe;
	size_t m_inversemodpow;
	size_t m_mask;
	size_t m_q;

	KeyT m_key;

//	static inline constexpr size_t q = (KeyT::bitCount() / 2);
//	[[gnu::always_inline]] static inline constexpr size_t mask() {
//		return (size_t(1) << KeyT::bitCount()) -1;
//	}

//	static inline size_t universe(size_t bitCount) {
//		return (size_t(1) << bitCount);
//	}

	size_t calcPow(size_t val, size_t power) const {
		if (power == 0) {
			return 1;
		}
		if (power % 2 == 0) {
			size_t res = calcPow(val, power/2);
			return res * res;
		}
		return val * calcPow(val, power-1);
	}

public:
//	typedef KeyT KeyType;
	typedef size_t StoreWordType;
	typedef HF_Fingerprint_KeyStoreType<StoreWordType> KeyStoreType;

	typedef HashInfo<KeyStoreType> StoreInfo;

//	HF_Fingerprint(const HF_Fingerprint &) = default;

	HF_Fingerprint(std::size_t mult, std::size_t valueBitCount, const KeyT &key)
		: m_mult(mult)
		, m_bitCount(valueBitCount & 1 ? valueBitCount +1 : valueBitCount)
		, m_universe(size_t(1) << m_bitCount)
		, m_q(m_bitCount /2)
        , m_mask (m_universe -1)
		, m_key(key)
	{
		if (valueBitCount & 1) {
			std::cerr << "valueBitCount " << valueBitCount << " is odd -> increased to " << m_bitCount << std::endl;
		}

		assert(m_mult & 1 && "the hash multiplier must be odd!");

		m_inversemodpow = calcPow(mult, m_universe/2-1) % m_universe;
//#ifdef DEBUG_TESTS
//		assert (calcPow(4, q) == universe() && "universe must be a power of 4");

//		assert ((m_mult*m_inversemodpow) % universe(m_bitCount) == 1); //, f"either a={a} is even or m={m} is not a power of 2"
//#endif
	}

    FORCE_INLINE size_t hashPos(const KeyT &code, size_t npages) const {
		size_t swap = ((code << m_q) ^ (code >> m_q)) & m_mask;
		swap = (m_mult * swap) & m_mask;
		return  swap % npages;
	}

    FORCE_INLINE StoreInfo hash(const typename KeyT::WordType &key, size_t npages) const {
//		typename KeyT::WordType key = code;
        size_t swap = ((key << m_q) ^ (key >> m_q)) & m_mask; ///@todo elias: Why xor and not just or?
		swap = (m_mult * swap) & m_mask;
		size_t page = swap % npages;
		KeyStoreType fingerprint (swap / npages);

		StoreInfo si {page, fingerprint};
//		si.pos = page;
//		si.storeVal = fingerprint;
		return si;
	}

	FORCE_INLINE KeyT value(const StoreInfo &storeinfo, size_t npages) {
		typename KeyT::WordType key = storeinfo.storeVal.operator StoreWordType() * npages + storeinfo.pos;
		key = (m_inversemodpow * key) & m_mask;
		m_key = ((key << m_q) ^ (key >> m_q)) & m_mask;
		return m_key;
	}

    FORCE_INLINE typename KeyT::WordType value_word(const StoreInfo &storeinfo, size_t npages) {
		typename KeyT::WordType key = storeinfo.storeVal.operator StoreWordType() * npages + storeinfo.pos;
		key = (m_inversemodpow * key) & m_mask;
		return ((key << m_q) ^ (key >> m_q)) & m_mask;
	}

//	static constexpr unsigned long long valueSize = sizeof (KeyType);

    inline size_t fpBitCount(size_t npages) const {
		return countBits(m_mask / npages);
	}
};
}
