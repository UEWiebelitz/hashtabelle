#ifndef DNA_ALPH_H
#define DNA_ALPH_H

#include <cassert>
#include <limits>

class DNA_Alph {
	typedef char CType;

public:
	static constexpr std::size_t alphSize = 4;

	DNA_Alph() {
		for(unsigned int i=0; i<sizeof (alphabet); ++i) {
			alphabet[i] = 5;
		}

		alphabet[std::size_t('a')] = 0;
		alphabet[std::size_t('A')] = 0;

		alphabet[std::size_t('c')] = 1;
		alphabet[std::size_t('C')] = 1;

		alphabet[std::size_t('g')] = 2;
		alphabet[std::size_t('G')] = 2;

		alphabet[std::size_t('t')] = 3;
		alphabet[std::size_t('T')] = 3;

		revAlph[0] = 'A';
		revAlph[1] = 'C';
		revAlph[2] = 'G';
		revAlph[3] = 'T';
	}

	inline CType encode(CType c) const {
#ifdef DEBUG_TESTS
		if (std::size_t(alphabet[std::size_t(c)]) >= alphSize) {
			assert(false && "character is not part of the alphabet");
		}
#endif

		return alphabet[std::size_t(c)];
	}

    inline CType complementOfEncoded(CType c) const {
        return (~c) & (alphSize -1);
	}

	inline CType decode(CType c) const {
		if (c < CType(alphSize)) {
			return revAlph[std::size_t(c)];
		}

		assert(false && "c is out of range");
		return 0;
	}

	inline bool isValid(CType c) const {
		return alphabet[std::size_t(c)] != 5;
	}

private:
	CType alphabet[std::numeric_limits<CType>::max()];
	CType revAlph[alphSize];
};

#endif // DNA_ALPH_H
