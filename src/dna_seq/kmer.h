#pragma once

#include <type_traits>
#include <algorithm>
#include <string>
#include <cassert>

#include "template/counters/countbits.h"

namespace Runtime {

template<class Alph>
class Kmer {
public:
	typedef size_t WordType;

protected:
	WordType word = 0;
	size_t charCount = 0;
	size_t k;
	size_t mask;

public:
#if __cplusplus >= 201703L
    static inline constexpr size_t bitPerChar = size_t(countBits(Alph::alphSize -1));
#else
    static constexpr size_t bitPerChar = size_t(countBits(Alph::alphSize -1));
#endif

//	static_assert (bitPerChar * k <= sizeof (size_t) * 8, "k is too large for word type");

	inline Kmer(size_t k) : k(k)
	{
		mask = (size_t(1) << (k*bitPerChar)) -1;
	}

	inline Kmer(size_t k, WordType word)
		: word(word)
		, k(k)
	{
#ifdef DEBUG_TESTS
		assert(bitPerChar * k <= sizeof (size_t) * 8 &&  "k is too large for word type");
#endif
		mask = (size_t(1) << (k*bitPerChar)) -1;
	}

	static const Alph alph;

    auto getK() const {
		return k;
	}

	inline operator WordType() const {
		return word;
	}

	inline bool isComplete() const {
		return charCount == k;
	}

	inline void reset() {
		word = 0;
		charCount = 0;
	}

	inline Kmer<Alph>& operator<< (char c)
	{
		word = ((word << bitPerChar) | alph.encode(c)) & mask;
		charCount = std::min(charCount +1, k);
		return *this;
	}

	inline char operator[](size_t idx) const {
		return alph.decode((word >> (bitPerChar * (charCount -1 - idx))) & ((size_t(1) << bitPerChar) -1));
	}

    char getEncoded(size_t idx) {
        return (word >> (idx * bitPerChar)) & ((1 << bitPerChar) -1);
    }

    void set(size_t idx, char val) {
        word &= ~(((1 << bitPerChar)-1) << (idx * bitPerChar));
        word |= WordType(val) << ((idx)*bitPerChar);
    }

    void inc(size_t idx) {
        char val = getEncoded(idx);
        ++val;
        if (val == Alph::alphSize) {
            val = 0;
        }

        set(idx, val);
    }

	bool operator==(const Kmer &other) const {
		return word == other.word && charCount == other.charCount;
	}

	inline Kmer& operator=(WordType word) {
		this->word = word;
		charCount = k;
		return *this;
	}

	inline size_t bitCount() {
		return k*bitPerChar;
	}
	
	std::string toString() const {
		std::string str;
		str.resize(charCount);
		for (size_t i=0; i<charCount; ++i) {
			str[i] = operator[](i);
		}

		return str;
	}
};

template<class Alph> const Alph Kmer<Alph>::alph = Alph();

}
