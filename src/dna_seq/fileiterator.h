#ifndef FILEITERATOR_H
#define FILEITERATOR_H

#include <string>
#include <fstream>
#include <exception>
#include <iostream>
#include <mutex>
#include <semaphore.h>
#include <condition_variable>
#include <thread>
#include <utility>

#include "thread_communication/circular_buffer.h"

#include "compile_hints/likely_hint.h"

class FileIterator {
	class CharBuffer {
		size_t buffSize;
		char *buff = nullptr;

	public:
		bool isReady = false;
		bool isWriteable = true;
		size_t readLen = 0;
		bool eof = false;

		CharBuffer(size_t buffSize)
			: buffSize(buffSize)
			, buff(new char[buffSize]) {}

		CharBuffer(CharBuffer &&other) = default;

		~CharBuffer() {
			delete[] buff;
		}

		inline operator char*() {
			return buff;
		}

		inline size_t size() const {return buffSize;}
	};

	std::ifstream ifs;
	size_t readPos = 0;

	CharBuffer buffer1;
	CharBuffer buffer2;
	CharBuffer *currReadable = nullptr;

	std::mutex mtx;
	std::condition_variable cond_var_readable;
	std::condition_variable cond_var_writeable;

	std::thread fileReaderThread;

	bool stopBufferWriter = false;

public:
	FileIterator(const std::string &filename, size_t buffSize = 10/*1024000*/)
		: ifs(filename, std::ifstream::in)
		, buffer1(buffSize)
		, buffer2(buffSize)
	{
		if (!ifs.is_open()) {
			std::cout << "could not open file" << std::endl;
            throw std::invalid_argument("could not open file " + filename);
		}

		auto bufferWriter = [this, buffSize](){
			while (!ifs.eof()) {
				std::unique_lock<std::mutex> lock(mtx);

				while (!buffer1.isWriteable && !buffer2.isWriteable) {  // loop to avoid spurious wakeups
					cond_var_writeable.wait(lock);
					if (stopBufferWriter) {
						return;
					}
				}

				CharBuffer *write;
				if (buffer1.isWriteable) {
					write = &buffer1;
				}
				else {
					write = &buffer2;
				}

				ifs.read(write->operator char *(), buffSize);
				write->readLen = ifs.gcount();

				write->isWriteable = false;
				write->isReady = true;
				write->eof = ifs.eof();
				cond_var_readable.notify_one();
			}
		};

		fileReaderThread = std::thread(bufferWriter);

		std::unique_lock<std::mutex> lock(mtx);
		while (!buffer1.isReady && !buffer2.isReady) {  // loop to avoid spurious wakeups
			cond_var_readable.wait(lock);
		}

		if (buffer1.isReady) {
			currReadable = &buffer1;
		}
		else {
			currReadable = &buffer2;
		}

		readPos = 0;
	}

	~FileIterator()
	{
		stopBufferWriter = true;
		cond_var_writeable.notify_one();
		fileReaderThread.join();
	}

	inline FileIterator &operator++() {
		++readPos;

		while (unlikely(!currReadable->eof && readPos >= currReadable->readLen)) {
			std::unique_lock<std::mutex> lock(mtx);
			currReadable->isReady = false;
			currReadable->isWriteable = true;

			cond_var_writeable.notify_one();

			while (!buffer1.isReady && !buffer2.isReady) {  // loop to avoid spurious wakeups
				cond_var_readable.wait(lock);
			}

			if (buffer1.isReady) {
				currReadable = &buffer1;
			}
			else {
				currReadable = &buffer2;
			}
			readPos = 0;
		}

		return *this;
	}

	inline char operator*() const {
		return currReadable->operator char *()[readPos];
	}

	inline bool eof() const {
		return currReadable->eof && readPos >= currReadable->readLen;
	}
};

#endif // FILEITERATOR_H
