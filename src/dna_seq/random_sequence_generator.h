#pragma once

#include <type_traits>
#include <string>
#include <vector>

template<class Kmer, class ...KmerArgs>
class RandomSequenceGenerator {
	std::size_t m_alreadyRead;
	std::size_t m_readLen = 0;
	Kmer kmer;
	std::vector<typename Kmer::WordType> m_sequence;

public:
	/**
	 * @brief RandomSequenceGenerator generates a random
	 * @param file
	 * @param readLength
	 * @param sequenceSize
	 */
	RandomSequenceGenerator(std::size_t readLength, std::size_t sequenceLength = 10240, KmerArgs ...kmerArgs)
		: m_readLen(readLength)
		, kmer(std::forward<KmerArgs>(kmerArgs)...)
	{
		m_sequence.reserve(sequenceLength);

		while (!kmer.isComplete()) {
			kmer << std::rand() % kmer.alph.alphSize;
		}

		for (size_t i=0; i<sequenceLength; ++i) {
			m_sequence.push_back(kmer);
			kmer << std::rand() % kmer.alph.alphSize;
		}
	}

public:
	inline RandomSequenceGenerator &operator++() {
		++m_alreadyRead;
	}

	inline Kmer operator*() const {
		return m_sequence[m_alreadyRead % m_sequence.size()];
	}

	inline bool eof() const {
		return m_alreadyRead >= m_readLen;
	}
};
