#pragma once

#include "kmer.h"

namespace Runtime {

template<class Alph/*, class Canonize*/>
class CanonizedKmer : public Kmer<Alph>
{
protected:
    using Base = Kmer<Alph>;

    typename Base::WordType reverse = 0;
    size_t shift;

public:
    inline CanonizedKmer(size_t k)
        : Base(k)
    {
        shift = (k -1) * Base::bitPerChar;
    }

    inline CanonizedKmer(size_t k, typename Base::WordType word)
        : Base(k)
    {
        shift = (k -1) * Base::bitPerChar;
        operator=(word);
    }

    inline bool operator==(const CanonizedKmer &other) const {
        return operator typename Base::WordType() == other.operator typename Base::WordType() && Base::charCount == other.Base::charCount;
    }
    
    /**
     * @brief inc replaces the Character at the given position with the next one in the order A->C->G->T->A.
     * For the reversed kmer the position is related to the left most character,
     * for the original kmer the position is related to the right most character.
     * @param idx index of character that should be changed
     */
    void inc(size_t idx) {
        Base::inc(idx);
        
        reverse &= ~((Base::alph.alphSize -1) << ((Base::k-idx -1) * Base::bitPerChar));
        char val = typename Base::WordType(Base::alph.complementOfEncoded(Base::getEncoded(idx)));

        reverse |= typename Base::WordType(val) << ((Base::k-idx -1)*Base::bitPerChar);
    }

    ///@todo elias: this function does not work as intended
    //	inline CanonizedKmer& operator=(typename Base::WordType word)
    //	{
    //		Base::operator=(word);
    //		reverse = 0;
    //		constexpr size_t lastCharMask = (typename Base::WordType(1) << Base::bitPerChar) -1;
    //		for (size_t i=0; i<Base::k; ++i) {
    //			size_t lastChar = word & lastCharMask;
    //			size_t shift = Base::bitPerChar * (Base::k -i -1);
    //			size_t lastCharComplement = Base::alph.complementEncode(Base::alph.decode(lastChar));
    //			reverse |= lastCharComplement << shift;
    //			word >>= Base::bitPerChar;
    //		}

    //		return *this;
    //	}

    inline void reset()
    {
        Base::reset();
        reverse = 0;
    }

    [[gnu::always_inline]] CanonizedKmer& operator<< (char c)
    {
        Base::operator<<(c);
        reverse >>= Base::bitPerChar;
        reverse |= (typename Base::WordType(Base::alph.complementOfEncoded(Base::getEncoded(0))) << shift);
        return *this;
    }

    inline typename Base::WordType reverseValue() const {
        return reverse;
    }

    [[gnu::always_inline]] operator typename Base::WordType() const {
        return std::max(Base::word, reverse);
    }

    char reverseGetAt(size_t pos) const {
//        return Base::alph.decode((reverse >> (Base::bitPerChar *pos)) & ((1 << Base::bitPerChar) -1));
        return Base::alph.decode((reverse >> (Base::bitPerChar * (Base::charCount -1 - pos))) & ((size_t(1) << Base::bitPerChar) -1));
    }

    std::string reverseToString() const {
        std::string str;
        str.resize(Base::charCount);
        for (size_t i=0; i<Base::charCount; ++i) {
            str[i] = reverseGetAt(i);
        }

        return str;
    }
};

}
