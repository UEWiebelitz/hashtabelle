#pragma once

#include <type_traits>
#include <mutex>

template<class InputIterator, class Type>
class Cached_KmerIterator{
		InputIterator &kmerIterator;
		std::mutex *mtx;
		size_t cacheSize;
		size_t posInCache = 0;
		size_t readable;
		bool lastChunk = false;
		Type *cache;

public:
		Cached_KmerIterator(InputIterator &kmerIterator, std::mutex *mtx, size_t cacheSize)
				: kmerIterator(kmerIterator)
				, mtx(mtx)
				, cacheSize(cacheSize)
		{
				cache = new Type[cacheSize];

				mtx->lock();
				for (readable=0; readable<cacheSize && !kmerIterator.eof(); ++readable, ++kmerIterator) {
						cache[readable] = *kmerIterator;
				}
				lastChunk = kmerIterator.eof();
				mtx->unlock();
		}

		~Cached_KmerIterator() {
				delete cache;
		}

		inline bool eof() const {
				return lastChunk && posInCache == readable;
		}

		inline Cached_KmerIterator &operator++() {
				++posInCache;

				if (unlikely(posInCache == readable)) {
						mtx->lock();
						for (readable=0; readable<cacheSize && !kmerIterator.eof(); ++readable, ++kmerIterator) {
								cache[readable] = *kmerIterator;
						}

						posInCache = 0;
						lastChunk = kmerIterator.eof();
						mtx->unlock();
				}

				return *this;
		}

		inline const Type &operator*() const {
				return cache[posInCache];
		}
};
