#ifndef KMER_READER_H
#define KMER_READER_H

#include "fileiterator.h"
#include "compile_hints/likely_hint.h"

#include <string>

template<class Kmer, class ...KmerArgs>
class KmerIterator {
	FileIterator fIter;
	bool atEnd = false;
	Kmer kmer;

public:
	KmerIterator(const std::string &file, size_t buffSize = 10240, KmerArgs... kmerArgs)
		: fIter(file, buffSize)
		, kmer(std::forward<KmerArgs>(kmerArgs)...)
	{
		bool firstRun = true;

		do {
			if (unlikely(fIter.eof())) {
				atEnd = true;
				return;
			}

			skipNewLine();

			if (unlikely(*fIter == '>')) {
				skipCurrentLine();
				kmer.reset();
			}

			if (unlikely(fIter.eof())) {
				atEnd = true;
				return;
			}

			if (unlikely(firstRun)) {
				firstRun = false;
			}
			else {
				++fIter;
			}

			skipNewLine();

			char c = *fIter;
			if (unlikely(!kmer.alph.isValid(c))) {
				kmer.reset();
				++fIter;
			}
			else {
				kmer << c;
			}
		} while(!kmer.isComplete());
	}

public:
	inline KmerIterator &operator++() {
		do {
			++fIter;
			if (unlikely(skipNewLine())) {
				if (*fIter == '>') {
					skipCurrentLine();
					kmer.reset();
				}
			}

			if (unlikely(fIter.eof())) {
				atEnd = true;
				return *this;
			}

			char c = *fIter;
			if (unlikely(!kmer.alph.isValid(c))) {
				kmer.reset();
			}
			else {
				kmer << c;
			}
		} while(unlikely(!kmer.isComplete()) && likely(!fIter.eof()));

		return *this;
	}

	inline Kmer operator*() const {
		return kmer;
	}

	inline bool eof() const {
		return atEnd;
	}

	/**
	 * @brief skipNewLine
	 * @return true if a newline was skipped
	 */
	[[gnu::always_inline]] bool skipNewLine() {
		bool newLine = false;
		while(unlikely(*fIter == '\n' || *fIter == '\r')) {
			  ++fIter;
			if (fIter.eof()) {
				atEnd = true;
				return true;
			}
			newLine = true;
		}

		return newLine;
	}

	/**
	 * @brief skipCurrentLine skips the whole current line including following empty lines
	 */
	[[gnu::always_inline]] void skipCurrentLine() {
		while(likely(*fIter != '\n' && *fIter != '\r')) {
			  ++fIter;
			if (fIter.eof()) {
				atEnd = true;
				return;
			}
		}
		skipNewLine();
	}
};

#endif // KMER_READER_H
