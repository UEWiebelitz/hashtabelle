TEMPLATE = app
#CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

QT += testlib

CONFIG += c++14
CONFIG += testcase


#DEFINES += LESS_INSTANCES

SOURCES += $$PWD/main.cpp

INCLUDEPATH += ../external/args/

include (src.pri)
