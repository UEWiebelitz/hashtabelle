if (${Args_INCLUDE_DIRS})
    message("already found Args")
else()
    message("trying to locate Args")
    set(Args_INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/external")

    find_path(Args_INCLUDE_DIRS args.hxx
            PATHS ${Args_INSTALL_DIR}/args /usr/local/args /opt/args)

    if (NOT ${Args_INCLUDE_DIRS} MATCHES "Args_INCLUDE_DIRS-NOTFOUND")
        message(STATUS "Found Args-Library: ${Args_INCLUDE_DIRS}")
    else ()
        set(Args_INCLUDE_DIRS ${Args_INSTALL_DIR}/args)

        message(STATUS "Removing remains of previously downloaded Args versions")
        file(REMOVE_RECURSE ${Args_INSTALL_DIR})
        file(MAKE_DIRECTORY ${Args_INSTALL_DIR}/)

        add_custom_command(
            OUTPUT args_git_download
            COMMAND cd ${Args_INSTALL_DIR}/ && git clone https://github.com/Taywee/args.git
        )

        add_custom_target(Args-download DEPENDS args_git_download)
        add_dependencies(external-downloads Args-download)
    endif ()

    include_directories(${Args_INCLUDE_DIRS})
endif()
