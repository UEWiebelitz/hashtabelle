# - Find GTest
# 
# Author: Jonas Ellert
#
# Copyright (c) 2016 TU Dortmund PG605
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# This module tries to locate GTest on the system. If it is not found, it will
# automatically be downloaded and built from GitHub.
#
# The following variables will be set:
#
# GTEST_FOUND and GTest_FOUND
# GTest_INCLUDE_DIRS
# GTest_LIBRARIES (use this, if your tests come with a main-method implementation)
# GTest_Main_LIBRARIES (use this, if your tests do not come with a main-method implementation)
# GTest_INSTALL_DIR and GTest_DOWNLOAD_DIR (if download is necessary)

include(FindPackageHandleStandardArgs)

set(GTest_DOWNLOAD_DIR "${CMAKE_CURRENT_BINARY_DIR}/tmp/GTest-external")
set(GTest_INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/GTest")

message(STATUS)
message(STATUS "Trying to locate package: GTest")

# Temporarily disable REQUIRED if set
set(GTest_FIND_REQUIRED_BACKUP ${GTest_FIND_REQUIRED})
set(GTest_FIND_REQUIRED 0)

# First dir to be searched for GTest is in build dir
set(GTest_ROOT_DIR ${GTest_INSTALL_DIR})

find_path(GTest_INCLUDE_DIRS gtest/gtest.h
        PATHS ${GTest_ROOT_DIR}/include /usr/local/include /usr/include)

find_library(GTest_LIBRARIES gtest
        PATHS ${GTest_ROOT_DIR}/lib /usr/local/lib /usr/lib)
find_package_handle_standard_args(GTest DEFAULT_MSG GTest_LIBRARIES GTest_INCLUDE_DIRS)

find_library(GTest_Main_LIBRARIES gtest_main
        PATHS ${GTest_ROOT_DIR}/lib /usr/local/lib /usr/lib)
find_package_handle_standard_args(GTest_Main DEFAULT_MSG GTest_Main_LIBRARIES)

message("GTEST_FOUND: ${GTEST_FOUND}")

# Restore REQUIRED
set(GTest_FIND_REQUIRED ${GTest_FIND_REQUIRED_BACKUP})

if (GTEST_FOUND AND GTEST_MAIN_FOUND)
    set(GTest_Main_LIBRARIES ${GTest_Main_LIBRARIES} ${GTest_LIBRARIES})
    message(STATUS "Found all GTest Libraries")
else ()
    message(STATUS "Could NOT locate GTest on system -- preparing download")

    set(GTest_INCLUDE_DIRS ${GTest_INSTALL_DIR}/include)
    set(GTest_LIBRARIES ${GTest_INSTALL_DIR}/lib/libgtest.a)
    set(GTest_Main_LIBRARIES ${GTest_INSTALL_DIR}/lib/libgtest_main.a ${GTest_LIBRARIES})

    message(STATUS "Removing remains of previously downloaded GTest versions")
    file(REMOVE_RECURSE ${GTest_DOWNLOAD_DIR})
    file(REMOVE_RECURSE ${GTest_INSTALL_DIR})

    include(ExternalProject)
    ExternalProject_Add(
        GTest-download
        PREFIX ${GTest_DOWNLOAD_DIR}
        GIT_REPOSITORY https://github.com/google/googletest.git
        PATCH_COMMAND ""
        UPDATE_COMMAND ""
        CONFIGURE_COMMAND ""
        BUILD_COMMAND cd googletest && mkdir build && cd build && cmake .. && make all
        BUILD_IN_SOURCE TRUE
        INSTALL_COMMAND
        mv googletest ${GTest_INSTALL_DIR}
#        cd googletest/build &&
#        mv ../include "${GTest_INSTALL_DIR}/include" &&
#        mv ../
#        INSTALL_DIR "${GTest_INSTALL_DIR}"
    )
    add_dependencies(external-downloads2 GTest-download)
    set_target_properties (GTest-download PROPERTIES IMPORTED_LOCATION ${GTest_INSTALL_DIR}/${GTest_LIBRARIES})

    FIND_PACKAGE_HANDLE_STANDARD_ARGS(GTest DEFAULT_MSG GTest_INCLUDE_DIRS)
endif ()
