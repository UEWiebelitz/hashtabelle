#ifndef BITVAL_H
#define BITVAL_H

class BitVal {
	unsigned int val;

public:
	BitVal (unsigned int val) : val(val) {}
	operator unsigned int () const {return val;}

	inline static constexpr unsigned int bitCount() {
		return 5;
	}
};

class BitVal7 {
	unsigned int val;

public:
	BitVal7 (unsigned int val, bool bitMasking = false) : val(bitMasking ? val & mask() : val) {}

	operator unsigned int () const {return val;}

	inline static constexpr unsigned int bitCount() {
		return 7;
	}

	inline static constexpr unsigned int mask() {
		return (1 << 7) -1;
	}
};

class BitVal13 {
	unsigned int val;

public:
	BitVal13 (unsigned int val) : val(val) {}
	operator unsigned int () const {return val;}

	inline static constexpr unsigned int bitCount() {
		return 13;
	}
};

#endif // BITVAL_H
