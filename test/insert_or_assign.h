#pragma once

#include <unordered_map>

template<class Key, class Val>
void insert_or_assign(std::unordered_map<Key, Val> &map, const Key &key, const Val &val) {
	auto it = map.find(key);
	if (it != map.end()) {
		map.erase(it);
	}
	map.insert({key, val});
}
