#pragma once

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>

#define protected public
#include "dna_seq/canonized_kmer.h"
#include "dna_seq/kmer_reader.h"
#include "dna_seq/dna_alph.h"

#include "reverse_kmer.h"

#include "runtime/hashfunctions/fingerprint.h"
#include "runtime/hashtables/replacing.h"
#include "runtime/page_layouts/ht_page_container.h"

#include <unordered_set>
//#undef protected

using namespace testing;

size_t countKmersRawString(const std::string &filename, size_t k, size_t expectedKmers) {
	std::ifstream t(filename);
	std::stringstream buffer;
	buffer << t.rdbuf();

	std::string sequence = buffer.str();
	std::unordered_set<std::string> kmerSet(expectedKmers);

	size_t processed = 0;
	for (size_t i=0; i<=sequence.size() - k; ++i) {
		std::string kmer = sequence.substr(i, k);
		std::string revKmer = reverseKmer(kmer);

		std::string canonKmer = std::max(kmer, revKmer);

		kmerSet.insert(canonKmer);
		++processed;
	}

	std::cout << "processed " << processed << " kmers" << std::endl;

	return kmerSet.size();
}

size_t countKmers(const std::string &filename, size_t k, size_t expectedKmers) {
	KmerIterator<Runtime::CanonizedKmer<DNA_Alph>, size_t> fileIterator(filename, 102400, k);
	std::unordered_set<size_t> kmerSet(expectedKmers);

	size_t processed = 0;
	while(!fileIterator.eof()) {
		kmerSet.insert(*fileIterator);
		++fileIterator;
		++processed;
	}

	std::cout << "processed " << processed << " kmers" << std::endl;

	return kmerSet.size();
}

#ifndef MANUAL_RAND
size_t countKmersCompareHTs(const std::string &filename, size_t k, size_t expectedKmers) {
	KmerIterator<Runtime::CanonizedKmer<DNA_Alph>, size_t> fileIterator(filename, 102400, k);

	typedef Runtime::CanonizedKmer<DNA_Alph> KeyT;
	typedef Runtime::HF_Fingerprint<KeyT> FpHf;
	using DataContainer = typename Runtime::HT_PageContainer<Runtime::WordPacking_access>;
	typedef Runtime::Hashtable::Replacing<size_t, size_t, FpHf, DataContainer> RuntimeHT;

	std::vector<FpHf> hashFunctions;
	size_t mults[] = {599, 601, 23};
	for (auto mult : mults) {
		hashFunctions.push_back({mult, 2*k, KeyT(k)});
	}

	DataContainer dataContainer(expectedKmers, 8, hashFunctions.size(), 0, [&](size_t hashPositionCnt) {return hashFunctions.front().fpBitCount(hashPositionCnt);});
	RuntimeHT ht(hashFunctions, dataContainer);
	std::unordered_set<size_t> kmerSet(expectedKmers);

	size_t processed = 0;
	while(!fileIterator.eof()) {
		auto lookup = ht.lookup(*fileIterator);

		if ((lookup == LOOKUP_EXISTS) != (kmerSet.find(*fileIterator) != kmerSet.end())) {
			std::cout << "lookup differs" << std::endl;
		}

		auto result = ht.insert(*fileIterator);

		if ((result.res == APPEND_EXISTS) != (kmerSet.find(*fileIterator) != kmerSet.end())) {
			std::cout << "hts differ in kmer: " << (*fileIterator).toString() << std::endl;
			std::cout << "result: " << result.res << std::endl;
		}
		EXPECT_EQ(result.res == APPEND_EXISTS, kmerSet.find(*fileIterator) != kmerSet.end());

		kmerSet.insert(*fileIterator);
		++fileIterator;
		++processed;
	}

	std::cout << "processed " << processed << " kmers" << std::endl;

	return ht.elemCount();
}
#endif

TEST(CountKmers, ebola_compareHTs)
{
	size_t cnt = countKmers(TEST_DATA_PATH "ebola.fna", 25, 20000);

	std::cout << "count of kmers: " << cnt << std::endl;
	EXPECT_EQ(size_t(18851), cnt);
}

#ifndef MANUAL_RAND
TEST(CountKmers, eColi_compareHts)
{
	size_t cnt = countKmersCompareHTs(TEST_DATA_PATH "Escherichia_coli_str_k_12_substr_mg1655.ASM584v2.dna.toplevel.fa", 25, 4600000);

	std::cout << "count of kmers: " << cnt << std::endl;
	EXPECT_EQ(size_t(4548910), cnt);
}

TEST(CountKmers, ebola)
{
	size_t cnt = countKmersCompareHTs(TEST_DATA_PATH "ebola.fna", 25, 20000);

	std::cout << "count of kmers: " << cnt << std::endl;
	EXPECT_EQ(size_t(18851), cnt);
}
#endif


TEST(CountKmers, eColi)
{
	size_t cnt = countKmers(TEST_DATA_PATH "Escherichia_coli_str_k_12_substr_mg1655.ASM584v2.dna.toplevel.fa", 25, 4600000);

	std::cout << "count of kmers: " << cnt << std::endl;
	EXPECT_EQ(size_t(4548910), cnt);
}

TEST(CountKmers, eColi_raw_kmerIterator)
{
	size_t cnt = countKmers(TEST_DATA_PATH "sequence_raw_no_linebreak_Escherichia_coli_str_k_12_substr_mg1655.ASM584v2.dna.toplevel.fa", 25, 4600000);

	std::cout << "count of kmers: " << cnt << std::endl;
	EXPECT_EQ(size_t(4548910), cnt);
}

TEST(CountKmers, eColi_raw)
{
	const size_t k = 25;
	const std::string filename (TEST_DATA_PATH "sequence_raw_no_linebreak_Escherichia_coli_str_k_12_substr_mg1655.ASM584v2.dna.toplevel.fa");
	const size_t expectedKmers = 4600000;

	size_t cnt = countKmersRawString(filename, k, expectedKmers);

	std::cout << "count of kmers: " << cnt << std::endl;
	EXPECT_EQ(size_t(4548910), cnt);
}
