#pragma once

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>

#define protected public
#include "template/bitshifting/combinedwords.h"
#include "runtime/bitshifting/combinedwords.h"

#include "template/bitshifting/wordpacking.h"
#include "runtime/bitshifting/wordpacking.h"

//#undef protected

using namespace testing;

#include "bitval.h"
#include "template/counters/emptydummy.h"

TEST(Compare_CombinedWords_impl, randomInsert) {
	CombinedWords<size_t, BitVal7, BitVal13, EmptyDummy> templ;
	Runtime::CombinedWords<3> runt({7, 13, 0});
	size_t runTimeVal = 0;


	for (int i=0; i < 10000; ++i) {
		size_t idx = rand() % 3;
		size_t value = rand();
		size_t modVal = 0;

		switch (idx) {
		case 0:
			modVal = value % (1 << 7);
			templ.template set<0>(modVal);
			break;
		case 1:
			modVal = value % (1 << 13);
			templ.template set<1>(modVal);
			break;
		case 2:
			modVal = value % (1 << 0);
			templ.template set<2>(modVal);
			break;
		}

		runt.set(idx, runTimeVal, modVal);

		EXPECT_EQ(templ.get<0>(), runt.get(0, runTimeVal));
		EXPECT_EQ(templ.get<1>(), runt.get(1, runTimeVal));
		EXPECT_EQ(templ.get<2>(), runt.get(2, runTimeVal));
	}
}

TEST(Compare_Wordpacking_impl, randomInsert) {
	WordPacking<size_t, 8, BitVal13> templ;
	Runtime::WordPacking_access runt(13, 8);
	size_t runTimeVal[8] = {0,0,0,0,0,0,0,0};

	for (int i=0; i < 10000; ++i) {
		size_t idx = rand() % 3;
		size_t value = rand() % (1 << 13);

		templ.store(idx, value);
		runt.store(runTimeVal, idx, value);

		for (size_t j=0; j<8; ++j) {
			EXPECT_EQ(templ.arr[j], runTimeVal[j]);
		}
//		EXPECT_EQ(templ.get<0>(), runt.get(0, runTimeVal));
//		EXPECT_EQ(templ.get<1>(), runt.get(1, runTimeVal));
//		EXPECT_EQ(templ.get<2>(), runt.get(2, runTimeVal));
	}
}
