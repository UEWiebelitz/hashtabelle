#pragma once

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>

#define protected public

#include "dna_seq/kmer_reader.h"
#include "dna_seq/kmer.h"
#include "dna_seq/dna_alph.h"

#include "runtime/dna_seq/canonized_kmer.h"
#include "dna_seq/dna_alph.h"

#include "reverse_kmer.h"

#include <unordered_set>

TEST(kmer_reader, readShortFile) {
	constexpr int k = 5;
	typedef DNA_Alph Alph;

	KmerIterator<Kmer<k, Alph>> kmerIt(TEST_DATA_PATH "short_artificial.fa");

	Kmer<k, Alph> kmer;
	kmer << 'A';
	kmer << 'C';
	kmer << 'G';
	kmer << 'A';
	kmer << 'C';

	EXPECT_EQ(kmer, *kmerIt);

	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer.reset();
	kmer << 'A';
	kmer << 'C';
	kmer << 'A';
	kmer << 'C';
	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer.reset();
	kmer << 'T';
	kmer << 'A';
	kmer << 'C';
	kmer << 'C';
	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer.reset();
	kmer << 'A';
	kmer << 'C';
	kmer << 'C';
	kmer << 'A';
	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	//start of sequence 2 here

	kmer.reset();
	kmer << 'A';
	kmer << 'C';
	kmer << 'A';
	kmer << 'C';
	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'A';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer.reset();
	kmer << 'A';
	kmer << 'A';
	kmer << 'C';
	kmer << 'T';
	kmer << 'G';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'T';
	EXPECT_EQ(kmer, *++kmerIt);

	kmer << 'C';
	EXPECT_EQ(kmer, *++kmerIt);
}


bool compareKmersRawString(const std::string &filename, size_t k) {
	std::ifstream t(filename);
	std::stringstream buffer;
	buffer << t.rdbuf();

	std::string sequence = buffer.str();

	size_t processed = 0;
	KmerIterator<Runtime::CanonizedKmer<DNA_Alph>, size_t> fileIterator(filename, 102400, k);

	bool equal = true;
	for (size_t i=0; i<=sequence.size() - k; ++i) {
		std::string kmer = sequence.substr(i, k);

		EXPECT_EQ(kmer, (*fileIterator).toString());
		equal &= (kmer == (*fileIterator).toString());

		std::string revKmer = reverseKmer(kmer);
		EXPECT_EQ(revKmer, (*fileIterator).reverseToString());

		std::string canonKmer = std::max(kmer, revKmer);


		++processed;
		++fileIterator;
	}

	std::cout << "processed " << processed << " kmers" << std::endl;

	return equal;
}

TEST(kmer_reader, compareKmers_raw_ecoli) {
	const std::string filename (TEST_DATA_PATH "sequence_raw_no_linebreak_Escherichia_coli_str_k_12_substr_mg1655.ASM584v2.dna.toplevel.fa");
	const size_t k = 25;

	EXPECT_TRUE(compareKmersRawString(filename, k));
}

bool compareKmersNormalToRawString(const std::string &filenameFA, const std::string &filenameRaw,  size_t k) {
	std::ifstream t(filenameRaw);
	std::stringstream buffer;
	buffer << t.rdbuf();

	std::string sequence = buffer.str();

	size_t processed = 0;
	KmerIterator<Runtime::CanonizedKmer<DNA_Alph>, size_t> fileIterator(filenameFA, 102400, k);

	bool equal = true;
	for (size_t i=0; i<=sequence.size() - k; ++i) {
		std::string kmer = sequence.substr(i, k);

		EXPECT_EQ(kmer, (*fileIterator).toString());
		equal &= (kmer == (*fileIterator).toString());

		std::string revKmer = reverseKmer(kmer);
		EXPECT_EQ(revKmer, (*fileIterator).reverseToString());

		std::string canonKmer = std::max(kmer, revKmer);


		++processed;
		++fileIterator;
	}

	std::cout << "processed " << processed << " kmers" << std::endl;

	return equal;
}

TEST(kmer_reader, compareKmers_fa_raw_ecoli) {
	const std::string filenameNormal (TEST_DATA_PATH "Escherichia_coli_str_k_12_substr_mg1655.ASM584v2.dna.toplevel.fa");
	const std::string filenameRaw (TEST_DATA_PATH "sequence_raw_no_linebreak_Escherichia_coli_str_k_12_substr_mg1655.ASM584v2.dna.toplevel.fa");
	const size_t k = 25;

	EXPECT_TRUE(compareKmersNormalToRawString(filenameNormal, filenameRaw, k));
}

