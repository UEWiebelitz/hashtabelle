#pragma once

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>

#define protected public
#include "template/counters/counter.h"
//#undef protected

using namespace testing;

TEST(ByteArrange, test)
{
	unsigned long long var = 0x0123456789abcdef;

	char *arr = reinterpret_cast<char*>(&var);

	for(int i=0; i<8; ++i) {
		std::cout << std::hex << std::uppercase << short(arr[i]) << std::endl;
	}
}



TEST(CounterRight, bitcount)
{
	Counter<6, unsigned int, true> cnt;
	EXPECT_EQ(3u, cnt.bitCount());
	EXPECT_EQ(0b111u, cnt.mask());
}

TEST(CounterLeft, bitcount)
{
	Counter<6, unsigned int, false> cnt;
	EXPECT_EQ(3, cnt.bitCount());
	EXPECT_EQ(0b11100000000000000000000000000000, cnt.mask());
}

TEST(CounterRight, setValue)
{
	Counter<6, unsigned int, true> cnt;
	unsigned int value = 15;
	cnt.setValue(value, 0);
	EXPECT_EQ(0u, cnt.value(value));
	EXPECT_EQ(8u, value);

	value = 0;
	cnt.setValue(value, 15);
	EXPECT_EQ(7u, cnt.value(value));
}

TEST(CounterLeft, setValue)
{
	Counter<6, unsigned int, false> cnt;
	unsigned int value = 15;
	cnt.setValue(value, 0);
	EXPECT_EQ(0u, cnt.value(value));
	EXPECT_EQ(15u, value);

	value = 0;
	cnt.setValue(value, 15);
	EXPECT_EQ(7u, cnt.value(value));
}

TEST(CounterRight, incremet)
{
	unsigned int value = 0;
	Counter<6, unsigned int, true> cnt;

	cnt.inc(value);
	EXPECT_EQ(1u, cnt.value(value));

	cnt.setValue(value, 7);
	cnt.inc(value);
	EXPECT_EQ(0u, cnt.value(value));
	EXPECT_EQ(0u, value);
}

TEST(CounterLeft, incremet)
{
	unsigned int value = 0;
	Counter<6, unsigned int, false> cnt;

	cnt.inc(value);
	EXPECT_EQ(1u, cnt.value(value));

	cnt.setValue(value, 7);
	cnt.inc(value);
	EXPECT_EQ(0u, cnt.value(value));
	EXPECT_EQ(0u, value);
}

TEST(CounterRight, decremet)
{
	unsigned int value = 0;
	Counter<6, unsigned int, true> cnt;

	cnt.dec(value);
	EXPECT_EQ(7u, cnt.value(value));

	cnt.setValue(value, 1);
	cnt.dec(value);
	EXPECT_EQ(0u, cnt.value(value));
	EXPECT_EQ(0u, value);
}

TEST(CounterLeft, decremet)
{
	unsigned int value = 0;
	Counter<6, unsigned int, false> cnt;

	cnt.dec(value);
	EXPECT_EQ(7u, cnt.value(value));

	cnt.setValue(value, 1);
	cnt.dec(value);
	EXPECT_EQ(0u, cnt.value(value));
	EXPECT_EQ(0u, value);
}

TEST(CounterAccessor, setValue)
{
	unsigned int value = 15;
	Counter<6, unsigned int, true>::CounterAccessor cnt(value);
	cnt = 0;
	EXPECT_EQ(0u, cnt.operator unsigned int());
	EXPECT_EQ(8u, value);

	value = 0;
	cnt = 15;
	EXPECT_EQ(7u, cnt.operator unsigned int());
}

TEST(CounterAccessor, incremet)
{
	unsigned int value = 0;
	Counter<6, unsigned int, true>::CounterAccessor cnt(value);

	++cnt;
	EXPECT_EQ(1u, cnt.operator unsigned int());

	cnt = 7;
	++cnt;
	EXPECT_EQ(0u, cnt.operator unsigned int());
	EXPECT_EQ(0u, value);
}

TEST(CounterAccessor, decremet)
{
	unsigned int value = 0;
	Counter<6, unsigned int, true>::CounterAccessor cnt(value);

	--cnt;
	EXPECT_EQ(7u, cnt.operator unsigned int());

	cnt = 1;
	--cnt;
	EXPECT_EQ(0u, cnt.operator unsigned int());
	EXPECT_EQ(0u, value);
}
