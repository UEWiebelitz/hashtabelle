include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG += thread
CONFIG -= qt

#QMAKE_CXXFLAGS_RELEASE += -fsave-optimization-record


CONFIG(debug, debug|release) {
    message ("debug mode")
} else {
    message ("release mode")
}

DEFINES += COUNT_ELEMS
DEFINES += TRACE_HASH_ACCESS

CONFIG(debug, debug|release) {
    DEFINES += NDEBUG
    DEFINES += DEBUG_TESTS
} else {
    DEFINES += LONG_TESTS
}

HEADERS += \
        tst_counter.h \
    tst_simple_page.h \
    tst_wordpacking.h \
    tst_wordpacking_counting.h \
    bitval.h \
    tst_simple_page.h \
    tst_hashtable_simple.h \
    tst_hashtable_replacing.h \
    tst_combindedwords.h \
    tst_reinterpretcast.h \
    tst_hashtable_multilevel.h \
    tst_hf_fingerprint.h \
    tst_kmerreader.h \
    tst_ecoli.h \
    tst_compare_wordpacking_impl.h \
    kmer.h

SOURCES += \
        main.cpp

include($$PWD/../src/src.pri)
