#pragma once

#include <string>

std::string reverseKmer(std::string kmer) {
        for (char &c : kmer) {
                switch (c) {
                case 'A':
                case 'a':
                        c = 'T';
                        break;
                case 'C':
                case 'c':
                        c = 'G';
                        break;
                case 'G':
                case 'g':
                        c = 'C';
                        break;
                case 'T':
                case 't':
                        c = 'A';
                        break;
                }
        }
        std::reverse(kmer.begin(), kmer.end());
        return kmer;
}
