#pragma once

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>

#include "bitval.h"

#define protected public
#include "template/bitshifting/wordpacking.h"


TEST(Wordpacking, store) {
	WordPacking<unsigned int, 2, BitVal> pack;
	pack.store(3, 17);
	EXPECT_EQ(0b10001000000000000000u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.store(2, 17);
	EXPECT_EQ(0b10001100010000000000u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.store(1, 17);
	EXPECT_EQ(0b10001100011000100000u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.store(0, 17);
	EXPECT_EQ(0b10001100011000110001u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.store(1, 0b01100);
	EXPECT_EQ(0b10001100011110110001u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.store(6, 0b11111);
	EXPECT_EQ(0b11000000000010001100011110110001u, pack.arr[0]);
	EXPECT_EQ(0b111u, pack.arr[1]);
}

TEST(Wordpacking, reset) {
	WordPacking<unsigned int, 2, BitVal> pack;
	pack.store(3, 17);
	EXPECT_EQ(0b10001000000000000000u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.store(2, 17);
	EXPECT_EQ(0b10001100010000000000u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.store(1, 17);
	EXPECT_EQ(0b10001100011000100000u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.store(0, 17);
	EXPECT_EQ(0b10001100011000110001u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.reset(1);
	EXPECT_EQ(0b10001100010000010001u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.store(6, 0b11111);
	EXPECT_EQ(0b11000000000010001100010000010001u, pack.arr[0]);
	EXPECT_EQ(0b111u, pack.arr[1]);

	pack.store(7, 0b10001);
	EXPECT_EQ(0b11000000000010001100010000010001u, pack.arr[0]);
	EXPECT_EQ(0b10001111u, pack.arr[1]);

	pack.reset(6);
	EXPECT_EQ(0b00000000000010001100010000010001u, pack.arr[0]);
	EXPECT_EQ(0b10001000u, pack.arr[1]);
}

TEST(Wordpacking, resetAndStore) {
	WordPacking<unsigned int, 2, BitVal> pack;
	pack.resetAndStore(3, 17);
	EXPECT_EQ(0b10001000000000000000u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.resetAndStore(2, 17);
	EXPECT_EQ(0b10001100010000000000u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.resetAndStore(1, 17);
	EXPECT_EQ(0b10001100011000100000u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.resetAndStore(0, 17);
	EXPECT_EQ(0b10001100011000110001u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.resetAndStore(1, 0b01100);
	EXPECT_EQ(0b10001100010110010001u, pack.arr[0]);
	EXPECT_EQ(0u, pack.arr[1]);

	pack.resetAndStore(6, 0b11111);
	EXPECT_EQ(0b11000000000010001100010110010001u, pack.arr[0]);
	EXPECT_EQ(0b111u, pack.arr[1]);
}

TEST(Wordpacking, getValue) {
	WordPacking<unsigned int, 2, BitVal> pack;

	const unsigned int maxElemCount = 12;

	for (unsigned int i=0; i<maxElemCount; ++i) {
		pack.store(i, i);
	}

	for (unsigned int i=0; i<maxElemCount; ++i) {
		EXPECT_EQ(i, pack[i]);
	}

	WordPacking<unsigned int, 2, BitVal> pack2;

	for (unsigned int i=0; i<maxElemCount; ++i) {
		pack2.store(i, 2*i +1);
	}

	for (unsigned int i=0; i<maxElemCount; ++i) {
		EXPECT_EQ(2*i +1, pack2[i]);
	}
}
