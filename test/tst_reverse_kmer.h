#pragma once

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>

#include "dna_seq/canonized_kmer.h"
#include "dna_seq/dna_alph.h"

using namespace testing;

std::string complement(const std::string &str) {
	std::string comp;
	for (char c : str) {
		char compChar;
		switch (c) {
		case 'A':
			compChar = 'T';
			break;
		case 'C':
			compChar = 'G';
			break;
		case 'G':
			compChar = 'C';
			break;
		case 'T':
			compChar = 'A';
			break;
        default:
            throw std::invalid_argument("character is unknown in DNA alphabet");
		}
		comp.push_back(compChar);
	}
	return comp;
}

TEST(Reverse_Kmers, equalToOriginal) {
	std::string sequence = "ACATGACTACTTATAAAAAAAAAACCAAAAAAAAAATTCGTGA";

	Runtime::CanonizedKmer<DNA_Alph> kmer(10);

	for (int i=0; i<9; ++i) {
		kmer << sequence[i];
		EXPECT_FALSE(kmer.isComplete());
	}

	kmer << sequence[9];
	EXPECT_TRUE(kmer.isComplete());

	EXPECT_EQ(sequence.substr(0, 10), kmer.toString());
	std::string reverse = kmer.reverseToString();
	std::reverse(reverse.begin(), reverse.end());
	EXPECT_EQ(kmer.toString(), complement(reverse));

	for (size_t i=10; i<sequence.size(); ++i) {
		kmer << sequence[i];
		EXPECT_EQ(sequence.substr(i-9, 10), kmer.toString());
		std::string reverse = kmer.reverseToString();
		std::reverse(reverse.begin(), reverse.end());
		EXPECT_EQ(kmer.toString(), complement(reverse));

		if (kmer.toString() > kmer.reverseToString()) {
			EXPECT_EQ(kmer.Kmer<DNA_Alph>::operator unsigned long(), kmer.operator unsigned long());
		}
		else {
			EXPECT_LE(kmer.Kmer<DNA_Alph>::operator unsigned long(), kmer.operator unsigned long());
		}
	}
}

TEST(Reverse_Kmers, increment) {
    std::string sequence = "ACATGACTACTTATAAAAAAAAAACCAAAAAAAAAATTCGTGA";

    Runtime::CanonizedKmer<DNA_Alph> kmer(10);

    for (int i=0; i<10; ++i) {
        kmer << sequence[i];
    }

    std::string firstKmer ("ACATGACTAC");
    std::string reverse(complement(firstKmer));
    std::reverse(reverse.begin(), reverse.end());

    EXPECT_EQ(kmer.toString(), firstKmer);
    std::string reverseKmer = kmer.reverseToString();
    EXPECT_EQ(reverseKmer, reverse);

    firstKmer.replace(9, 1, "G");
    reverse.replace(0, 1, "C");
    kmer.inc(0);
    EXPECT_EQ(kmer.toString(), firstKmer);
    EXPECT_EQ(kmer.reverseToString(), reverse);

    firstKmer.replace(9, 1, "T");
    reverse.replace(0, 1, "A");
    kmer.inc(0);
    EXPECT_EQ(kmer.toString(), firstKmer);
    EXPECT_EQ(kmer.reverseToString(), reverse);

    firstKmer.replace(9, 1, "A");
    reverse.replace(0, 1, "T");
    kmer.inc(0);
    EXPECT_EQ(kmer.toString(), firstKmer);
    EXPECT_EQ(kmer.reverseToString(), reverse);

    firstKmer.replace(9, 1, "C");
    reverse.replace(0, 1, "G");
    kmer.inc(0);
    EXPECT_EQ(kmer.toString(), firstKmer);
    EXPECT_EQ(kmer.reverseToString(), reverse);

    firstKmer.replace(6, 1, "G");
    reverse.replace(3, 1, "C");
    kmer.inc(3);
    EXPECT_EQ(kmer.toString(), firstKmer);
    EXPECT_EQ(kmer.reverseToString(), reverse);

    firstKmer.replace(6, 1, "T");
    reverse.replace(3, 1, "A");
    kmer.inc(3);
    EXPECT_EQ(kmer.toString(), firstKmer);
    EXPECT_EQ(kmer.reverseToString(), reverse);

    firstKmer.replace(6, 1, "A");
    reverse.replace(3, 1, "T");
    kmer.inc(3);
    EXPECT_EQ(kmer.toString(), firstKmer);
    EXPECT_EQ(kmer.reverseToString(), reverse);

    firstKmer.replace(6, 1, "C");
    reverse.replace(3, 1, "G");
    kmer.inc(3);
    EXPECT_EQ(kmer.toString(), firstKmer);
    EXPECT_EQ(kmer.reverseToString(), reverse);
}
