#pragma once

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>
#include <bitset>

using namespace testing;

TEST(Reinterpretcast, charTest) {
	{
		unsigned char arr[10] = {0b0, 0b1, 0b11, 0b111, 0b1111, 0b11111, 0b111111, 0b1111111, 0b11111111, 0b1011};

		for(int i=0; i<10; ++i) {
			std::cout << std::bitset<32>(*reinterpret_cast<unsigned int*>(&arr[i])) << std::endl;
		}
	}

	std::cout << std::endl;

	{
		struct Test {
			unsigned char c;
		};

		Test arr[10] = {{0b0}, {0b1}, {0b11}, {0b111}, {0b1111}, {0b11111}, {0b111111}, {0b1111111}, {0b11111111}, {0b1011}};

		for(int i=0; i<10; ++i) {
			std::cout << std::bitset<32>(*reinterpret_cast<unsigned int*>(&arr[i])) << std::endl;
		}
	}
}
