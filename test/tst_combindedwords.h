#pragma once

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>

#define protected public
#include "template/bitshifting/combinedwords.h"
#include "template/bitshifting/wordcontainer.h"
#include "bitval.h"
#include "template/counters/emptydummy.h"
//#undef protected

using namespace testing;


TEST(CombindedWords, bitCount)
{
	{
		typedef CombinedWords<unsigned long long, EmptyDummy> CW;
		EXPECT_EQ(0u, CW::bitCount());
	}

	{
		typedef CombinedWords<unsigned long long, EmptyDummy, BitVal7> CW;
		EXPECT_EQ(7u, CW::bitCount());
	}

	{
		typedef CombinedWords<unsigned long long, BitVal7, EmptyDummy> CW;
		EXPECT_EQ(7u, CW::bitCount());
	}

	{
		typedef CombinedWords<unsigned long long, EmptyDummy, BitVal7, BitVal13> CW;
		EXPECT_EQ(20u, CW::bitCount());
	}

	{
		typedef CombinedWords<unsigned long long, BitVal7, BitVal13> CW;
		EXPECT_EQ(20u, CW::bitCount());
	}

	{
		typedef CombinedWords<unsigned short, BitVal7, BitVal13> CW;
		EXPECT_EQ(20u, CW::bitCount());
	}
}

TEST(CombindedWords, wordCount)
{
	{
		typedef CombinedWords<unsigned long long, EmptyDummy> CW;
		EXPECT_EQ(0u, CW::wordCount());
	}

	{
		typedef CombinedWords<unsigned long long, EmptyDummy, BitVal7> CW;
		EXPECT_EQ(1u, CW::wordCount());
	}

	{
		typedef CombinedWords<unsigned long long, BitVal7, EmptyDummy> CW;
		EXPECT_EQ(1u, CW::wordCount());
	}

	{
		typedef CombinedWords<unsigned long long, EmptyDummy, BitVal7, BitVal13> CW;
		EXPECT_EQ(1u, CW::wordCount());
	}

	{
		typedef CombinedWords<unsigned long long, BitVal7, BitVal13> CW;
		EXPECT_EQ(1u, CW::wordCount());
	}

	{
		typedef CombinedWords<unsigned short, BitVal7, BitVal13> CW;
		EXPECT_EQ(2u, CW::wordCount());
	}
}

TEST(CombindedWords, setValue)
{
	{
		typedef CombinedWords<unsigned long long, EmptyDummy, BitVal7> CW;
		CW cw;
		cw.template set<1>(5);
		EXPECT_EQ(5u, cw.store[0]);

		cw.template set<1>(127);
		EXPECT_EQ(127u, cw.store[0]);

		cw.template set<1>(15);
		EXPECT_EQ(15u, cw.store[0]);
	}

	{
		typedef CombinedWords<unsigned char, BitVal7, EmptyDummy> CW;
		CW cw;
		cw.template set<0>(5);
		EXPECT_EQ(5u, cw.store[0]);

		cw.template set<0>(127);
		EXPECT_EQ(127u, cw.store[0]);

		cw.template set<0>(15);
		EXPECT_EQ(15u, cw.store[0]);
	}

	{
		typedef CombinedWords<unsigned char, WordContainer<unsigned char>, EmptyDummy> CW;
		CW cw;
		cw.template set<0>(5);
		EXPECT_EQ(5u, cw.store[0]);

		cw.template set<0>(127);
		EXPECT_EQ(127u, cw.store[0]);

		cw.template set<0>(15);
		EXPECT_EQ(15u, cw.store[0]);

		cw.template get<1>();
	}

	{
		typedef CombinedWords<unsigned long long, BitVal7, BitVal13> CW;
		CW cw;
		cw.template set<0>(5);
		EXPECT_EQ(5u, cw.store[0]);

		cw.template set<0>(127);
		EXPECT_EQ(127u, cw.store[0]);

		cw.template set<0>(15);
		EXPECT_EQ(15u, cw.store[0]);

		cw.template set<1>(5);
		EXPECT_EQ(0b1010001111u, cw.store[0]);

		cw.template set<1>(127);
		EXPECT_EQ(0b11111110001111u, cw.store[0]);

		cw.template set<1>(15);
		EXPECT_EQ(0b00011110001111u, cw.store[0]);

		cw.template set<1>(8000);
		EXPECT_EQ(0b11111010000000001111u, cw.store[0]);
	}
}

TEST(CombinedWords, multWords) {
	{
		typedef CombinedWords<unsigned char, BitVal7, BitVal7> CW;
		CW cw;

		cw.set<0>(15);
		cw.set<1>(27);

		EXPECT_EQ(15, cw.get<0>());
		EXPECT_EQ(27, cw.get<1>());
	}

	{
		typedef CombinedWords<unsigned char, BitVal7, BitVal7, WordContainer<unsigned char>> CW;
		CW cw;

		cw.set<0>(17);
		cw.set<1>(25);
		cw.set<2>(33);

		EXPECT_EQ(17, cw.get<0>());
		EXPECT_EQ(25, cw.get<1>());
		EXPECT_EQ(33, cw.get<2>());
	}
}
