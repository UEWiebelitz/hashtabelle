#include "tst_counter.h"
//#include "tst_simple_page.h"
//#include "tst_wordpacking.h"
//#include "tst_wordpacking_counting.h"
#include "tst_combindedwords.h"
#include "tst_reinterpretcast.h"
//#include "tst_simple_page.h"
//#include "tst_hashtable_simple.h"
//#include "tst_hashtable_replacing.h"
//#include "tst_hashtable_multilevel.h"
//#include "tst_hf_fingerprint.h"
//#include "tst_kmerreader.h"
//#include "tst_ecoli.h"
#include "tst_compare_wordpacking_impl.h"
#include "tst_reverse_kmer.h"
#include "tst_count_kmers.h"
#include "kmer.h"

#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
