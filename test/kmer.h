#pragma once

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>

#define protected public

#include "dna_seq/dna_alph.h"
#include "dna_seq/canonized_kmer.h"

TEST(kmer_runtime, reverse) {
	Runtime::CanonizedKmer<DNA_Alph> kmer(4);

	kmer << 'A';
	kmer << 'C';
	kmer << 'A';
	kmer << 'G';

	EXPECT_EQ(0b00010010u, kmer.word);
	EXPECT_EQ(0b01111011u, kmer.reverse);
}
