#pragma once

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>

#include "bitval.h"

#define protected public
#include "page_layouts/page_packed_counting.h"

TEST(Page_Packed_Counting, elemCapacity) {
	Page_Packed_Counting<unsigned int, 2, BitVal> pack;
	EXPECT_EQ(12u, pack.elemCapacity());

	Page_Packed_Counting<unsigned int, 3, BitVal> pack2;
	EXPECT_EQ(18u, pack2.elemCapacity());
}

TEST(Page_Packed_Counting, elemCount) {
	{
		Page_Packed_Counting<unsigned int, 2, BitVal> page;
		for (unsigned int i=0; i<page.elemCapacity(); ++i) {
                        page.insert(i);
			EXPECT_EQ(i +1, page.elemCount());
		}

                AppendResult res = page.insert(20);
		EXPECT_EQ(APPEND_FULL, res);
		EXPECT_EQ(page.elemCapacity(), page.elemCount());
	}

	{
		Page_Packed_Counting<unsigned long long, 5, BitVal7> page;
		for (unsigned int i=0; i<page.elemCapacity(); ++i) {
                        page.insert(page.elemCapacity() - i);
			EXPECT_EQ(i +1, page.elemCount());
		}

                AppendResult res = page.insert(100);
		EXPECT_EQ(APPEND_FULL, res);
		EXPECT_EQ(page.elemCapacity(), page.elemCount());
	}
}

TEST(Page_Packed_Counting, store) {
	Page_Packed_Counting<unsigned int, 2, BitVal> page;
	for (unsigned int i=0; i<page.elemCapacity(); ++i) {
                page.insert(i);
	}

	for (unsigned int i=0; i<page.elemCapacity(); ++i) {
		EXPECT_EQ(i, page[i]);
	}

	Page_Packed_Counting<unsigned long long, 5, BitVal7> page2;
	for (unsigned int i=0; i<page2.elemCapacity(); ++i) {
                page2.insert(page2.elemCapacity() - i);
	}

	for (unsigned int i=0; i<page2.elemCapacity(); ++i) {
		EXPECT_EQ(page2.elemCapacity() - i, page2[i]);
	}
}

TEST(Page_Packed_Counting, replace) {
	Page_Packed_Counting<unsigned int, 2, BitVal> page;
	for (unsigned int i=0; i<page.elemCapacity(); ++i) {
                page.insert(i);
		EXPECT_EQ(i +1, page.elemCount());
	}

	for (unsigned int i=0; i<page.elemCapacity(); ++i) {
		EXPECT_EQ(i, page[i]);
	}

	for (unsigned int i=0; i<page.elemCapacity(); ++i) {
		unsigned int res = page.replace(i, page.elemCapacity() - i);
		EXPECT_EQ(i, res);
		EXPECT_EQ(page.elemCapacity(), page.elemCount());
	}

	for (unsigned int i=0; i<page.elemCapacity(); ++i) {
		EXPECT_EQ(page.elemCapacity() - i, page[i]);
	}
}
